import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  Output,
  QueryList,
  ViewChildren
} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil, throttleTime } from 'rxjs/operators';
import { UI } from '../../core/enums/ui';
import { CAROUSEL_ANIMATION } from './carousel.animation';
import { CarouselOrientation, CarouselVisibility } from './carousel.enums';
import { CarouselItemComponent } from './item/item.component';

const DEFAULT_HEIGHT = 300;
const DEFAULT_SPEED = 500;
const DEFAULT_AUTOSPEED = 1500;
const OBSERVER_DELAY = 1000;
const OBSERVER_OPTIONS = {
  attributes: true,
  childList: true,
  characterData: true,
  subtree: true
};

@Component({
  selector: 'sn-carousel',
  templateUrl: './carousel.encapsulated.html',
  animations: [CAROUSEL_ANIMATION],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CarouselComponent implements AfterViewInit {

  ui = UI;
  carouselOrientation = CarouselOrientation;
  carouselVisibility = CarouselVisibility;

  private destroyed$ = new Subject<any>();
  private interval: any;
  private _speed = DEFAULT_SPEED;
  private _autoplay = false;
  private _autoplaySpeed = DEFAULT_AUTOSPEED;
  private _orientation = CarouselOrientation.right;
  private paused = false;
  private slide$ = new Subject<any>();
  current = 0;
  currentOrientation: CarouselOrientation = CarouselOrientation.right;

  @HostBinding('attr.ca')
  readonly host = '';

  @Input()
  @HostBinding('style.height.px')
  height = 0;

  @Input()
  set speed(speed: number) {
    this._speed = speed || DEFAULT_SPEED;
  }

  get speed() {
    return this._speed;
  }

  @Input()
  set orientation(orientation: CarouselOrientation) {
    this._orientation = orientation || CarouselOrientation.right;
  }

  get orientation() {
    return this._orientation;
  }

  @Input()
  set autoplay(autoplay) {
    this._autoplay = autoplay;
    this.play();
  }

  get autoplay() {
    return this._autoplay;
  }

  @Input()
  set autoplaySpeed(autoplaySpeed: number) {
    this._autoplaySpeed = autoplaySpeed || DEFAULT_AUTOSPEED;
    this.play();
  }

  get autoplaySpeed() {
    return this._autoplaySpeed;
  }

  @Input()
  infinite = true;

  @Input()
  dots = false;

  @Input()
  arrows = true;

  @Output()
  onChange = new EventEmitter<any>();

  @ContentChildren(CarouselItemComponent)
  items: QueryList<CarouselItemComponent>;

  @ViewChildren('carouselItem')
  carouselItems: QueryList<any>;

  set slide(index: number) {
    this.slide$.next(index);
  }

  @HostListener('mouseenter')
  onEnter() {
    this.paused = true;
    this.pause();
  }

  @HostListener('mouseleave')
  onLeave() {
    this.paused = false;
    this.play();
  }

  constructor(private cd: ChangeDetectorRef) {
  }

  ngAfterViewInit() {
    if (this.carouselItems && this.carouselItems.length > 0 && !this.height) {
      this.carouselItems.forEach(item => this.observeItem(item));
    }

    this.slide$.pipe(
      throttleTime(this.speed),
      takeUntil(this.destroyed$)
    ).subscribe(index => {
      this.pause();
      this.go(index);
      if (this.autoplay && !this.paused) {
        this.play();
      }
    });

    this.play();
  }

  private observeItem(item: ElementRef) {
    let timer: ReturnType<typeof setTimeout>;
    setTimeout(() => {
      this.cd.detectChanges();
      this.setHeight(item);
    }, OBSERVER_DELAY);
    new MutationObserver((_, observer: MutationObserver) => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        this.cd.detectChanges();
        if (this.height === this.setHeight(item)) {
          observer.disconnect();
        }
      }, this.autoplaySpeed);

      this.setHeight(item);
    }).observe(item.nativeElement, OBSERVER_OPTIONS);
  }

  private setHeight(item: ElementRef): number {
    const height = item.nativeElement.clientHeight || DEFAULT_HEIGHT;
    if (this.height !== height) {
      this.height = Math.max(height, this.height);
      this.cd.detectChanges();
    }
    return height;
  }

  go(index: number, orientation: CarouselOrientation = null) {
    this.currentOrientation = orientation || (index < this.current
      ? CarouselOrientation.left : CarouselOrientation.right);

    if (((this.current <= 0 && this.currentOrientation === CarouselOrientation.left)
      || (this.current >= this.items.length - 1) && this.currentOrientation === CarouselOrientation.right)
      && !this.infinite) {
      return;
    }

    this.current = index < 0 ? this.items.length - 1
      : (index === this.items.length ? 0 : index);
    this.cd.detectChanges();
  }

  private play() {
    this.pause();
    if (this.autoplay && !!this.items && this.items.length > 1) {
      this.interval = setInterval(() => {
        if (!document.hidden) {
          this.go(this.current + 1, this.orientation);
        }
      }, this.autoplaySpeed);
    } else {
      this.pause();
    }
  }

  private pause() {
    clearInterval(this.interval);
  }
}
