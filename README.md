# Esanum UI test application

The project contains sources for `Esanum UI` & test app.

See demo here 👉 https://ui.esanum.de/

For using `Esanum UI` in your apps please check https://www.npmjs.com/package/@esanum/ui

## For development

```
npm i
cd ./projects/esanum-ui
npm i
cd -
npm start
```

Open in browser http://localhost:4200/

### Gitlab

#### Publish:
```
cd projects/esanum-ui
npm config set @esanum:registry https://gitlab.com/api/v4/projects/31260197/packages/npm/
npm config set '//gitlab.com/api/v4/projects/31260197/packages/npm/:_authToken' "xxx"
npm run publish:version
```
Where `xxx`: 
- personal access token 
  - https://gitlab.com/-/profile/personal_access_tokens
  - Create new token with `read_registry` and `write_registry` scopes
  - Save this token to own computer and use this token
- deploy token
 
#### Install:

##### From Gitlab:
- Add this commands to project package.json:
```
"esanum:set": "npm config set @esanum:registry https://gitlab.com/api/v4/projects/31260197/packages/npm",
"esanum:install": "npm install https://gitlab.com/api/v4/projects/31260197/packages/npm/@esanum/ui/-/@esanum/ui-xxx.tgz --save",
"esanum": "npm run esanum:set && npm run esanum:install"
```
Where `xxx` - current library version (https://gitlab.com/esanum/ui/-/packages)

- Run `npm run esanum` in the terminal

##### From local package:
In library:
```
cd projects/esanum-ui
npm run pack:version
```

In project:
- Use `"@esanum/ui": "file:../../esanum-ui/esanum-ui/dist/esanum-ui/esanum-ui-xxx.tgz",`
    - Where `xxx` - current library version (file `VERSION` in `esanum-ui/projects/esanum-ui`)
- Don't forget replace `"@esanum/ui": "file:../../esanum-ui/esanum-ui/dist/esanum-ui/esanum-ui-xxx.tgz",` to real package

#### Remove Gitlab registry for publish to npm:
```
cd projects/esanum-ui
npm config rm @esanum:registry
npm run publish:version
```

#### Info about current publish registry:
```
cd projects/esanum-ui
npm config list
```
