import { Component, OnInit } from '@angular/core';
import { UI } from 'esanum-ui';

@Component({
  selector: 'app-typography-test',
  templateUrl: './typography.component.html',
  styleUrls: ['./typography.component.scss']
})
export class TypographyComponent implements OnInit {
  ui = UI;
  constructor() { }

  ngOnInit() {
  }

}
