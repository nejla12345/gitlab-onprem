export const BUTTON_API = {
  'properties': {
    'shape': {
      'description': 'Button shape',
      'path': 'ui.shape',
      'default': 'square',
      'options': [
        'circle',
        'square'
      ]
    },
    'loading': {
      'description': 'Set the loading status of button',
      'type': 'boolean',
      'default': 'false'
    },
    '_icon': {
      'name': 'icon',
      'description': 'Icon for button',
      'type': 'string | {icon: string, position: Position}'
    },
    'spacing': {
      'description': 'Space between elements',
      'path': 'ui.gutter',
      'options': ['none', 'tiny', 'small', 'normal', 'big', 'large', 'huge'],
      'default': 'small'
    },
    'scheme': {
      'description': 'Button color scheme',
      'path': 'ui.scheme',
      'options': [
        'primary',
        'secondary',
        'success',
        'fail',
        'accent'
      ],
      'default': 'primary'
    },
    'size': {
      'description': 'Button size',
      'path': 'ui.size',
      'options': [
        'tiny',
        'small',
        'normal',
        'large'
      ],
      'default': 'normal'
    },
    'outline': {
      'description': 'Button outline',
      'path': 'ui.outline',
      'default': 'fill',
      'options': [
        'transparent',
        'ghost',
        'fill'
      ]
    },
    'width': {
      'description': 'Button width',
      'path': 'ui.width',
      'default': 'default',
      'options': [
        'default',
        'fluid'
      ]
    },
    'disabled': {
      'description': 'Set disabled state',
      'type': 'boolean',
      'default': 'false'
    },
    'type': {
      'description': 'Button typeControl',
      'path': 'ui.button.type',
      'default': 'button',
      'options': [
        'button',
        'submit'
      ]
    },
    'text': {
      'description': 'Text on button',
      'type': 'string'
    },
    'ariaLabel': {
      'description': 'Aria-label for button',
      'type': 'string'
    },
    'click': {
      'description': 'Click event',
      'path': 'EventEmitter'
    }
  }
};

export const BUTTON_GROUP_API = {
  'properties': {
    'size': {
      'description': 'Button group size',
      'path': 'ui.size',
      'options': [
        'tiny',
        'small',
        'normal',
        'large'
      ],
      'default': 'normal'
    },
    'scheme': {
      'description': 'Button group color scheme',
      'path': 'ui.scheme',
      'options': [
        'primary',
        'secondary',
        'success',
        'fail'
      ],
      'default': 'primary'
    },
    'outline': {
      'description': 'Button group outline',
      'path': 'ui.outline',
      'default': 'fill',
      'options': [
        'transparent',
        'ghost',
        'fill'
      ]
    },
    'width': {
      'description': 'Button group width',
      'path': 'ui.width',
      'default': 'default',
      'options': [
        'default',
        'fluid'
      ]
    },
    'features': {
      'description': 'Adapted button group on mobile view',
      'path': '[ui.feature',
      'options': ['adapted]']
    }
  }
};
