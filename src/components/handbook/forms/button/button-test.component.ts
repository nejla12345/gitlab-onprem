import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TabsComponent, UI } from 'esanum-ui';
import { HANDBOOK } from 'src/consts';
import { LocalUI } from 'src/enums/local-ui';
import { BUTTON_API, BUTTON_GROUP_API } from './button-test.api';

@Component({
  selector: 'app-button-test',
  templateUrl: './button-test.component.html',
  styleUrls: ['./button-test.component.scss']
})
export class ButtonTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  api = {
    button: BUTTON_API,
    buttonGroup: BUTTON_GROUP_API
  };
  handbook = HANDBOOK;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/forms/button';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI?node-id=114%3A0';

  @ViewChild('tabs', {static: true})
  tabs: TabsComponent;

  schemeControl = this.fb.control(null);
  sizeControl = this.fb.control(null);
  outlineControl = this.fb.control(null);
  widthControl = this.fb.control(null);
  typeControl = this.fb.control(null);
  loadingControl = this.fb.control(false);
  disabledControl = this.fb.control(false);
  positionControl = this.fb.control(null);
  shapeControl = this.fb.control(null);
  adaptedControl = this.fb.control(true);
  elementsControl = this.fb.control(['text', 'icon', 'badge']);
  spacingControl = this.fb.control(null);
  ariaLabelControl = this.fb.control(false);

  builder = this.fb.group({
    scheme: this.schemeControl,
    size: this.sizeControl,
    outline: this.outlineControl,
    width: this.widthControl,
    type: this.typeControl,
    loading: this.loadingControl,
    disabled: this.disabledControl,
    position: this.positionControl,
    shape: this.shapeControl,
    adapted: this.adaptedControl,
    elements: this.elementsControl,
    spacing: this.spacingControl,
    ariaLabel: this.ariaLabelControl
  });

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));

    this.shapeControl.disable();

    this.elementsControl.valueChanges
      .subscribe(value => {
        value.includes('text') || value.includes('badge')
          ? this.shapeControl.disable()
          : this.shapeControl.enable();
        value.includes('icon')
          ? this.positionControl.enable()
          : this.positionControl.disable();
      });
  }
}
