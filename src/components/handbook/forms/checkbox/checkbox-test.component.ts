import { KeyValue } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BlockComponent, BreakpointService, TabsComponent, UI } from 'esanum-ui';
import { Language } from 'src/components/handbook/shared/code-highlight/enum';
import { HANDBOOK, HEROES } from 'src/consts';
import { LocalUI } from 'src/enums/local-ui';
import { CHECKBOX_API, CHECKBOX_GROUP_API } from './checkbox-test.api';

const SUCCESS_DELAY = 1800;

@Component({
  selector: 'app-checkbox-test',
  templateUrl: './checkbox-test.component.html',
  styleUrls: ['./checkbox-test.component.scss']
})
export class CheckboxTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  language = Language;
  api = {
    checkbox: CHECKBOX_API,
    checkboxGroup: CHECKBOX_GROUP_API
  };
  handbook = HANDBOOK;
  heroes = HEROES;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/forms/checkbox';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI-v1?node-id=2570%3A2779';

  @ViewChild('tabs') tabs: TabsComponent;

  @ViewChild('block')
  block: BlockComponent;

  orientationControl = this.fb.control(null);
  spacingControl = this.fb.control(null);
  sizeControl = this.fb.control(null);
  alignControl = this.fb.control(null);
  disableControl = this.fb.control(null);
  colsControl = this.fb.control(null);
  customControl = this.fb.control(false);
  adaptedControl = this.fb.control(false);

  builder = this.fb.group({
    orientation: this.orientationControl,
    spacing: this.spacingControl,
    size: this.sizeControl,
    align: this.alignControl,
    cols: this.colsControl,
    disable: this.disableControl,
    custom: this.customControl,
    adapted: this.adaptedControl
  });

  heroControl = this.fb.control([this.heroes.captain.code], Validators.required);

  form = this.fb.group({
    hero: this.heroControl
  });

  originalOrder = (a: KeyValue<number, string>, b: KeyValue<number, string>): number => 0;

  constructor(private fb: FormBuilder,
              public breakpoint: BreakpointService) {
  }

  ngOnInit() {
    this.disableControl.valueChanges.subscribe(disabled =>
      disabled ? this.heroControl.disable({emitEvent: false})
        : this.heroControl.enable({emitEvent: false}));

    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));

    this.colsControl.valueChanges.subscribe(value => {
      if (!!value) {
        this.orientationControl.disable();
        this.spacingControl.disable();
      } else {
        this.orientationControl.enable();
        this.spacingControl.enable();
      }
    });
  }

  submit() {
    this.block.success();
    setTimeout(() => this.form.reset(), SUCCESS_DELAY);
  }

  setHero() {
    this.heroControl.setValue([this.heroes.superman.code]);
  }
}
