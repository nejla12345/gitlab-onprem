export const SWITCH_API = {
  'properties': {
    'label': {'description': 'Label for switch', 'type': 'string'},
    'icons': {'description': 'Icons for states', 'type': '{on: string, off: string}'},
    'tags': {'description': 'Tags for states', 'type': '{on: string, off: string}'},
    'size': {'description': 'Switch size', 'path': 'ui.size', 'default': 'normal', 'options': ['tiny', 'small', 'normal', 'large']},
    'align': {
      'description': 'Align by vertical for switch',
      'path': 'ui.align',
      'options': ['center', 'start', 'end'],
      'default': 'center'
    },
    'value': {'description': 'Value for checkbox', 'type': 'any'}
  },
  'content': {
    'labelTemplate': {'selector': '#switchLabelTemplate', 'description': 'Switch label template'}
  }
};

export const SWITCH_GROUP_API = {
  'properties': {
    'orientation': {
      'description': 'Defined main axis of elements align',
      'path': 'ui.orientation',
      'default': 'vertical',
      'options': ['vertical', 'horizontal']
    },
    'align': {'description': 'Align in radio group', 'path': 'ui.align'},
    'cols': {'description': 'Count of cols in checkbox group', 'type': 'number', 'default': 1},
    'size': {
      'description': 'Size for checkbox in checkbox group',
      'path': 'ui.size',
      'options': ['tiny', 'small', 'normal', 'large'],
      'default': 'normal'
    },
    'spacing': {
      'description': 'Spacing between radio item',
      'path': 'ui.gutter',
      'options': ['tiny', 'small', 'normal', 'large', 'big', 'huge'],
      'default': 'normal'
    },
    'features': {'description': 'Adapted radio group on mobile view', 'path': 'ui.feature', 'options': ['adapted']}
  }
};
