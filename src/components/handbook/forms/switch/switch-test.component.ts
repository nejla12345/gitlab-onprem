import { KeyValue } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { BreakpointService, TabsComponent, UI } from 'esanum-ui';
import { HANDBOOK, HEROES } from 'src/consts';
import { LocalUI } from 'src/enums/local-ui';
import { Language } from '../../shared/code-highlight/enum';
import { SWITCH_API, SWITCH_GROUP_API } from './switch-test.api';

enum SwitchType {
  single,
  group
}

@Component({
  selector: 'app-switch-test',
  templateUrl: './switch-test.component.html',
  styleUrls: ['./switch-test.component.scss']
})
export class SwitchTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  api = {
    switch: SWITCH_API,
    switchGroup: SWITCH_GROUP_API
  };
  handbook = HANDBOOK;
  heroes = HEROES;
  language = Language;
  switchType = SwitchType;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/forms/switch';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI-v1?node-id=2570%3A2784';

  @ViewChild('tabs') tabs: TabsComponent;

  sizeControl = this.fb.control(null);
  labelControl = this.fb.control(false);
  disabledControl = this.fb.control(false);
  colsControl = this.fb.control(null);
  typeControl = this.fb.control(SwitchType.single);
  alignControl = this.fb.control(UI.align.center);

  builder = this.fb.group({
    size: this.sizeControl,
    label: this.labelControl,
    disabled: this.disabledControl,
    cols: this.colsControl,
    type: this.typeControl,
    align: this.alignControl
  });

  switchControl = this.fb.control(false);
  heroControl = this.fb.control([this.heroes.captain.code], Validators.required);

  form = this.fb.group({
    switch: this.switchControl,
    hero: this.heroControl
  });

  originalOrder = (a: KeyValue<number, string>, b: KeyValue<number, string>): number => 0;

  constructor(private fb: FormBuilder,
              public breakpoint: BreakpointService) {
  }

  ngOnInit() {
    this.disabledControl.valueChanges.subscribe((disabled) => {
      disabled ? this.switchControl.disable({emitEvent: false})
        : this.switchControl.enable({emitEvent: false});
    });

    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));
  }
}
