export const IMAGE_CROPPER_API = {
  'properties': {
    'area': {
      'description': 'Size of crop area',
      'type': 'CropperPosition',
      'default': '{width: 200, height: 200}'
    },
    'min': {'description': 'Min of cropping', 'type': 'number', 'default': '0.01'},
    'max': {'description': 'Max of cropping', 'type': 'number', 'default': '5'},
    'step': {'description': 'Step of cropping', 'type': 'number', 'default': '0.01'},
    'url': {'description': 'Url of image', 'type': 'string', 'default': null},
    'shape': {'description': 'Avatar shape', 'path': 'ui.shape', 'default': 'circle', 'options': ['circle', 'square']}
  }
};
