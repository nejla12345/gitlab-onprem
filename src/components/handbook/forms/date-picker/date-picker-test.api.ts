export const DATE_PICKER_API = {
  'properties': {
    'placeholder': {'description': 'Placeholder for date picker', 'type': 'string'},
    'features': {'description': 'Button for reset input', 'path': 'ui.feature', 'options': ['allowEmpty']},
    'type': {'description': 'Date picker type', 'path': 'ui.type', 'options': ['date', 'time', 'dateTime']},
    'width': {'description': 'Input width', 'path': 'ui.width', 'default': 'default', 'options': ['default', 'fluid']}
  }
};
