export const SELECT_API = {
  'properties': {
    'labelField': {'description': 'Select label field', 'type': 'string', 'default': 'label'},
    'keyField': {'description': 'Select key field', 'type': 'string', 'default': 'key'},
    'groupField': {'description': 'Group field', 'type': 'string', 'default': 'null'},
    'groupFieldKey': {'description': 'Group field key', 'type': 'string', 'default': 'null'},
    'placeholder': {'description': 'Select placeholder', 'type': 'string'},
    'required': {'description': 'Select required', 'type': 'boolean', 'default': 'false'},
    'label': {'description': 'Select label', 'type': 'string'},
    'icon': {'description': 'Icon for select', 'type': 'string'},
    'state': {'description': 'Select state', 'path': 'ui.state', 'options': ['loading']},
    'optionTemplate': {'description': 'Template for option', 'type': 'TemplateRef<any>'},
    'emptyOptionsTemplate': {'description': 'Template for empty options', 'type': 'TemplateRef<any>'},
    'optionsHeaderTemplate': {'description': 'Template for options header', 'type': 'TemplateRef<any>'},
    'updated': {'description': 'Selected value', 'type': '(selected)='},
    'placement': {'description': 'Menu popover placement', 'path': 'ui.placement', 'default': 'absolute', 'options': ['absolute', 'fixed']},
    'mode': {'description': 'Select mode', 'path': 'ui.select.mode', 'default': 'single', 'options': ['single', 'multiple']},
    'features': {'description': 'Select features', 'path': 'ui.feature', 'options': ['search', 'multiplex', 'allowEmpty']},
    'autocomplete': {'description': 'Auto complete for select', 'path': 'ui.select.autocomplete', 'options': ['on', 'off']},
    'size': {'description': 'Select size', 'path': 'ui.size', 'default': 'normal', 'options': ['tiny', 'small', 'normal', 'large']},
    'width': {'description': 'Select width', 'path': 'ui.width', 'default': 'default', 'options': ['default', 'fluid']},
    'loader': {'description': 'Select loader', 'type': 'function'},
    'creator': {'description': 'Select creator', 'type': 'function'}
  }
};

export const SELECT_OPTION_API = {
  'properties': {
    'icon': {'description': 'Icon for select option', 'type': 'string'},
    'key': {'description': 'Key for select option', 'type': 'number | string'},
    'label': {'description': 'Label name for select option', 'type': 'string'},
    'value': {'description': 'Value for select option', 'type': 'any'}
  }
};
