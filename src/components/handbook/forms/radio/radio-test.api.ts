export const RADIO_API = {
  'properties': {
    'size': {
      'description': 'Size for radio button',
      'type': 'string',
      'path': 'ui.size',
      'options': ['tiny', 'small', 'normal', 'large'],
      'default': 'normal'
    },
    'label': {'description': 'Label name for radio button', 'type': 'string'},
    'value': {'description': 'Value for radio button', 'type': 'any'},
    'features': {'description': 'Allow empty for radio', 'path': 'ui.feature', 'options': ['allowEmpty']}
  }
};

export const RADIO_GROUP_API = {
  'properties': {
    'orientation': {
      'description': 'Defined main axis of elements align',
      'path': 'ui.orientation',
      'default': 'vertical',
      'options': ['vertical', 'horizontal']
    },
    'align': {'description': 'Align in radio group', 'path': 'ui.align'},
    'cols': {'description': 'Count of cols in radio group', 'type': 'number', 'default': 1},
    'size': {
      'description': 'Size for radio in radio group',
      'path': 'ui.size',
      'options': ['tiny', 'small', 'normal', 'large'],
      'default': 'normal'
    },
    'spacing': {
      'description': 'Spacing between radio item',
      'path': 'ui.gutter',
      'options': ['tiny', 'small', 'normal', 'large', 'big', 'huge'],
      'default': 'normal'
    },
    'features': {
      'description': 'Adapted radio group on mobile view; Allow empty for radio',
      'path': 'ui.feature',
      'options': ['adapted', 'allowEmpty']
    }
  }
};
