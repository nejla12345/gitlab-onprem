export const FILTER_API = {
  'properties': {
    'selected': {'description': 'Condition for selected filter', 'type': 'boolean'},
    'control': {'description': 'Associated form control for filter', 'type': 'AbstractControl'},
    'placeholder': {'description': 'Placeholder for filter', 'type': 'string'},
    'icon': {'description': 'Icon for filter', 'type': 'string'},
    'label': {'description': 'Label for filter', 'type': 'string'}
  },
  'content': {
    'filterContentTemplate': {'selector': '#filterContentTemplate', 'description': 'Filter content template'}
  }
};
