export const SWITCHER_API = {
  'properties': {
    'orientation': {
      'description': 'Switcher orientation ',
      'path': 'ui.orientation',
      'default': 'horizontal',
      'options': ['horizontal', 'vertical']
    },
    'disabled': {'description': 'Set disabled state', 'type': 'boolean', 'default': 'false'},
    'keyField': {'description': 'Select key field', 'type': 'string', 'default': 'key'},
    'mode': {'description': 'Switcher mode', 'path': 'ui.select.mode', 'default': 'single', 'options': ['single', 'multiple']},
    'features': {
      'description': 'Add badge with the number of selected items; Select all item in switcher; Allow empty value in switcher; Adapted on mobile; Display marks',
      'path': 'ui.feature',
      'default': '[ui.feature.adapted]',
      'options': ['badge', 'selectAll', 'allowEmpty', 'adapted', 'marks']
    },
    'capacity': {'description': 'Display skeleton', 'type': 'count: number'},
    'loading': {'description': 'Loading', 'type': 'boolean', 'default': 'false'},
    'width': {'description': 'Input width', 'path': 'ui.width', 'default': 'default', 'options': ['default', 'fluid']},
    'updated': {'description': 'Selected value', 'type': '(selected)='}
  }
};

export const SWITCHER_OPTION_API = {
  'properties': {
    'label': {'description': 'Text on switcher option', 'type': 'string'},
    'value': {'description': 'Switcher option value', 'type': 'any'},
    'icon': {'description': 'Icon for switcher option', 'type': 'string'},
    'disabled': {'description': 'Disable switcher option', 'type': 'boolean'}
  }
};
