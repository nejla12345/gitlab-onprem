import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TabsComponent, UI } from 'esanum-ui';
import { HANDBOOK } from 'src/consts';
import { LocalUI } from 'src/enums/local-ui';
import { SLIDER_API } from './slider-test.api';

@Component({
  selector: 'app-slider-test',
  templateUrl: './slider-test.component.html',
  styleUrls: ['./slider-test.component.scss']
})
export class SliderTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  api = {
    slider: SLIDER_API
  };
  handbook = HANDBOOK;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/forms/slider';

  @ViewChild('tabs') tabs: TabsComponent;

  minControl = this.fb.control(0);
  maxControl = this.fb.control(100);
  stepControl = this.fb.control(1);
  builder = this.fb.group({
    min: this.minControl,
    max: this.maxControl,
    step: this.stepControl
  });

  sliderControl = this.fb.control(50);

  form = this.fb.group({
    slider: this.sliderControl
  });

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));
  }

}
