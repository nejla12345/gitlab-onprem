export const MENU_API = {
  'properties': {
    'style': {
      'description': 'Menu style',
      'path': 'ui.menu.style',
      'default': 'default',
      'options': ['tabs', 'tags', 'default']
    },
    'orientation': {
      'description': 'Menu orientation',
      'path': 'ui.orientation',
      'default': 'horizontal',
      'options': ['horizontal', 'vertical']
    },
    'wrap': {'description': 'Menu items wrap', 'path': 'ui.wrap', 'default': 'wrap', 'options': ['wrap', 'nowrap']},
    'placement': {'description': 'Menu popover placement', 'path': 'ui.placement', 'default': 'absolute', 'options': ['absolute', 'fixed']},
    'gutter': {
      'description': 'Size of gutter between menu items',
      'path': 'ui.gutter',
      'default': 'none',
      'options': ['none', 'tiny', 'small', 'normal', 'large', 'big', 'huge']
    },
    'spacing': {
      'description': 'Size of spacing between menu items then wrapping',
      'path': 'ui.gutter',
      'default': 'none',
      'options': ['none', 'tiny', 'small', 'normal', 'large', 'big', 'huge']
    },
    'trigger': {'description': 'Trigger for open dropdown menu', 'path': 'ui.trigger', 'default': 'click', 'options': ['click', 'hover']}
  }
};

export const MENU_ITEM_API = {
  'properties': {
    'loading': {'description': 'Loading for menu item', 'default': 'false', 'type': 'boolean'},
    'icon': {'description': 'Icon for menu item', 'type': 'string'},
    'disabled': {'description': 'Disable menu item', 'type': 'boolean', 'default': 'false'},
    'title': {'description': 'Menu item title', 'type': 'string'},
    'link': {'name': 'link', 'description': 'Menu item source', 'type': 'string | any[]'},
    'target': {'description': 'Menu item target', 'type': 'string', 'default': 'self', 'options': ['blank', 'parent', 'self', 'top']},
    'matching': {'description': 'Methods of matching', 'path': 'ui.matching', 'default': 'fullMatch', 'options': ['fullMatch', 'wildcard']},
    'active': {'description': 'Set active menu item', 'type': 'boolean'},
    'queryParams': {'description': 'Menu item query params', 'type': '{[k: string]: any}'},
    'fragment': {'description': 'Fragment for link #anchor', 'default': 'null'},
    'click': {'description': 'Output event for click on menu item'}
  }
};
