export const BREADCRUMBS_API = {
  'properties': {
    'wrap': {
      'description': 'Set wrap of breadcrumbs',
      'path': 'ui.wrap',
      'options': ['wrap', 'nowrap', 'reverse']
    },
    'aside': {'description': 'Support burger button for mobile devices', 'type': 'AppAsideComponent'},
    'features': {'description': 'Set page title based on breadcrumb title', 'path': 'ui.feature', 'options': ['page-title']}
  }
};
