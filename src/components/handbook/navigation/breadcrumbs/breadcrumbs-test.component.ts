import { Component } from '@angular/core';
import { UI } from 'esanum-ui';
import { Language } from 'src/components/handbook/shared/code-highlight/enum';
import { HANDBOOK } from 'src/consts';
import { LocalUI } from 'src/enums/local-ui';
import { BREADCRUMBS_API } from './breadcrumbs-test.api';

@Component({
  selector: 'app-breadcrumbs-test',
  templateUrl: './breadcrumbs-test.component.html',
  styleUrls: ['./breadcrumbs-test.component.scss']
})
export class BreadcrumbsTestComponent {
  ui = UI;
  localUi = LocalUI;
  language = Language;
  handbook = HANDBOOK;

  api = {
    breadcrumbs: BREADCRUMBS_API
  };

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/navigation/breadcrumbs';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI?node-id=780%3A31';

}
