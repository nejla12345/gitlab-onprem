import { Component } from '@angular/core';
import { UI } from 'esanum-ui';

@Component({
  selector: 'app-breadcrumbs-test3',
  template: `
    <sn-stack [orientation]="ui.orientation.horizontal" [align]="ui.align.center">
      <sn-icon [icon]="ui.icons.chevronRight"></sn-icon>
      <div block>3</div>
    </sn-stack>
  `,
  styleUrls: ['./test.component.scss']
})

export class BreadCrumbTest3Component {
  ui = UI;
}
