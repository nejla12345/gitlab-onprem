export const PAGER_API = {
  'properties': {
    'size': {'description': 'Pager size', 'type': 'number'},
    'count': {'description': 'Items count for pager', 'type': 'number'},
    'pageSize': {'description': 'Page size for pager', 'type': 'number', 'default': '10'},
    'mode': {'description': 'Mode for pager', 'path': 'ui.pager.mode', 'options': ['offset', 'page'], 'default': 'offset'}
  }
};
