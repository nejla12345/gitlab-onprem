export const LINK_API = {
  'properties': {
    'disabled': {'description': 'Disable link', 'type': 'boolean', 'default': 'false'},
    'outline': {'description': 'Link outline', 'path': 'ui.outline', 'default': 'transparent', 'options': ['transparent', 'ghost', 'fill']},
    '__icon__': {'description': 'Icon for link', 'type': 'string', 'name': 'icon'},
    'title': {'description': 'Link title', 'type': 'string'},
    'queryParams': {'description': 'Link query params', 'type': '{[k: string]: any}'},
    'source': {'description': 'Link source', 'type': 'string | (string | { [key: string]: string | number })[]'},
    'target': {'description': 'Link target', 'path': 'ui.target', 'default': 'self', 'options': ['blank', 'self', 'parent', 'top']},
    'fragment': {'description': 'Fragment for link #anchor', 'default': 'null'},
    'matching': {
      'description': 'Matching to activate link',
      'path': 'ui.matching',
      'default': 'fullMatch',
      'options': ['fullMatch', 'wildcard']
    },
    'features': {'description': 'Show chevron near link', 'path': 'ui.feature', 'options': ['dropdown']},
    'context': {'description': 'Link context', 'path': 'ui.context', 'default': 'text', 'options': ['text', 'box']}
  }
};
