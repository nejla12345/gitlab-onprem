import { Component, Inject, LOCALE_ID, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TabsComponent, UI } from 'esanum-ui';
import { HANDBOOK } from 'src/consts';
import { Language } from 'src/enums/language';
import { LocalUI } from 'src/enums/local-ui';
import { ACCORDION_API, ACCORDION_SECTION_API } from './accordion-test.api';

@Component({
  selector: 'app-accordion-test',
  templateUrl: './accordion-test.component.html',
  styleUrls: ['./accordion-test.component.scss']
})
export class AccordionTestComponent implements OnInit {


  ui = UI;
  localUi = LocalUI;
  language = Language;
  api = {
    accordion: ACCORDION_API,
    accordionSection: ACCORDION_SECTION_API
  };
  handbook = HANDBOOK;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/collections/accordion';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI-v1?node-id=6587%3A0';

  heroes = [
    {id: 1, name: 'Spiderman', avatar: 'assets/images/heroes/spiderman.svg', likes: 381},
    {id: 2, name: 'Ironman', avatar: 'assets/images/heroes/ironman.svg', likes: 412},
    {id: 3, name: 'Captain America', avatar: 'assets/images/heroes/captain.svg', likes: 221}
  ];

  @ViewChild('tabs') tabs: TabsComponent;

  iconControl = this.fb.control(true);
  stateControl = this.fb.control(null);
  titleControl = this.fb.control(false);
  outlineControl = this.fb.control(null);
  behaviourControl = this.fb.control(null);

  builder = this.fb.group({
    icon: this.iconControl,
    state: this.stateControl,
    title: this.titleControl,
    outline: this.outlineControl,
    behaviour: this.behaviourControl
  });

  constructor(private fb: FormBuilder,
              @Inject(LOCALE_ID) public locale: string) {
  }

  ngOnInit() {
    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));
  }

}
