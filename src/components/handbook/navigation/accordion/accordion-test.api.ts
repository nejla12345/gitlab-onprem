export const ACCORDION_API = {
  'properties': {
    'active': {'description': 'Accordion active section', 'type': 'number', 'default': '0'}
  }
};

export const ACCORDION_SECTION_API = {
  'properties': {
    'title': {'description': 'Accordion section title', 'type': 'string'},
    'icon': {'description': 'Accordion section icon', 'type': 'string'},
    'state': {'description': 'State of accordion', 'path': 'ui.state', 'options': ['warning', 'loading']}
  }
};
