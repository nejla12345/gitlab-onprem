import { Component, Inject, LOCALE_ID, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TabsComponent, UI } from 'esanum-ui';
import { Language as HighlightLanguage } from 'src/components/handbook/shared/code-highlight/enum';
import { HANDBOOK } from 'src/consts';
import { Language } from 'src/enums/language';
import { LocalUI } from 'src/enums/local-ui';
import { TIMELINE_API, TIMELINE_ITEM_API } from './timeline-test.api';

@Component({
  selector: 'app-timeline-test',
  templateUrl: './timeline-test.component.html',
  styleUrls: ['./timeline-test.component.scss']
})
export class TimelineTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  language = Language;
  highlight = {language: HighlightLanguage};
  api = {
    timeline: TIMELINE_API,
    timelineItem: TIMELINE_ITEM_API
  };
  handbook = HANDBOOK;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/collections/timeline';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI-v1?node-id=6587%3A0';

  @ViewChild('tabs') tabs: TabsComponent;

  iconControl = this.fb.control(false);
  colorControl = this.fb.control(this.ui.color.primary);

  builder = this.fb.group({
    icon: this.iconControl,
    color: this.colorControl
  });

  constructor(private fb: FormBuilder,
              @Inject(LOCALE_ID) public locale: string) {
  }

  ngOnInit() {
    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));
  }

}
