export const TIMELINE_API = {
  'content': {
    'timelineItemContentTemplate': {
      'selector': '#timelineItemContentTemplate',
      'description': 'Timeline content template'
    }
  }
};

export const TIMELINE_ITEM_API = {
  'properties': {
    'title': {'description': 'Timeline item title', 'type': 'string'},
    'color': {
      'description': 'Set the color to \'red\' | \'green\' | \'blue\' or other custom colors (css color) for timeline item',
      'type': 'string | Color'
    },
    'icon': {'description': 'Icon', 'type': 'string'}
  },
  'content': {
    'contentTemplate': {'selector': '#timelineItemContentTemplate', 'description': 'timeline item template'}
  }
};
