export const SPINNER_API = {
  'properties': {
    'size': {
      'description': 'Spinner size',
      'path': 'ui.size',
      'default': 'normal',
      'options': ['small', 'normal', 'large']
    }
  }
};
