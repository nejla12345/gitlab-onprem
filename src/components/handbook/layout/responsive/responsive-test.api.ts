export const FOR_API = {
  'properties': {
    'target': {
      'name': 'snFor',
      'description': 'Target break point for rendering',
      'path': 'ui.breakpoints',
      'options': ['mobile', 'tablet', 'desktop', 'wide']
    }
  }
};

export const MIN_FOR_API = {
  'properties': {
    'target': {
      'name': 'snMinFor',
      'description': 'Min break point for rendering',
      'path': 'ui.breakpoints',
      'options': ['mobile', 'tablet', 'desktop', 'wide']
    }
  }
};

export const MAX_FOR_API = {
  'properties': {
    'target': {
      'name': 'snMaxFor',
      'description': 'Max break point for rendering',
      'path': 'ui.breakpoints',
      'options': ['mobile', 'tablet', 'desktop', 'wide']
    }
  }
};
