import { Component, Inject, LOCALE_ID, ViewChild } from '@angular/core';
import { TabDirective, UI } from 'esanum-ui';
import { HANDBOOK } from 'src/consts';
import { Language } from 'src/enums/language';
import { LocalUI } from 'src/enums/local-ui';
import { Language as HighlightLanguage } from '../../shared/code-highlight/enum';
import { SelectorType } from '../../shared/component-api/enums';
import { FOR_API, MAX_FOR_API, MIN_FOR_API } from './responsive-test.api';

@Component({
  selector: 'app-responsive-test',
  templateUrl: './responsive-test.component.html',
  styleUrls: ['./responsive-test.component.scss']
})
export class ResponsiveTestComponent {

  ui = UI;
  localUi = LocalUI;
  selectorType = SelectorType;
  language = Language;
  highlight = {language: HighlightLanguage};
  handbook = HANDBOOK;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/layout/responsive';

  api = {
    for: FOR_API,
    minFor: MIN_FOR_API,
    maxFor: MAX_FOR_API
  };

  @ViewChild('code') code: TabDirective;

  constructor(@Inject(LOCALE_ID) public locale: string) {
  }

}
