export const COLLAPSIBLE_API = {
  'properties': {
    'orientation': {
      'description': 'Collapsible orientation',
      'path': 'ui.orientation',
      'default': 'horizontal',
      'options': ['horizontal', 'vertical']
    },
    'opened': {'description': 'opened of collapsible', 'type': 'boolean', 'default': 'false'},
    'icon': {'description': 'icon of collapsible', 'type': 'string'},
    'title': {'description': 'Title of collapsible', 'type': 'string'}
  },
  'content': {
    'titleTemplate': {'selector': '#collapsibleTitleTemplate', 'description': 'collapsible title template'},
    'contentTemplate': {'selector': '#collapsibleContentTemplate', 'description': 'collapsible content template'}
  }
};

