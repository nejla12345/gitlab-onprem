import { Component, Inject, LOCALE_ID, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TabsComponent, UI } from 'esanum-ui';
import { HANDBOOK } from 'src/consts';
import { Language } from 'src/enums/language';
import { LocalUI } from 'src/enums/local-ui';
import { COLUMN_API, CONTAINER_API, ROW_API } from './grid-test.api';

@Component({
  selector: 'app-grid-test',
  templateUrl: './grid-test.component.html',
  styleUrls: ['./grid-test.component.scss']
})
export class GridTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  language = Language;
  api = {
    container: CONTAINER_API,
    row: ROW_API,
    column: COLUMN_API
  };
  handbook = HANDBOOK;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/layout/grid';

  @ViewChild('tabs') tabs: TabsComponent;

  spacingControl = this.fb.control(null);
  alignControl = this.fb.control(null);
  justifyControl = this.fb.control(null);
  spanControl = this.fb.control(2);
  countControl = this.fb.control(4);
  gutterControl = this.fb.control(null);

  builder = this.fb.group({
    spacing: this.spacingControl,
    align: this.alignControl,
    justify: this.justifyControl,
    span: this.spanControl,
    count: this.countControl,
    gutter: this.gutterControl
  });

  constructor(private fb: FormBuilder,
              @Inject(LOCALE_ID) public locale: string) {
  }

  ngOnInit() {
    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));
  }
}
