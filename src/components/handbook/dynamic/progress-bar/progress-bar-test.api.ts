export const PROGRESS_BAR_API = {
  'properties': {
    'value': {'description': 'Completion percentage', 'type': 'number', 'default': '0'},
    'color': {'description': 'Default color', 'type': 'number', 'default': '0'}
  },
  'content': {
    'progressBarLegendTemplate': {'selector': '#progressBarLegendTemplate', 'description': 'Legend template'}
  }
};

