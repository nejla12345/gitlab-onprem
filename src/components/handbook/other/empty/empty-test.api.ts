export const EMPTY_API = {
  'properties': {
    'icon': {'description': 'Label icon', 'type': 'string'},
    'scheme': {
      'description': 'Message color scheme',
      'path': 'ui.schemes',
      'default': 'primary',
      'options': ['primary', 'secondary', 'success', 'fail', 'accent']
    }
  }
};
