export const SHORTCUTS_API = {
  'properties': {
    'shortcuts': {
      'description': 'Shortcuts array',
      'type': '[{key: ui.kayboard.key, modifiers: [ui.keyboard.modifier], action: Function}]',
      'default': '[]'
    }
  }
};
