export const DOT_API = {
  'properties': {
    'color': {'description': 'Dot color', 'type': 'string', 'default': 'orange'},
    'features': {'description': 'Animation pulse for dot', 'path': 'ui.feature', 'options': ['pulse']}
  }
};
