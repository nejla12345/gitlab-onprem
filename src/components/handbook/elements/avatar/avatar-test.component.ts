import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TabsComponent, UI } from 'esanum-ui';
import { HANDBOOK } from 'src/consts';
import { LocalUI } from 'src/enums/local-ui';
import { AVATAR_API, AVATARS_GROUP_API, AVATARS_LIST_API } from './avatar-test.api';

export enum Sketches {
  icon = 'icon',
  initials = 'initials',
  image = 'image'
}

@Component({
  selector: 'app-avatar-test',
  templateUrl: './avatar-test.component.html',
  styleUrls: ['./avatar-test.component.scss']
})
export class AvatarTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  sketches = Sketches;
  api = {
    avatar: AVATAR_API,
    avatarsList: AVATARS_LIST_API,
    avatarsGroup: AVATARS_GROUP_API
  };
  handbook = HANDBOOK;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/elements/avatar';
  figma = 'https://www.figma.com/file/EIUNwZCXL9Nm5BKQKl43mfDr/Junte-UI-v1?node-id=791%3A0';

  @ViewChild('tabs') tabs: TabsComponent;

  sizeControl = this.fb.control(null);
  shapeControl = this.fb.control(null);
  dotControl = this.fb.control(false);
  sketchControl = this.fb.control(this.sketches.image);

  builder = this.fb.group({
    size: this.sizeControl,
    shape: this.shapeControl,
    sketch: this.sketchControl,
    dot: this.dotControl
  });

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));
  }

}
