export const AVATAR_API = {
  'properties': {
    'size': {
      'description': 'Avatar size',
      'path': 'ui.size',
      'default': 'normal',
      'options': ['tiny', 'small', 'normal', 'large']
    },
    'shape': {'description': 'Avatar shape', 'path': 'ui.shape', 'default': 'circle', 'options': ['circle', 'square']},
    'icon': {'description': 'Icon on avatar', 'type': 'string', 'default': 'ui.icons.user'},
    'name': {'description': 'First char of name on avatar', 'type': 'string'},
    'surname': {'description': 'First char of surname on avatar', 'type': 'string'},
    'image': {'description': 'Image on avatar', 'type': 'string'}
  }
};

export const AVATARS_LIST_API = {
  'properties': {
    'size': {
      'description': 'Avatars list size',
      'path': 'ui.size',
      'default': 'normal',
      'options': ['tiny', 'small', 'normal', 'large']
    }
  }
};

export const AVATARS_GROUP_API = {
  'properties': {
    'size': {
      'description': 'Group size',
      'path': 'ui.size',
      'default': 'normal',
      'options': ['tiny', 'small', 'normal', 'large']
    }, 'total': {'description': 'Total avatars (users)', 'type': 'number', 'default': 0}
  }
};
