export const PICTURE_API = {
  'properties': {
    'icon': {'description': 'Icon on picture', 'type': 'string', 'default': 'ui.icons.image'},
    'src': {'description': 'Path to image on picture', 'type': 'string'},
    'title': {'description': 'Picture title', 'type': 'string'},
    'alt': {'description': 'Picture alt', 'type': 'string'},
    'width': {'description': 'Picture width', 'type': 'string'},
    'height': {'description': 'Picture height', 'type': 'string'},
    'fit': {
      'description': 'Image size in relation to width or height',
      'path': 'ui.fit',
      'default': 'width',
      'options': ['width', 'height']
    },
    'position': {
      'description': 'Image position',
      'path': 'ui.position',
      'default': 'center',
      'options': ['center', 'left', 'right', 'bottom', 'top']
    },
    'loading': {
      'description': 'Set the loading status of picture',
      'type': 'boolean',
      'default': 'false'
    },
    'attributes': {
      'description': 'Set custom picture attributes',
      'type': '{ [key: string]: string }'
    },
  }
};
