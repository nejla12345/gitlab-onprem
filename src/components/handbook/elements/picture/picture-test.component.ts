import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TabsComponent, UI } from 'esanum-ui';
import { Fit } from 'projects/esanum-ui/src/lib/core/enums/fit';
import { PICTURE_API } from './picture-test.api';
import { HANDBOOK } from 'src/consts';
import { LocalUI } from 'src/enums/local-ui';

export enum Sketches {
  icon = 'icon',
  image = 'image'
}

@Component({
  selector: 'app-picture-test',
  templateUrl: './picture-test.component.html',
  styleUrls: ['./picture-test.component.scss']
})
export class PictureTestComponent implements OnInit {

  ui = UI;
  localUi = LocalUI;
  sketches = Sketches;
  api = {
    picture: PICTURE_API
  };
  handbook = HANDBOOK;
  fit = Fit;

  gitlab = 'https://gitlab.com/junte/esanum/social/ui/-/tree/master/projects/esanum-ui/src/lib/elements/picture';

  @ViewChild('tabs') tabs: TabsComponent;

  imageControl = this.fb.control(true);
  iconControl = this.fb.control(false);
  typeControl = this.fb.control(this.sketches.image);
  fitControl = this.fb.control(null);
  positionControl = this.fb.control(null);
  widthControl = this.fb.control(300);
  heightControl = this.fb.control(200);
  loadingControl = this.fb.control(null);
  copyrightControl = this.fb.control(null);
  altControl = this.fb.control(null);
  titleControl = this.fb.control(null);
  customControl = this.fb.control(null);

  builder = this.fb.group({
    image: this.imageControl,
    icon: this.iconControl,
    type: this.typeControl,
    fit: this.fitControl,
    position: this.positionControl,
    width: this.widthControl,
    height: this.heightControl,
    loading: this.loadingControl,
    copyright: this.copyrightControl,
    alt: this.altControl,
    title: this.titleControl,
    custom: this.customControl
  });

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.builder.valueChanges
      .subscribe(() => this.tabs.flash(1));

    this.typeControl.valueChanges
      .subscribe(value => {
        if (value === this.sketches.icon) {
          this.copyrightControl.disable();
          this.customControl.disable();
          this.altControl.disable();
          this.titleControl.disable();
        } else {
          this.copyrightControl.enable();
          this.customControl.enable();
          this.altControl.enable();
          this.titleControl.enable();
        }
      });
  }

}
