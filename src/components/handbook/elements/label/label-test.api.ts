export const LABEL_API = {
  'properties': {
    'label': {'description': 'Label text', 'type': 'string'},
    'icon': {'description': 'Label icon', 'type': 'string'},
    'color': {'description': 'Label background color', 'type': 'string', 'default': 'purple'},
    'size': {'description': 'Label size', 'path': 'ui.size', 'default': 'normal', 'options': ['small', 'normal']},
    'outline': {'description': 'Label outline', 'path': 'ui.outline', 'default': 'fill', 'options': ['ghost', 'fill']}
  }
};
