export const BADGE_API = {
  'properties': {
    'text': {'description': 'Text to show in badge', 'type': 'string'},
    'value': {'description': 'Number to show in badge', 'type': 'number'},
    'overflow': {'description': 'Max count to show', 'type': 'number'},
    'features': {'description': 'Features for badge', 'path': 'ui.feature', 'options': ['overflow'], 'default': '[Feature.overflow]'},
    'color': {'description': 'Badge background color', 'type': 'string', 'default': 'ui.color.primary'},
    'position': {'description': 'Badge position', 'path': 'ui.position', 'default': 'inline', 'options': ['inline', 'rightTop', 'leftTop']}
  }
};
