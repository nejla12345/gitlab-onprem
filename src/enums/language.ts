export enum Language {
  en = 'en',
  ru = 'ru',
  de = 'de'
}
