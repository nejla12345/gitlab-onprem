import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AnalyticsDirective } from './analytics';

@NgModule({
  declarations: [
    AnalyticsDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    AnalyticsDirective
  ]
})

export class AnalyticsDirectivesModule {
}
