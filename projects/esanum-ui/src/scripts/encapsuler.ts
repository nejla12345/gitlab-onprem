/* tslint:disable:no-string-literal */
import * as fs from 'fs';
import * as gulp from 'gulp';
import * as debug from 'gulp-debug';
import { Gulpclass, SequenceTask, Task } from 'gulpclass';
import * as map from 'map-stream';
import { HTMLElement, Node, parse } from 'node-html-parser';
import * as path from 'path';
import * as createQueryWrapper from 'query-ast';
import 'reflect-metadata';
import { parse as scssParce, stringify } from 'scss-parser';

const buildFiles = './**/build.json';
const nodeModulesFiles = '!node_modules/**';

const argument = require('minimist')(process.argv.slice(2));

const IDENTIFIER_TYPE = 'identifier';
const SELECTOR_TYPE = 'selector';
const PSEUDO_CLASS_TYPE = 'pseudo_class';
const ATTRIBUTE_TYPE = 'attribute';
const ARGUMENTS_TYPE = 'arguments';

class Encapsuler {
  host: string;
  html: { from: string, to: string };
  scss: { from: string, to: string };
}

class Builder {
  encapsuler: Encapsuler[];
}

const files: { [key: string]: string } = {};

@Gulpclass()
export class Gulpfile {

  private setHost(nodes: Node[], host: string) {
    return nodes.map(node => {
      if (node['tagName'] && !node['tagName'].startsWith('ng-')) {
        node['rawAttrs'] = `_${host} ${node['rawAttrs']}`;
      }
      node.childNodes = this.setHost(node.childNodes, host);
      return node;
    });
  }

  private encapsulateHTML(from: string, to: string, host: string) {
    if (!!from) {
      const templateContent = fs.readFileSync(from, 'utf8');
      if (!!templateContent) {
        const html = parse(templateContent) as HTMLElement;
        html.childNodes = this.setHost(html.childNodes, host);
        html.set_content(html.toString());

        if (!fs.existsSync(path.dirname(to))) {
          fs.mkdirSync(path.dirname(to), {recursive: true});
        }
        fs.writeFileSync(to, html.toString());
      }
    }
  }

  private encapsulateSCSS(from: string, to: string, _host: string) {
    const host = `[${_host}]`;
    const child = `[_${_host}]`;
    let styleContent = fs.readFileSync(from, 'utf8');
    if (!!styleContent) {
      if (!fs.existsSync(path.dirname(to))) {
        fs.mkdirSync(path.dirname(to), {recursive: true});
      }
      if (!!_host) {
        const $ = createQueryWrapper(scssParce(styleContent));

        const query = $(n => n.node.type === IDENTIFIER_TYPE && n.parent.node.type === SELECTOR_TYPE
          || (n.node.type === ATTRIBUTE_TYPE && n.parent.node.value[0].value !== '&'));

        query.nodes.forEach((n, index) => {
          if (!n.node.value.includes('webkit') && n.parent.node.type !== ARGUMENTS_TYPE) {
            query.eq(index).after({value: child});
          }
        });

        const hostQuery = $(n => n.node.type === IDENTIFIER_TYPE
          && n.parent.node.type === PSEUDO_CLASS_TYPE
          && n.node.value === 'host');
        hostQuery.parent().replace(() => ({value: host}));

        styleContent = stringify($().get(0));
      }

      fs.writeFileSync(to, styleContent.toString());
    } else {
      if (fs.existsSync(to)) {
        fs.unlink(to, error => console.log(error));
      }
    }
  }

  @Task()
  components() {
    return gulp.src([buildFiles, nodeModulesFiles])
      .pipe(debug())
      .pipe(map((file, cb) => {
        const encapsuler = (JSON.parse(file.contents.toString()) as Builder).encapsuler;

        if (!!encapsuler) {
          if (!!encapsuler && !!encapsuler.length) {
            encapsuler.forEach(e => {
              if (!!e.html) {
                const from = path.normalize(`${path.dirname(file.path)}/${e.html.from}`);
                const content = fs.readFileSync(from, 'hex');
                if (files[from] !== content) {
                  console.log('changed: ', from);
                  const to = path.normalize(`${path.dirname(file.path)}/${e.html.to}`);
                  this.encapsulateHTML(from, to, e.host);
                  files[from] = content;
                }
              }
              if (!!e.scss) {
                const from = path.normalize(`${path.dirname(file.path)}/${e.scss.from}`);
                const content = fs.readFileSync(from, 'hex');
                if (files[from] !== content) {
                  console.log('changed: ', from);
                  const to = path.normalize(`${path.dirname(file.path)}/${e.scss.to}`);
                  this.encapsulateSCSS(from, to, e.host);
                  files[from] = content;
                }
              }
            });
          }
        }
        return cb();
      }));
  }

  @SequenceTask()
  build() {
    const listTask = ['components'];

    if (argument.watch) {
      listTask.push('watch');
    }

    return listTask;
  }

  @Task()
  watch() {
    return gulp.watch(['./**/*.component.html', './**/*.component.scss',
      '!node_modules/**/*.scss', '!node_modules/**/*.html'], () => this.components());
  }
}
