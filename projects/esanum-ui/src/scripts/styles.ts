import * as fs from 'fs';
import * as gulp from 'gulp';
import { Gulpclass, SequenceTask, Task } from 'gulpclass';
import * as map from 'map-stream';
import * as path from 'path';
import 'reflect-metadata';

const argument = require('minimist')(process.argv.slice(2));
const BASE_FILES = './src/lib/assets/styles/*.scss';
const COMPONENTS_FILES = './src/lib/assets/styles/**/*.scss';
const BUILD_FILES = './src/lib/**/build.json';

class Style {
  constructor(public section: string,
              public to: string) {
  }
}

class Component {
  constructor(public host: string,
              public name: string) {
  }
}

class Composition {
  section: string;
  pathFrom: string;
  pathTo: string;
  from: string[];
  to: string;
}

class Encapsuler {
  host: string;
  name: string;
  html: {
    from: string;
    to: string;
  };
  scss: {
    from: string;
    to: string;
  };
}

class Builder {
  composition: Composition;
  encapsuler: Encapsuler[];
}

@Gulpclass()
export class Gulpfile {

  private _styles: Style[] = [];
  private _components: Component[] = [];

  private clearImports(content: string, section: string, to: string) {
    let imports = content.match(/@import.*$/gm);
    imports = imports.filter(file => !file.includes('sn-variables')
      && !file.includes(`${section}/${to.replace('.scss', '').replace('_', '')}`));

    const clean = file => file.replace(/";|';/, '').replace(/@import ["|']/, '@import \'../').split('/');
    imports = [...(new Set(imports.map(file => clean(file).slice(0, 3).join('/') + '\';')))];

    let cleared = content
      .replace(/@import.*$/gm, '')
      .replace(/(\n){2,}/gm, '\n');
    imports.forEach(file => cleared = `${file}\n${cleared}`);
    return `@import '../sn-variables';\n${cleared}`;
  }

  // Encapsulation

  @Task()
  builders() {
    return gulp.src([BUILD_FILES])
      .pipe(map((file, cb) => {
        const builder = (JSON.parse(file.contents.toString()) as Builder);
        const encapsulers = builder.encapsuler;
        if (!!encapsulers) {
          this._components = this._components.concat(encapsulers
            .filter(encapsuler => !!encapsuler.host)
            .map(encapsuler => ({
              host: encapsuler.host,
              name: encapsuler.name
            })));
        }
        return cb(null, file);
      }));
  }

  @Task()
  checkHosts(done) {
    const hosts = {};
    this._components.forEach(comp => hosts[comp.host] = ++hosts[comp.host] || 1);
    for (let dup in hosts) {
      if (hosts[dup] === 1) {
        delete hosts[dup];
      }
    }
    const duplicates = Object.keys(hosts);
    if (duplicates.length > 0) {
      throw Error(`Duplicated hosts: ${duplicates.join(', ')}`);
    }
    done();
  }

  @Task()
  hostsStyles() {
    return gulp.src(['./src/lib/assets/styles/sn-hosts.scss'])
      .pipe(map((file, cb) => {
        const hosts = this._components
          .sort((a, b) => a.host.localeCompare(b.host))
          .map(comp => `$sn-${comp.name}-host: '[${comp.host}]';`)
          .join('\n');
        file.contents = Buffer.from(hosts);
        return cb(null, file);
      }))
      .pipe(gulp.dest('./src/lib/assets/styles/'));
  }

  @SequenceTask()
  hosts(done) {
    const tasks = ['builders', 'checkHosts', 'hostsStyles'];
    if (argument.watch) {
      tasks.push('watch');
    }
    done();
    return tasks;
  }

  // Save styles and mixins

  @Task()
  mixins() {
    return gulp.src(['./src/lib/assets/styles/sn-mixins.scss'])
      .pipe(map((file, cb) => {
        const content = this._styles
          .map(component => `@import './${component.section}/${component.to}';`)
          .join('\r\n');
        fs.writeFileSync('./../../dist/esanum-ui/lib/assets/styles/sn-mixins.scss', content);
        return cb(null, file);
      }));
  }

  @Task()
  styles() {
    return gulp.src([BUILD_FILES])
      .pipe(map((file, cb) => {
        const builder = (JSON.parse(file.contents.toString()) as Builder);
        const composition = builder.composition;
        if (!!composition) {
          const from = path.normalize(`${path.dirname(file.path)}/${composition.pathFrom}`);
          const to = path.normalize(`${path.dirname(file.path)}/${composition.pathTo}`);
          let content = '';

          composition.from.forEach(scss => content +=
            (fs.readFileSync(`${from}/${composition.section}/${scss}`).toString() + '\n\r'));

          if (!fs.existsSync(`${to}/${composition.section}`)) {
            fs.mkdirSync(`${to}/${composition.section}`, {recursive: true});
            fs.writeFileSync(`${to}/${composition.section}.scss`, '');
          }

          fs.writeFileSync(`${to}/${composition.section}/${composition.to}`,
            this.clearImports(content, composition.section, composition.to));

          fs.appendFileSync(`${to}/${composition.section}.scss`,
            `@import "./${composition.section}/${composition.to}";\n`);

          this._styles.push(new Style(composition.section, composition.to));
        }
        return cb(null, file);
      }));
  }

  @Task()
  base() {
    return gulp.src([BASE_FILES])
      .pipe(gulp.dest('../../dist/esanum-ui/lib/assets/styles'));
  }

  @SequenceTask()
  build(done) {
    const tasks = ['styles', 'mixins'];
    if (argument.watch) {
      tasks.push('watch');
    }
    done();
    return tasks;
  }

  @Task()
  watch(done) {
    done();
    return gulp.watch([BASE_FILES, COMPONENTS_FILES, BUILD_FILES],
      {ignoreInitial: false}, gulp.series('styles', 'base', 'mixins'));
  }
}
