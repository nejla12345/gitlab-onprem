import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { EsanumUIConfig } from '../../config';
import { I18nLoader } from './loader';

export function i18nLoaderFactory(config: EsanumUIConfig) {
  return new I18nLoader(config.i18n);
}

export const I18N_PROVIDERS = TranslateModule.forRoot({
  isolate: true,
  loader: {
    provide: TranslateLoader,
    useFactory: i18nLoaderFactory,
    deps: [EsanumUIConfig]
  },
  defaultLanguage: 'en'
}).providers;
