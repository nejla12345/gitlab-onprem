import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

export class BindingConfig {
  attributes: { [key: string]: string };

  constructor(defs: Partial<BindingConfig> = null) {
    Object.assign(this, defs);
  }
}

@Directive({
  selector: '[snBinding]',
  exportAs: 'snBinding'
})
export class BindingDirective {

  @Input('snBinding')
  set config(config: Partial<BindingConfig>) {
    Object.keys(config.attributes || {}).forEach(name => this.renderer
      .setAttribute(this.hostRef.nativeElement, name, config.attributes[name]));
  }

  constructor(private hostRef: ElementRef,
              private renderer: Renderer2) {
  }

}
