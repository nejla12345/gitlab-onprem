import { DatePipe } from '@angular/common';
import { CustomNGXLoggerService, LoggerConfig, NGXLogger, NGXLoggerHttpService, NGXMapperService } from 'ngx-logger';
import { EsanumUIConfig } from '../../config';

export function loggerConfigFactory(config: EsanumUIConfig): LoggerConfig {
  const logger = new LoggerConfig();
  logger.level = config.logger;
  return logger;
}

export const LOGGER_PROVIDERS = [
  {
    provide: LoggerConfig,
    useFactory: loggerConfigFactory,
    deps: [EsanumUIConfig]
  },
  NGXLogger,
  NGXLoggerHttpService,
  CustomNGXLoggerService,
  NGXMapperService,
  DatePipe
];
