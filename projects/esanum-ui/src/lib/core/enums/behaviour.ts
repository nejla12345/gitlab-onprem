export enum Behaviour {
  dropdown = 'dropdown',
  multiple = 'multiple',
  single = 'single'
}
