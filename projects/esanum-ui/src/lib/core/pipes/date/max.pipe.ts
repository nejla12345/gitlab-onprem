import { Pipe, PipeTransform } from '@angular/core';
import max from 'date-fns/max';

@Pipe({name: 'snMax'})
export class MaxPipe implements PipeTransform {
  transform(dates: Date[]): Date {
    return max(dates);
  }
}
