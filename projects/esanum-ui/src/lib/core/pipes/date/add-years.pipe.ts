import { Pipe, PipeTransform } from '@angular/core';
import { addYears } from 'date-fns';

@Pipe({name: 'snAddYears'})
export class AddYearsPipe implements PipeTransform {
  transform(date: Date, amount: number): Date {
    return addYears(date, amount);
  }
}
