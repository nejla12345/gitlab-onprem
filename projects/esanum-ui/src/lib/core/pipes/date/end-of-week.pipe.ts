import { Pipe, PipeTransform } from '@angular/core';
import endOfWeek from 'date-fns/endOfWeek';
import { EsanumUIConfig } from '../../../config';

@Pipe({name: 'snEndOfWeek'})
export class EndOfWeekPipe implements PipeTransform {

  constructor(private config: EsanumUIConfig) {
  }

  transform(date: Date): Date {
    return endOfWeek(date, {
      locale: this.config.locale.dfns,
      weekStartsOn: this.config.weekStartsOn
    });
  }
}
