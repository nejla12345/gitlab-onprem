import { Pipe, PipeTransform } from '@angular/core';
import isBefore from 'date-fns/isBefore';

@Pipe({name: 'snIsBefore'})
export class IsBeforePipe implements PipeTransform {
  transform(date: Date, dateToCompare: Date): boolean {
    return isBefore(date, dateToCompare);
  }
}
