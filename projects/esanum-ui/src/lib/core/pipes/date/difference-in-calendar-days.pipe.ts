import { Pipe, PipeTransform } from '@angular/core';
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';

@Pipe({name: 'snDifferenceInCalendarDays'})
export class DifferenceInCalendarDaysPipe implements PipeTransform {
  transform(dateLeft: Date, dateRight: Date): number {
    return differenceInCalendarDays(dateLeft, dateRight);
  }
}
