import { Pipe, PipeTransform, } from '@angular/core';
import format from 'date-fns/format';
import { EsanumUIConfig } from '../../../config';

@Pipe({name: 'snFormat'})
export class FormatPipe implements PipeTransform {

  constructor(private config: EsanumUIConfig) {
  }

  transform(date: Date, dateFormat: string): string {
    return format(date, dateFormat, {
      locale: this.config.locale.dfns,
      weekStartsOn: this.config.weekStartsOn
    });
  }
}
