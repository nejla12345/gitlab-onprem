import { Component, HostBinding, Input } from '@angular/core';
import { FlexAlign } from '../../../core/enums/flex';
import { Gutter } from '../../../core/enums/gutter';
import { Orientation } from '../../../core/enums/orientation';
import { UI } from '../../../core/enums/ui';

@Component({
  selector: 'sn-form-item',
  templateUrl: './form-item.encapsulated.html'
})
export class FormItemComponent {

  @HostBinding('attr.fi')
  readonly host = '';

  ui = UI;

  private _orientation: Orientation = Orientation.vertical;
  private _align: FlexAlign = FlexAlign.stretch;
  private _gutter: Gutter = Gutter.tiny;

  @HostBinding('attr.data-sn-orientation')
  @Input()
  set orientation(type: Orientation) {
    this._orientation = type || Orientation.vertical;
  }

  get orientation() {
    return this._orientation;
  }

  @Input()
  set align(align: FlexAlign) {
    this._align = align || FlexAlign.stretch;
  }

  get align() {
    return this._align;
  }

  @Input()
  set gutter(gutter: Gutter) {
    this._gutter = gutter || Gutter.tiny;
  }

  get gutter() {
    return this._gutter;
  }

}
