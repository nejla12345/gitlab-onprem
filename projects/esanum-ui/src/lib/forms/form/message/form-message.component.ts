import { Component, HostBinding, Input } from '@angular/core';
import { Validator } from '../../../core/enums/validator';

@Component({
  selector: 'sn-form-message',
  templateUrl: './form-message.encapsulated.html'
})
export class FormMessageComponent {

  @HostBinding('attr.fg')
  readonly host = '';

  @HostBinding('attr.data-sn-hidden')
  get hidden() {
    return !this.active;
  }

  @Input()
  active = false;

  @HostBinding('style.display')
  get style() {
    return this.active || !this.validator ? 'inline-block' : 'none';
  }

  @Input()
  validator: Validator = null;
}
