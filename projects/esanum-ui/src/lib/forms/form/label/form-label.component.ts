import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { AbstractControl, ControlContainer, FormGroupName } from '@angular/forms';

@Component({
  selector: 'sn-form-label',
  templateUrl: './form-label.encapsulated.html'
})
export class FormLabelComponent implements OnInit {

  @HostBinding('attr.fl')
  readonly host = '';

  required = false;

  @Input()
  for: string;

  constructor(private parent: ControlContainer) {
  }

  ngOnInit() {
    const control = (<FormGroupName>this.parent).control.get(this.for);
    if (!!control) {
      control.statusChanges.subscribe(() => this.check(control));
      this.check(control);
    }
  }

  check(control: AbstractControl) {
    if (!!control.validator && !!control.validator(<AbstractControl>{})) {
      this.required = control.validator(<AbstractControl>{}).required;
    } else {
      this.required = false;
    }
  }
}
