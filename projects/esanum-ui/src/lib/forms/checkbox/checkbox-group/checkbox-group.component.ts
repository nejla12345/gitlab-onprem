import {
  AfterViewInit,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component,
  ContentChildren,
  forwardRef,
  HostBinding,
  HostListener,
  Input,
  QueryList
} from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import { merge, Subscription } from 'rxjs';
import { Feature } from '../../../core/enums/feature';
import { FlexAlign } from '../../../core/enums/flex';
import { Gutter } from '../../../core/enums/gutter';
import { Orientation } from '../../../core/enums/orientation';
import { Size } from '../../../core/enums/size';
import { UI } from '../../../core/enums/ui';
import { LOGGER_PROVIDERS } from '../../../core/logger/providers';
import { CheckboxComponent } from '../checkbox.component';

@Component({
  selector: 'sn-checkbox-group',
  templateUrl: './checkbox-group.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxGroupComponent),
      multi: true
    },
    ...LOGGER_PROVIDERS
  ]
})

export class CheckboxGroupComponent implements ControlValueAccessor, AfterViewInit {

  ui = UI;

  private _subscriptions: {checkboxes: Subscription} = {checkboxes: null};

  @HostBinding('attr.cg')
  readonly host = '';

  private _orientation: Orientation = Orientation.vertical;
  private _spacing: Gutter = Gutter.small;
  private _align: FlexAlign;
  private _size: Size = Size.normal;
  private _selectedItems = [];

  checkboxesControl = this.fb.array([]);
  form = this.fb.group({
    checkboxes: this.checkboxesControl
  });

  @Input()
  set orientation(orientation: Orientation) {
    this._orientation = orientation || Orientation.vertical;
  }

  get orientation() {
    return this.features?.includes(Feature.adapted) ? Orientation.vertical : this._orientation;
  }

  @Input()
  set align(align: FlexAlign) {
    this._align = align;
  }

  get align() {
    return this._align;
  }

  @Input()
  cols = 1;

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }

  get size() {
    return this._size;
  }

  @Input()
  set spacing(spacing: Gutter) {
    this._spacing = spacing || Gutter.small;
  }

  get spacing() {
    return this._spacing;
  }

  @HostBinding('attr.data-sn-features')
  @Input()
  features: Feature[] = [];

  @ContentChildren(CheckboxComponent)
  checkboxes: QueryList<CheckboxComponent>;

  onChange: (value: any) => void = () => this.logger.error('value accessor is not registered');
  onTouched: () => void = () => this.logger.error('value accessor is not registered');
  registerOnChange = fn => this.onChange = fn;
  registerOnTouched = fn => this.onTouched = fn;
  @HostListener('blur') onBlur = () => this.onTouched();

  constructor(private fb: FormBuilder,
              private logger: NGXLogger,
              private cd: ChangeDetectorRef) {
  }

  private listenCheckboxes() {
    this._subscriptions.checkboxes?.unsubscribe();
    this._subscriptions.checkboxes = merge(...this.checkboxes.map(checkbox => checkbox.updated))
      .subscribe(() => this.cd.markForCheck());
  }

  ngAfterViewInit() {
    this.update();
    this.checkboxes.changes.subscribe(() => this.update());
  }

  update() {
    if (!!this.checkboxes) {
      this.checkboxesControl.reset([], {emitEvent: false});
      this.checkboxes.forEach((checkbox, i) => {
        let control = this.checkboxesControl.get(i.toString());
        if (!!control) {
          control.setValue(this._selectedItems.includes(checkbox.value), {emitEvent: false});
        } else {
          control = new FormControl(this._selectedItems.includes(checkbox.value));
          this.checkboxesControl.controls.push(control);
          const index = this.checkboxesControl.length - 1;
          control.valueChanges.subscribe(value => {
            const checkbox = this.checkboxes.toArray()[index].value;
            if (value) {
              this._selectedItems.push(checkbox);
            } else {
              this._selectedItems.splice(this._selectedItems.indexOf(checkbox), 1);
            }
            this.onChange(this._selectedItems);
          });
        }
      });
      this.listenCheckboxes();
      this.cd.markForCheck();
    }
  }

  writeValue(value: any) {
    let selectedItems = [];
    if (!!value) {
      selectedItems = Array.isArray(value) ? value : [value];
    }
    this._selectedItems = selectedItems;
    this.update();
  }

  setDisabledState(isDisabled: boolean) {
    isDisabled ? this.checkboxesControl.disable({emitEvent: false})
      : this.checkboxesControl.enable({emitEvent: false});
  }
}
