import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ResponsiveModule } from '../../layout/responsive/responsive.module';
import { BindingModule } from '../../core/directives/binding.module';
import { MathPipesModule } from '../../core/pipes/math-pipes.module';
import { ArrayPipesModule } from '../../core/pipes/array-pipes.module';
import { IconModule } from '../../elements/icon/icon.module';
import { GridModule } from '../../layout/grid/grid.module';
import { StackModule } from '../../layout/stack/stack.module';
import { CheckboxGroupComponent } from './checkbox-group/checkbox-group.component';
import { CheckboxComponent } from './checkbox.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IconModule,
    StackModule,
    GridModule,
    ArrayPipesModule,
    MathPipesModule,
    BindingModule,
    ResponsiveModule
  ],
  exports: [
    CheckboxComponent,
    CheckboxGroupComponent,
  ],
  declarations: [
    CheckboxComponent,
    CheckboxGroupComponent,
  ],
  entryComponents: [
    CheckboxComponent,
    CheckboxGroupComponent
  ]
})
export class CheckboxModule {
}
