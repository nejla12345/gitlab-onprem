import { animate, style, transition, trigger } from '@angular/animations';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  EventEmitter,
  forwardRef,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  Output,
  TemplateRef
} from '@angular/core';
import { ControlValueAccessor, FormBuilder, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import { EsanumUIConfig } from '../../config';
import { FlexAlign } from '../../core/enums/flex';
import { Size } from '../../core/enums/size';
import { UI } from '../../core/enums/ui';
import { LOGGER_PROVIDERS } from '../../core/logger/providers';

enum AnimationState {
  default = 'default',
  checked = 'checked',
  unchecked = 'unchecked'
}

@Component({
  selector: 'sn-checkbox',
  templateUrl: './checkbox.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true
    },
    ...LOGGER_PROVIDERS
  ],
  animations: [
    trigger('scale', [
        transition(`* => ${AnimationState.default}`, []),
        transition(':enter', [
          style({transform: 'scale(0)'}),
          animate('.3s', style({transform: 'scale(1)'}))
        ]),
        transition(':leave', [
          style({transform: 'scale(1)'}),
          animate('.3s', style({transform: 'scale(0)'}))
        ])
      ]
    )
  ]
})
export class CheckboxComponent implements ControlValueAccessor, OnInit {

  ui = UI;
  animate = AnimationState.default;

  @HostBinding('attr.ch')
  readonly host = '';

  @HostBinding('attr.data-sn-size')
  _size: Size = Size.normal;

  private _align: FlexAlign = FlexAlign.center;
  private _label: string;
  private _labelTemplate: TemplateRef<any>;

  @HostBinding('attr.label')
  @Input()
  set label(label: string) {
    this._label = label;
    this.updated.emit();
  }

  get label() {
    return this._label;
  }

  @ContentChild('checkboxLabelTemplate')
  set labelTemplate(template: TemplateRef<any>) {
    this._labelTemplate = template;
    this.updated.emit();
  }

  get labelTemplate() {
    return this._labelTemplate;
  }

  @Input()
  attributes: { [key: string]: string };

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }

  @Input()
  set align(align: FlexAlign) {
    this._align = align || FlexAlign.center;
  }

  get align() {
    return this._align;
  }

  @Input()
  value: any;

  @Output()
  updated = new EventEmitter<any>();

  checkboxControl = this.fb.control(false);
  form = this.fb.group({
    checkbox: this.checkboxControl
  });

  onChange: (value: any) => void = () => this.logger.error('value accessor is not registered');
  onTouched: () => void = () => this.logger.error('value accessor is not registered');
  registerOnChange = fn => this.onChange = fn;
  registerOnTouched = fn => this.onTouched = fn;
  @HostListener('blur') onBlur = () => this.onTouched();

  constructor(private fb: FormBuilder,
              private logger: NGXLogger,
              private cd: ChangeDetectorRef,
              public config: EsanumUIConfig) {
  }

  ngOnInit() {
    this.checkboxControl.valueChanges
      .subscribe(checked => {
        this.animate = checked ? AnimationState.checked : AnimationState.unchecked;
        this.onChange(checked);
      });
  }

  writeValue(value: boolean) {
    this.checkboxControl.setValue(!!value, {emitEvent: false});
    this.cd.markForCheck();
  }

  setDisabledState(disabled: boolean) {
    disabled ? this.checkboxControl.disable({emitEvent: false})
      : this.checkboxControl.enable({emitEvent: false});
  }
}
