import { Component, ContentChildren, EventEmitter, forwardRef, HostBinding, HostListener, Input, Output, QueryList } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import { Feature } from '../../core/enums/feature';
import { Orientation } from '../../core/enums/orientation';
import { UI } from '../../core/enums/ui';
import { Width } from '../../core/enums/width';
import { LOGGER_PROVIDERS } from '../../core/logger/providers';
import { isEqual } from '../../core/utils/equal';
import { DeviceService } from '../../layout/responsive/device.service';
import { SelectMode } from '../select/enums';
import { Key } from '../select/model';
import { SwitcherOptionDirective } from './switcher-option.directive';

@Component({
  selector: 'sn-switcher',
  templateUrl: './switcher.encapsulated.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SwitcherComponent),
      multi: true
    },
    ...LOGGER_PROVIDERS
  ]
})
export class SwitcherComponent implements ControlValueAccessor {

  @HostBinding('attr.sr')
  readonly host = '';

  ui = UI;

  private _features: Feature[] = [];
  private _orientation: Orientation = Orientation.horizontal;

  @HostBinding('attr.data-sn-width')
  _width: Width = Width.default;

  @HostBinding('attr.data-sn-orientation')
  @Input()
  set orientation(type: Orientation) {
    this._orientation = type || Orientation.horizontal;
  }

  get orientation() {
    return this._orientation;
  }

  @HostBinding('attr.data-sn-disabled')
  @Input()
  disabled = false;

  @Input()
  keyField: string;

  @HostBinding('attr.data-sn-mode')
  _mode: SelectMode = SelectMode.single;

  @Input()
  set mode(mode: SelectMode) {
    this._mode = mode || SelectMode.single;
  }

  get mode() {
    return this._mode;
  }

  @Input()
  set features(features: Feature[]) {
    this._features = features || [];
  }

  get features() {
    return this._features;
  }

  @Input()
  capacity = 3;

  @Input()
  loading = false;

  @Input()
  set width(width: Width) {
    this._width = width || Width.default;
  }

  @Output('selected')
  updated = new EventEmitter<any>();

  @ContentChildren(SwitcherOptionDirective)
  options: QueryList<SwitcherOptionDirective>;

  selected: any[] = [];
  version = 0;

  onChange: (value: any) => void = () => this.logger.error('value accessor is not registered');
  onTouched: () => void = () => this.logger.error('value accessor is not registered');
  registerOnChange = fn => this.onChange = fn;
  registerOnTouched = fn => this.onTouched = fn;
  @HostListener('blur') onBlur = () => this.onTouched();

  constructor(private logger: NGXLogger,
              public device: DeviceService) {
  }

  writeValue(value: any | any[]) {
    if (this.mode === SelectMode.multiple && !value) {
      throw new Error('Wrong value form multiple select mode');
    }

    this.selected = (this.mode === SelectMode.single
      ? ((value ?? null) !== null ? [value] : []) : value) as Key[];
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }

  select(value: any) {
    switch (this.mode) {
      case SelectMode.single:
        const current = this.selected.length > 0 ? this.selected[0] : null;
        if (current !== null) {
          const same = !!this.keyField
            ? current[this.keyField] === value[this.keyField]
            : isEqual(current, value);
          if (same && !this.features.includes(Feature.allowEmpty)) {
            return;
          }

          this.selected = same || value === null ? [] : [value];
          this.onChange(same ? null : value);
          this.updated.emit(same ? null : value);
        } else {
          this.selected = value === null ? [] : [value];
          this.onChange(value);
          this.updated.emit(value);
        }

        this.version++;
        break;
      case SelectMode.multiple:
        const index = !!this.keyField
          ? this.selected.indexOf(value[this.keyField])
          : this.selected.findIndex(e => isEqual(e, value));
        if (index !== -1) {
          this.selected.splice(index, 1);
        } else {
          this.selected.push(value);
        }
        this.version++;
        this.onChange(this.selected);
        this.updated.emit(this.selected);
        break;
    }
  }

  selectAll() {
    this.options.forEach(o => this.selected.push(o.value));
    this.version++;
    this.onChange(this.selected);
    this.updated.emit(this.selected);
  }
}
