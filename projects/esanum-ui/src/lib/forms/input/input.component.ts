import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { ControlValueAccessor, FormBuilder, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { Feature } from '../../core/enums/feature';
import { Key } from '../../core/enums/keyboard';
import { Size } from '../../core/enums/size';
import { State } from '../../core/enums/state';
import { TextAlign, TextTransform } from '../../core/enums/text';
import { UI } from '../../core/enums/ui';
import { Width } from '../../core/enums/width';
import { LOGGER_PROVIDERS } from '../../core/logger/providers';
import { InputAutocomplete, InputScheme, InputType } from './input.enums';

const DIGIT_MASK_CHAR = '_';
const DIGIT_KEYS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
const DEFAULT_NUMBER = 0;

@Component({
  selector: 'sn-input',
  templateUrl: './input.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true
    },
    ...LOGGER_PROVIDERS
  ]
})
export class InputComponent implements OnInit, ControlValueAccessor {

  @HostBinding('attr.in')
  readonly host = '';

  ui = UI;
  textTransform = TextTransform;
  view = {password: {display: false}};
  copied = false;

  private _mask = '';
  private _type: InputType = InputType.text;
  private _placeholder = '';

  inputControl = this.fb.control(null);
  formattedControl = this.fb.control(null);
  form = this.fb.group({
    input: this.inputControl,
    formatted: this.formattedControl
  });

  @HostBinding('attr.data-sn-focused')
  focused = false;

  @HostBinding('attr.data-sn-disabled')
  disabled = false;

  @HostBinding('attr.data-sn-scheme')
  _scheme: InputScheme = InputScheme.normal;

  @HostBinding('attr.data-sn-size')
  _size: Size = Size.normal;

  @HostBinding('attr.data-sn-width')
  _width: Width = Width.default;

  @HostBinding('attr.data-sn-with-icon')
  get withIcon() {
    return !!this.icon;
  }

  @Input()
  icon: string;

  @Input()
  label: string;

  @Input()
  name: string = null;

  @Input()
  transform: TextTransform;

  @Input()
  autocomplete: InputAutocomplete = InputAutocomplete.off;

  @HostBinding('attr.data-sn-textAlign')
  @Input()
  textAlign: TextAlign = TextAlign.left;

  @Input()
  min: number = null;

  @Input()
  max: number = null;

  @Input()
  step = 1;

  @Input()
  readonly = false;

  @Input()
  set scheme(scheme: InputScheme) {
    this._scheme = scheme || InputScheme.normal;
  }

  @Input()
  set placeholder(placeholder: string) {
    this._placeholder = placeholder || '';
  }

  get placeholder() {
    return this._placeholder;
  }

  @Input()
  set type(type: InputType) {
    this._type = type || InputType.text;
  }

  get type() {
    return this._type;
  }

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }

  @Input() set width(width: Width) {
    this._width = width || Width.default;
  }

  @HostBinding('attr.data-sn-state')
  @Input()
  state: State;

  @Input()
  rows = 5;

  @Input()
  set mask(mask: string) {
    this._mask = mask || '';
    if (!!mask) {
      this.form.setValue(this.masking(this.inputControl.value || ''));
    }

    this.cd.markForCheck();
  }

  get mask() {
    return this._mask;
  }

  @HostBinding('attr.data-sn-features')
  @Input()
  features: Feature[] = [];

  @Output()
  click = new EventEmitter<any>();

  @HostBinding('attr.tabindex')
  tabindex = 1;

  @ViewChild('valueRef', {read: ElementRef, static: false})
  valueRef: ElementRef<HTMLInputElement>;

  @ViewChild('maskedRef', {read: ElementRef, static: false})
  maskedRef: ElementRef<HTMLInputElement>;

  @ViewChild('layoutRef', {read: ElementRef})
  layoutRef: ElementRef<HTMLElement>;

  onChange: (value: any) => void = () => this.logger.error('value accessor is not registered');
  onTouched: () => void = () => this.logger.error('value accessor is not registered');
  registerOnChange = fn => this.onChange = fn;
  registerOnTouched = fn => this.onTouched = fn;
  @HostListener('blur') onBlur = () => this.onTouched();

  constructor(private fb: FormBuilder,
              private logger: NGXLogger,
              private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.inputControl.valueChanges.subscribe(value => {
      this.onChange(!!value ? (this.type === InputType.number ? +value : value) : null);
      this.cd.markForCheck();
    });

    this.formattedControl.valueChanges.pipe(
      distinctUntilChanged(),
      filter(() => !!this.maskedRef),
      map(formatted => formatted || this.mask)
    ).subscribe(formatted => {
      const position = formatted.indexOf(DIGIT_MASK_CHAR);
      this.maskedRef.nativeElement.setSelectionRange(position, position);

      let cleared = {input: null, formatted: null};
      for (let i = 0; i < this.mask.length; i++) {
        if (this.mask.charAt(i) !== formatted.charAt(i)) {
          cleared = this.masking(formatted.substr(i));
          break;
        }
      }
      if (cleared.input !== this.inputControl.value
        || cleared.formatted !== this.formattedControl.value) {
        this.form.setValue(cleared);
      }
      this.cd.markForCheck();
    });
  }

  private masking(value: string = null): {
    input: string,
    formatted: string
  } {
    const chars = !!value ? value.replace(/\D/gi, '') : '';
    let formatted = this.mask;
    const length = this.mask.split(DIGIT_MASK_CHAR).length - 1;
    for (const char of chars) {
      formatted = formatted.replace(DIGIT_MASK_CHAR, char);
    }

    return {
      input: chars.substr(0, length) || null,
      formatted: formatted !== this.mask ? formatted : null
    };
  }

  pasteMask(event: ClipboardEvent) {
    event.preventDefault();
    const text = event.clipboardData.getData('text');
    const data = this.masking(text);
    this.form.setValue(data);
    this.cd.markForCheck();
  }

  keydownMask(event: KeyboardEvent) {
    const value = this.inputControl.value || '';
    let data = {
      input: '',
      formatted: ''
    };

    if (DIGIT_KEYS.includes(event.key)) {
      if (this.transform === TextTransform.ranks) {
        this._mask = (value + event.key)
          .replace(/\d/g, '_')
          .replace(/(.{3})/g, '$1 ')
          .trim();
      }
      data = this.masking(value + event.key);
    } else if (event.key === Key.backspace) {
      if (this.transform === TextTransform.ranks) {
        this._mask = this.mask.substr(0, this.mask.length - 1).trim();
      }
      data = this.masking(value.substr(0, value.length - 1));
    } else if (event.key === Key.tab
      || event.key === Key.arrowLeft
      || event.key === Key.arrowRight
      || (event.ctrlKey || event.metaKey) && event.key === Key.v) {
      return;
    }
    event.preventDefault();
    if (data !== undefined) {
      this.form.setValue(data);
    }

    this.cd.markForCheck();
  }

  keydown(event: KeyboardEvent) {
    if (this.type === InputType.number) {
      if (this.inputControl.value && this.inputControl.value.length === 1 && event.key === Key.backspace) {
        this.inputControl.setValue(null);
      }
    }
    if (event.key === Key.enter) {
      event.preventDefault();
    }
    this.cd.markForCheck();
  }

  keyup() {
    let value = this.inputControl.value;

    if (this.type === InputType.text && !!this.transform && !!value) {
      switch (this.transform) {
        case TextTransform.capitalize:
          value = value.charAt(0).toUpperCase() + value.slice(1);
          break;
        case TextTransform.lowercase:
          value = value.toLowerCase();
          break;
        case TextTransform.uppercase:
          value = value.toUpperCase();
          break;
      }
      this.inputControl.setValue(value);
    }

    this.cd.markForCheck();
  }

  writeValue(value) {
    this.form.patchValue(!!this.mask ? this.masking(value) : {input: value}, {emitEvent: false});
    this.cd.markForCheck();
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
    this.cd.markForCheck();
  }

  setNumber(step: number) {
    if (this.inputControl.value === '' || this.inputControl.value === null) {
      this.inputControl.setValue(DEFAULT_NUMBER);
    } else {
      let number = +this.inputControl.value + step;
      number = this.max !== undefined && this.max !== null ? Math.min(number, this.max) : number;
      number = this.min !== undefined && this.min !== null ? Math.max(number, this.min) : number;
      this.inputControl.setValue(number);
    }

    this.cd.markForCheck();
  }

  clear(event: MouseEvent) {
    this.inputControl.setValue(null);
    this.formattedControl.setValue(this.mask);
    this.cd.markForCheck();
    event.stopPropagation();
  }

  focus() {
    if (!!this.valueRef) {
      this.valueRef.nativeElement.focus();
    } else if (!!this.maskedRef) {
      this.maskedRef.nativeElement.focus();
    }
  }

  @HostListener('click', ['$event'])
  onClick({target}: { target: HTMLElement }) {
    if ([this.layoutRef.nativeElement].includes(target)) {
      this.focus();
    }
  }

  copy() {
    this.copied = true;
    this.cd.markForCheck();
    setTimeout(() => {
      this.copied = false;
      this.cd.markForCheck();
    }, 2100);
  }
}
