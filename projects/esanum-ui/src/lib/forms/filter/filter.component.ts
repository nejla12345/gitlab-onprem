import { Component, ContentChild, EventEmitter, forwardRef, HostBinding, HostListener, Input, Output, TemplateRef } from '@angular/core';
import { AbstractControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { UI } from '../../core/enums/ui';

@Component({
  selector: 'sn-filter',
  templateUrl: './filter.encapsulated.html',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => FilterComponent),
    multi: true
  }]
})
export class FilterComponent {

  ui = UI;

  @HostBinding('attr.fr')
  readonly host = '';

  value: any;

  @HostBinding('attr.data-sn-active')
  get active() {
    return this.selected || (!!this.control && !!this.control.value) || !!this.value;
  }

  @Input()
  selected: boolean;

  @Input()
  control: AbstractControl;

  @Input()
  placeholder = '';

  @Input()
  icon: string;

  @Input()
  label = '';

  @ContentChild('filterContentTemplate')
  filterContentTemplate: TemplateRef<any>;

  @Output()
  clear = new EventEmitter();

  onChange: (value: any) => void;
  onTouched: () => void;
  registerOnChange = fn => this.onChange = fn;
  registerOnTouched = fn => this.onTouched = fn;
  @HostListener('blur') onBlur = () => this.onTouched();

  writeValue(value: any) {
    this.value = value;
  }

  reset() {
    if (!!this.control) {
      this.control.setValue(null);
    } else if (!!this.onChange) {
      this.value = null;
      this.onChange(this.value);
    } else {
      this.clear.emit();
    }
  }

}
