import { animate, state, style, transition, trigger } from '@angular/animations';

export const BUTTON_ANIMATION = [
  trigger('appear', [
      state(
        'void',
        style({
          opacity: 0
        })
      ),
      state(
        '*',
        style({
          opacity: 1
        })
      ),
      transition(
        'void => *',
        [
          animate('.5s ease-in-out')
        ]
      ),
    ]
  ),

  trigger('visibility', [
    state('show', style({
      visibility: 'visible',
      opacity: 1
    })),
    state('hide', style({
      visibility: 'collapse',
      opacity: 0
    })),
    transition('show <=> hide', [
      animate('.5s ease-in-out')
    ]),
  ])
];
