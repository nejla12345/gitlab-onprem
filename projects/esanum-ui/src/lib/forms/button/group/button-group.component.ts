import {
  AfterViewInit,
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component,
  ContentChildren,
  HostBinding,
  Input,
  QueryList
} from '@angular/core';
import { merge, Subscription } from 'rxjs';
import { Feature } from '../../../core/enums/feature';
import { Outline } from '../../../core/enums/outline';
import { Scheme } from '../../../core/enums/scheme';
import { Size } from '../../../core/enums/size';
import { UI } from '../../../core/enums/ui';
import { Width } from '../../../core/enums/width';
import { ButtonComponent } from '../button.component';

@Component({
  selector: 'sn-button-group',
  templateUrl: './button-group.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonGroupComponent implements AfterViewInit{

  @HostBinding('attr.bg')
  readonly host = '';

  ui = UI;

  private _subscriptions: {buttons: Subscription} = {buttons: null}

  @ContentChildren(ButtonComponent, {descendants: true})
  buttons: QueryList<ButtonComponent>;

  @Input()
  size: Size = Size.normal;

  @Input()
  scheme: Scheme = Scheme.primary;

  @Input()
  outline: Outline = Outline.fill;

  @HostBinding('attr.data-sn-width')
  @Input()
  width: Width = Width.default;

  @HostBinding('attr.data-sn-features')
  @Input()
  features: Feature[] = [];

  constructor(
    private cd: ChangeDetectorRef
  ) {
  }

  ngAfterViewInit() {
    this.listenButtons();
    this.buttons.changes
      .subscribe(() => {
        this.cd.detectChanges();
        this.listenButtons();
      });
  }

  private listenButtons() {
    this._subscriptions.buttons?.unsubscribe();
    this._subscriptions.buttons = merge(...this.buttons.map(button => button.updated))
      .subscribe(() => this.cd.detectChanges());
  }
}
