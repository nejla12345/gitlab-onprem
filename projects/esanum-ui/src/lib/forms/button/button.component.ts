import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  Output,
  QueryList,
  ViewChild
} from '@angular/core';
import { merge, Subscription } from 'rxjs';
import { Gutter } from '../../core/enums/gutter';
import { Outline } from '../../core/enums/outline';
import { Position } from '../../core/enums/position';
import { Scheme } from '../../core/enums/scheme';
import { Shape } from '../../core/enums/shape';
import { Size } from '../../core/enums/size';
import { UI } from '../../core/enums/ui';
import { Width } from '../../core/enums/width';
import { BadgeComponent } from '../../elements/badge/badge.component';
import { BUTTON_ANIMATION } from './button.animation';
import { ButtonType } from './button.enums';

interface Icon {
  icon: string;
  position: Position;
}

@Component({
  selector: 'sn-button',
  templateUrl: './button.encapsulated.html',
  animations: BUTTON_ANIMATION,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonComponent implements AfterViewInit{

  @HostBinding('attr.bt')
  readonly host = '';

  ui = UI;

  private _subscriptions: {badges: Subscription} = {badges: null};

  private _type: ButtonType = ButtonType.button;
  private _text: string;
  private _disabled = false;
  private _ariaLabel: string;
  private _spacing: Gutter = Gutter.small;
  icon: Icon;

  @HostBinding('attr.data-sn-loading')
  _loading = false;

  @HostBinding('attr.data-sn-scheme')
  _scheme: Scheme = Scheme.primary;

  @HostBinding('attr.data-sn-size')
  _size: Size = Size.normal;

  @HostBinding('attr.data-sn-outline')
  _outline: Outline = Outline.fill;

  @HostBinding('attr.data-sn-width')
  _width: Width = Width.default;

  @HostBinding('attr.data-sn-shape')
  _shape: Shape = Shape.square;

  @HostBinding('attr.data-sn-with-text')
  get withText() {
    return !!this._text || this.badges.length > 0;
  }

  @HostBinding('attr.data-sn-disabled')
  get disable() {
    return this._disabled || this.loading;
  }

  @Input()
  set type(type: ButtonType) {
    this._type = type || ButtonType.button;
    this.updated.emit();
  }

  get type() {
    return this._type;
  }

  @Input()
  set text(text: string) {
    this._text = text;
    this.updated.emit();
  }

  get text() {
    return this._text;
  }

  @Input()
  set disabled(disabled: boolean) {
    this._disabled = disabled;
    this.updated.emit();
  }

  get disabled() {
    return this._disabled;
  }

  @Input()
  set ariaLabel(ariaLabel: string) {
    this._ariaLabel = ariaLabel;
    this.updated.emit();
  }

  get ariaLabel() {
    return this._ariaLabel;
  }

  @Input()
  set spacing(spacing: Gutter) {
    this._spacing = spacing || Gutter.small;
    this.updated.emit();
  }

  get spacing() {
    return this._spacing;
  }

  @Input('icon')
  set _icon(icon: string | Icon) {
    this.icon = <Icon>(typeof icon === 'string'
      ? {icon: icon, position: Position.left} : icon);
    this.updated.emit();
  }

  @Input()
  set loading(loading: boolean) {
    this._loading = loading;
    this.updated.emit();
  }

  get loading() {
    return this._loading;
  }

  @Input()
  set scheme(scheme: Scheme) {
    this._scheme = scheme || Scheme.primary;
    this.updated.emit();
  }

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
    this.updated.emit();
  }

  get size() {
    return this._size;
  }

  @Input()
  set outline(outline: Outline) {
    this._outline = outline || Outline.fill;
    this.updated.emit();
  }

  @Input()
  set width(width: Width) {
    this._width = width || Width.default;
    this.updated.emit();
  }

  @Input()
  set shape(shape: Shape) {
    this._shape = shape || Shape.square;
    this.updated.emit();
  }

  @Output()
  click = new EventEmitter<any>();

  @Output()
  updated = new EventEmitter<any>();

  @ContentChildren(BadgeComponent)
  badges: QueryList<BadgeComponent>;

  @ViewChild('buttonRef', {read: ElementRef})
  buttonRef: ElementRef<HTMLButtonElement>;

  constructor(
    private cd: ChangeDetectorRef
  ) {
  }

  ngAfterViewInit() {
    this.listenBadges();
    this.badges.changes
      .subscribe(() => {
        this.cd.detectChanges();
        this.updated.emit();
        this.listenBadges();
      });
  }

  private listenBadges() {
    this._subscriptions.badges?.unsubscribe();
    this._subscriptions.badges = merge(...this.badges.map(badge => badge.updated))
      .subscribe(() => {
        this.cd.detectChanges();
        this.updated.emit();
      });
  }

  focus() {
    this.buttonRef.nativeElement.focus();
  }

}
