import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  forwardRef,
  HostBinding,
  HostListener,
  Input,
  QueryList
} from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import { Feature } from '../../../core/enums/feature';
import { FlexAlign } from '../../../core/enums/flex';
import { Gutter } from '../../../core/enums/gutter';
import { Orientation } from '../../../core/enums/orientation';
import { Size } from '../../../core/enums/size';
import { UI } from '../../../core/enums/ui';
import { LOGGER_PROVIDERS } from '../../../core/logger/providers';
import { SwitchComponent } from '../switch.component';

@Component({
  selector: 'sn-switch-group',
  templateUrl: './switch-group.encapsulated.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SwitchGroupComponent),
      multi: true
    },
    ...LOGGER_PROVIDERS
  ]
})

export class SwitchGroupComponent implements ControlValueAccessor, AfterViewInit {

  ui = UI;

  @HostBinding('attr.sg')
  readonly host = '';

  private _orientation: Orientation = Orientation.vertical;
  private _spacing: Gutter = Gutter.small;
  private _align: FlexAlign;
  private _size: Size = Size.normal;
  private selectedItems = [];

  switchesControl = this.fb.array([]);
  form = this.fb.group({
    switches: this.switchesControl
  });

  @Input()
  set orientation(orientation: Orientation) {
    this._orientation = orientation || Orientation.vertical;
  }

  get orientation() {
    return this.features?.includes(Feature.adapted) ?
      Orientation.vertical : this._orientation;
  }

  @Input()
  set align(align: FlexAlign) {
    this._align = align;
  }

  get align() {
    return this._align;
  }

  @Input()
  cols = 1;

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }

  get size() {
    return this._size;
  }

  @Input()
  set spacing(spacing: Gutter) {
    this._spacing = spacing || Gutter.small;
  }

  get spacing() {
    return this._spacing;
  }

  @HostBinding('attr.data-sn-features')
  @Input()
  features: Feature[] = [];

  @ContentChildren(SwitchComponent)
  switches: QueryList<SwitchComponent>;

  onChange: (value: any) => void = () => this.logger.error('value accessor is not registered');
  onTouched: () => void = () => this.logger.error('value accessor is not registered');
  registerOnChange = fn => this.onChange = fn;
  registerOnTouched = fn => this.onTouched = fn;
  @HostListener('blur') onBlur = () => this.onTouched();

  constructor(private fb: FormBuilder,
              private logger: NGXLogger,
              private cd: ChangeDetectorRef) {
  }

  ngAfterViewInit() {
    this.update();
    this.switches.changes.subscribe(() => this.update());
  }

  update() {
    if (!!this.switches) {
      this.switchesControl.reset([], {emitEvent: false});
      this.switches.forEach((checkbox, i) => {
        let control = this.switchesControl.get(i.toString());
        if (!!control) {
          control.setValue(this.selectedItems.includes(checkbox.value), {emitEvent: false});
        } else {
          control = new FormControl(this.selectedItems.includes(checkbox.value));
          this.switchesControl.controls.push(control);
          const index = this.switchesControl.length - 1;
          control.valueChanges.subscribe(value => {
            const checkbox = this.switches.toArray()[index].value;
            if (value) {
              this.selectedItems.push(checkbox);
            } else {
              this.selectedItems.splice(this.selectedItems.indexOf(checkbox), 1);
            }
            this.onChange(this.selectedItems);
          });
        }
      });
    }
    this.cd.detectChanges();
  }

  writeValue(value: any) {
    let selectedItems = [];
    if (!!value) {
      selectedItems = Array.isArray(value) ? value : [value];
    }
    this.selectedItems = selectedItems;
    this.update();
  }

  setDisabledState(isDisabled: boolean) {
    isDisabled ? this.switchesControl.disable({emitEvent: false})
      : this.switchesControl.enable({emitEvent: false});
  }
}
