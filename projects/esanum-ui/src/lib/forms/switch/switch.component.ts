import { Component, ContentChild, forwardRef, HostBinding, HostListener, Input, OnInit, TemplateRef } from '@angular/core';
import { ControlValueAccessor, FormBuilder, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import { EsanumUIConfig } from '../../config';
import { FlexAlign } from '../../core/enums/flex';
import { Size } from '../../core/enums/size';
import { SwitchStyle } from '../../core/enums/style';
import { UI } from '../../core/enums/ui';
import { LOGGER_PROVIDERS } from '../../core/logger/providers';

@Component({
  selector: 'sn-switch',
  templateUrl: './switch.encapsulated.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SwitchComponent),
      multi: true
    },
    ...LOGGER_PROVIDERS
  ]
})
export class SwitchComponent implements ControlValueAccessor, OnInit {

  @HostBinding('attr.sw')
  readonly host = '';

  ui = UI;
  styles = SwitchStyle;

  switchControl = this.fb.control(false);
  form = this.fb.group({
    switch: this.switchControl
  });

  _style: SwitchStyle;

  @HostBinding('attr.data-sn-size')
  _size: Size = Size.normal;

  _align: FlexAlign = FlexAlign.center;

  @HostBinding('attr.data-sn-focused')
  focused = false;

  get checked() {
    return this.switchControl.value;
  }

  @Input()
  label: string;

  @Input()
  icons: { on?: string, off?: string };

  @Input()
  tags: { on?: string, off?: string };

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }

  get size() {
    return this._size;
  }

  @Input()
  set align(align: FlexAlign) {
    this._align = align || FlexAlign.center;
  }

  get align() {
    return this._align;
  }

  @Input()
  set style(style: SwitchStyle) {
    this._style = style;
  }

  get style() {
    return this._style || this.config.switch?.style || SwitchStyle.default;
  }

  @Input()
  value: any;

  @ContentChild('switchLabelTemplate')
  labelTemplate: TemplateRef<any>;

  get onIcon() {
    return this.icons?.on || this.config.switch?.icons?.on;
  }

  get offIcon() {
    return this.icons?.off || this.config.switch?.icons?.off;
  }

  onChange: (value: any) => void = () => this.logger.error('value accessor is not registered');
  onTouched: () => void = () => this.logger.error('value accessor is not registered');
  registerOnChange = fn => this.onChange = fn;
  registerOnTouched = fn => this.onTouched = fn;
  @HostListener('blur') onBlur = () => this.onTouched();

  constructor(private config: EsanumUIConfig,
              private logger: NGXLogger,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.switchControl.valueChanges
      .subscribe(value => this.onChange(value));
  }

  writeValue(value) {
    this.switchControl.setValue(value, {emitEvent: false});
  }

  setDisabledState(disabled: boolean) {
    disabled ? this.switchControl.disable({emitEvent: false}) : this.switchControl.enable({emitEvent: false});
  }

}
