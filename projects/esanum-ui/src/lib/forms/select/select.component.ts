import {
  AfterContentInit,
  Component,
  ContentChildren,
  Directive,
  ElementRef,
  EventEmitter,
  forwardRef,
  HostBinding,
  HostListener,
  Input,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  Renderer2,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { ControlValueAccessor, FormBuilder, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import { Observable, Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, takeUntil, tap } from 'rxjs/operators';
import { Behaviour } from '../../core/enums/behaviour';
import { Breakpoint } from '../../core/enums/breakpoint';
import { Context } from '../../core/enums/context';
import { Feature } from '../../core/enums/feature';
import { Key as Keyboard } from '../../core/enums/keyboard';
import { Placement } from '../../core/enums/placement';
import { Size } from '../../core/enums/size';
import { State } from '../../core/enums/state';
import { UI } from '../../core/enums/ui';
import { Width } from '../../core/enums/width';
import { LOGGER_PROVIDERS } from '../../core/logger/providers';
import { progress } from '../../core/utils/rxjs';
import { BreakpointService } from '../../layout/responsive/breakpoint.service';
import { DeviceService } from '../../layout/responsive/device.service';
import { PopoverInstance, PopoverService } from '../../overlays/popover/popover.service';
import { InputAutocomplete } from '../input/input.enums';
import { SelectMode } from './enums';
import { IOption, Key, Options } from './model';

const MIN_WIDTH = 20;
const CHAR_WIDTH = 8;
const CHECKING_INTERVAL = 100;

@Directive({
  selector: 'sn-select-option'
})
export class SelectOptionDirective {

  ui = UI;

  @Input()
  icon: string;

  @Input()
  key: Key;

  @Input()
  label: string;

  @Input()
  value: any;
}

const SEARCH_DELAY = 100;

@Component({
  selector: 'sn-select',
  templateUrl: './select.encapsulated.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true
    },
    ...LOGGER_PROVIDERS
  ]
})
export class SelectComponent implements OnInit, AfterContentInit, OnDestroy, ControlValueAccessor {

  @HostBinding('attr.se')
  readonly host = '';

  private reference: { popover: PopoverInstance } = {popover: null};
  private destroyed = new Subject();
  private _features: Feature[] = [];

  ui = UI;

  @HostBinding('attr.data-inline')
  get inline() {
    return this.breakpoint.current === Breakpoint.mobile || this.context === UI.context.popover;
  }

  private fetcher: Subscription;
  private _placement: Placement = Placement.absolute;

  options: Options = {persisted: {}, found: {}};
  changes = {selected: 0, options: 0};
  selected: Key[] = [];
  progress = {options$: new Subject<boolean>()};

  queryControl = this.fb.control({value: null, disabled: true});
  form = this.fb.group(
    {
      query: this.queryControl
    }
  );

  @Input()
  labelField = 'label';

  @Input()
  keyField = 'key';

  @Input()
  groupField = null;

  @Input()
  groupFieldKey = null;

  @Input()
  placeholder = '';

  @Input()
  required = false;

  @HostBinding('attr.data-sn-mode')
  _mode: SelectMode = SelectMode.single;

  @HostBinding('attr.data-sn-size')
  _size: Size = Size.normal;

  @HostBinding('attr.data-sn-width')
  _width: Width = Width.default;

  @Input()
  label: string;

  @Input()
  icon: string;

  @HostBinding('attr.data-sn-state')
  @Input()
  state: State;

  @Input()
  optionTemplate: TemplateRef<any>;

  @Input()
  emptyOptionsTemplate: TemplateRef<any>;

  @Input()
  optionsHeaderTemplate: TemplateRef<any>;

  @ContentChildren(SelectOptionDirective)
  optionsFromMarkup: QueryList<SelectOptionDirective>;

  @ViewChild('optionsTemplate')
  optionsTemplate: TemplateRef<any>;

  @Input()
  groupTemplate: TemplateRef<any>;

  @Output('selected')
  updated = new EventEmitter<any>();

  @HostBinding('attr.data-sn-opened')
  opened = false;

  @Input()
  set placement(placement: Placement) {
    this._placement = placement || Placement.absolute;
  }

  get placement() {
    return this._placement;
  }

  @Input()
  set mode(mode: SelectMode) {
    this._mode = mode || SelectMode.single;
  }

  get mode() {
    return this._mode;
  }

  @HostBinding('attr.data-sn-features')
  @Input()
  set features(features: Feature[]) {
    this._features = features || [];
    this.features.includes(Feature.search)
      ? this.queryControl.enable({emitEvent: false})
      : this.queryControl.disable({emitEvent: false});
  }

  get features() {
    return this._features;
  }

  @Input()
  autocomplete: InputAutocomplete = InputAutocomplete.off;

  @HostBinding('attr.data-sn-disabled')
  @Input()
  disabled = false;

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }

  @Input()
  set width(width: Width) {
    this._width = width || Width.default;
  }

  @HostBinding('attr.data-sn-empty')
  get empty() {
    return this.selected.length === 0;
  }

  @Input()
  loader: (query: string) => Observable<(Object & { icon: string })[]> = null;

  @Input()
  creator: (query: string, close: Function) => Observable<null> | null = null;

  @HostBinding('attr.data-context')
  @Input()
  context: Context;

  @ViewChild('queryRef')
  queryRef: ElementRef<HTMLInputElement>;

  @ViewChild('selectedRef')
  selectedRef: ElementRef<HTMLUListElement>;

  @ViewChild('layoutRef', {read: ElementRef})
  layoutRef: ElementRef<HTMLElement>;

  @ViewChild('iconRef', {read: ElementRef, static: false})
  iconRef: ElementRef;

  onChange: (value: Key | Key[]) => void = () => this.logger.error('value accessor is not registered');
  onTouched: () => void = () => this.logger.error('value accessor is not registered');
  registerOnChange = fn => this.onChange = fn;
  registerOnTouched = fn => this.onTouched = fn;

  constructor(private hostRef: ElementRef,
              private renderer: Renderer2,
              private fb: FormBuilder,
              private popover: PopoverService,
              private logger: NGXLogger,
              private breakpoint: BreakpointService,
              public device: DeviceService) {
  }

  ngOnInit() {
    this.popover.attached.pipe(takeUntil((this.destroyed)),
      filter(t => this.opened && !!t && t !== this.hostRef))
      .subscribe(() => this.close());

    const loadOptions = (query: string) => {
      if (!!this.fetcher) {
        this.fetcher.unsubscribe();
      }

      if (!!this.loader) {
        this.fetcher = this.loader(query)
          .pipe(progress(this.progress.options$))
          .subscribe(objects => {
            this.options.found = {};
            objects.forEach((o, index) => {
              const key = o[this.keyField];
              if (!!key) {
                this.options.found[`${key}`] = {
                  index,
                  key,
                  label: o[this.labelField],
                  icon: o.icon,
                  value: o
                };
              }
            });
            this.changes.options++;
          });
      } else {
        this.options.found = {};
        let options = Object.values(this.options.persisted);
        options = options.filter(o => o.label.toLocaleLowerCase()
          .includes(query.toLocaleLowerCase()));
        options.forEach(option => this.options.found[option.key] = option);
        this.changes.options++;
      }
    };

    this.queryControl.valueChanges.pipe(
      distinctUntilChanged(),
      tap(query => {
        this.logger.debug('query has been changed');
        const input = this.queryRef.nativeElement;
        if (!!query && query.length > 0) {
          const width = Math.max((query.length + 1) * CHAR_WIDTH, MIN_WIDTH);
          this.renderer.setStyle(input, 'width', width + 'px');
        } else {
          this.renderer.removeStyle(input, 'width');
        }
      }),
      debounceTime(SEARCH_DELAY),
      filter(query => !!query)
    ).subscribe(query => loadOptions(query));
  }

  ngAfterContentInit() {
    const convert = (options: SelectOptionDirective[]) => {
      this.options.persisted = {};
      options.forEach(({key, label, icon, value}, index) =>
        this.options.persisted[`${key}`] = {index, key, label, icon, value});
      this.changes.options++;
    };

    convert(this.optionsFromMarkup.toArray());
    this.optionsFromMarkup.changes.pipe(tap(() => this.logger.debug('options from markup changed')))
      .subscribe(options => convert(options.toArray()));
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
    if (!!this.reference.popover) {
      this.reference.popover.hide();
      this.reference.popover = null;
    }
  }

  @HostListener('document:click', ['$event'])
  onClickOutside(event: Event) {
    const path = event.composedPath();
    const picked = path.indexOf(this.hostRef.nativeElement) !== -1;
    if (!!this.reference.popover && this.opened && !picked && !this.reference.popover.picked(path)) {
      this.close();
    }
  }

  @HostListener('click', ['$event'])
  onFocus({target}: { target: HTMLElement, path: HTMLElement[] }) {
    switch (this.mode) {
      case SelectMode.single:
        break;
      case SelectMode.multiple:
        if (target === this.selectedRef.nativeElement) {
          this.opened ? this.close() : this.open();
        }
        break;
    }
  }

  @HostListener('blur')
  onBlur() {
    this.onTouched();
  }

  @HostListener('click', ['$event'])
  onClick({target}: { target: HTMLElement }) {
    // TODO: think about iconRef
    const elements = [
      this.layoutRef.nativeElement,
      this.selectedRef.nativeElement,
      this.iconRef?.nativeElement
    ];
    if (elements.includes(target)) {
      this.open();
    }
  }

  trackOption(index: number, option: IOption) {
    return option.key || index;
  }

  toggle(option: IOption) {
    this.selected.indexOf(option.key) === -1
      ? this.select(option) : this.remove(option.key);
  }

  select(option: IOption) {
    this.logger.debug('option is selected');
    this.options.persisted[`${option.key}`] = option;
    this.changes.options++;
    if (this.mode === SelectMode.multiple) {
      this.selected.push(option.key);
      if (!!this.reference.popover) {
        this.reference.popover.update();
      }
    } else {
      this.selected = [option.key];
    }

    if (this.mode !== SelectMode.multiple || !this.features.includes(Feature.multiplex)) {
      this.close();
    }
    this.onChange(this.mode === SelectMode.multiple ? this.selected : option.key);
    this.updated.emit(option.value);
  }

  remove(key: Key) {
    const index = this.selected.findIndex(i => i === key);
    if (index !== -1 && (this.mode === SelectMode.multiple || this.features.includes(Feature.allowEmpty))) {
      this.logger.debug(`option ${index} has been removed`);
      this.selected.splice(index, 1);
      this.changes.selected++;
      this.onChange(this.mode === SelectMode.multiple ? this.selected : null);
    }
  }

  open() {
    this.opened = true;
    const input = this.queryRef.nativeElement;
    const focusQuery = () => {
      const style = getComputedStyle(input);
      if (style.display !== 'none') {
        setTimeout(() => input.focus());
      } else {
        setTimeout(() => focusQuery(), CHECKING_INTERVAL);
      }
    };
    focusQuery();
    if (!this.inline) {
      this.reference.popover = this.popover.show(this.hostRef, {
        contentTemplate: this.optionsTemplate,
        behaviour: Behaviour.dropdown,
        placement: this.placement,
        padding: UI.gutter.small
      });
    }
  }

  close() {
    this.opened = false;
    this.queryControl.setValue(null);
    if (!!this.reference.popover) {
      this.reference.popover.hide();
      this.reference.popover = null;
    }
  }

  writeValue(value: Key | Key[]) {
    if (this.mode === SelectMode.multiple && !Array.isArray(value)) {
      throw new Error('Wrong value form multiple select mode');
    }

    this.selected = (this.mode === SelectMode.single ? (!!value ? [value] : []) : value) as Key[];
  }

  createOption(query, event: KeyboardEvent) {
    if (event.key === Keyboard.enter) {
      event.preventDefault();
    }
    if (!!query && event.key === Keyboard.enter && !!this.creator) {
      const complete = this.creator(query, this.close.bind(this));
      if (!!complete) {
        complete.subscribe(() => this.queryControl.setValue(null));
      }
    }
  }

  setDisabledState(disabled: boolean) {
    this.disabled = disabled;
  }
}
