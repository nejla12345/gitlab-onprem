import { animate, style, transition, trigger } from '@angular/animations';
import { Component, ContentChild, forwardRef, HostBinding, HostListener, Input, OnInit, TemplateRef } from '@angular/core';
import { ControlValueAccessor, FormBuilder, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import { FlexAlign } from '../../core/enums/flex';
import { Feature } from '../../core/enums/feature';
import { Size } from '../../core/enums/size';
import { UI } from '../../core/enums/ui';
import { LOGGER_PROVIDERS } from '../../core/logger/providers';

enum AnimationState {
  default = 'default',
  checked = 'checked',
  unchecked = 'unchecked'
}

@Component({
  selector: 'sn-radio',
  templateUrl: './radio.encapsulated.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RadioComponent),
      multi: true
    },
    ...LOGGER_PROVIDERS
  ],
  animations: [
    trigger('scale', [
        transition(`* => ${AnimationState.default}`, []),
        transition(':enter', [
          style({transform: 'scale(0)'}),
          animate('.3s', style({transform: 'scale(1)'}))
        ]),
        transition(':leave', [
          style({transform: 'scale(1)'}),
          animate('.3s', style({transform: 'scale(0)'}))
        ])
      ]
    )
  ]
})
export class RadioComponent implements ControlValueAccessor, OnInit {

  ui = UI;
  animate = AnimationState.default;

  @HostBinding('attr.ra')
  readonly host = '';

  _align: FlexAlign = FlexAlign.center;

  radioControl = this.fb.control(false);
  form = this.fb.group({
    radio: this.radioControl
  });

  @HostBinding('attr.data-sn-checked')
  get checked() {
    return this.radioControl.value;
  }

  @HostBinding('attr.data-sn-size')
  _size = Size.normal;

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }

  @Input()
  set align(align: FlexAlign) {
    this._align = align || FlexAlign.center;
  }

  get align() {
    return this._align;
  }

  @Input()
  attributes: { [key: string]: string };

  @Input()
  label: string;

  @Input()
  value: any;

  @HostBinding('attr.data-sn-features')
  @Input()
  features: Feature[] = [];

  @ContentChild('radioLabelTemplate')
  labelTemplate: TemplateRef<any>;

  onChange: (value: any) => void = () => this.logger.error('value accessor is not registered');
  onTouched: () => void = () => this.logger.error('value accessor is not registered');
  registerOnChange = fn => this.onChange = fn;
  registerOnTouched = fn => this.onTouched = fn;
  @HostListener('blur') onBlur = () => this.onTouched();

  constructor(private fb: FormBuilder,
              private logger: NGXLogger) {
  }

  ngOnInit() {
    this.radioControl.valueChanges
      .subscribe(checked => {
        this.animate = checked ? AnimationState.checked : AnimationState.unchecked;
        // TODO: check this
        this.onChange(this.features.includes(Feature.allowEmpty) ? (checked || null) : true);
      });
  }

  writeValue(value: boolean) {
    this.radioControl.setValue(value, {emitEvent: false});
  }

  setDisabledState(disabled: boolean) {
    disabled ? this.radioControl.disable({emitEvent: false})
      : this.radioControl.enable({emitEvent: false});
  }
}
