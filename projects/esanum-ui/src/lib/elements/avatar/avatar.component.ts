import { Component, ContentChild, HostBinding, Input } from '@angular/core';
import { Shape } from '../../core/enums/shape';
import { Size } from '../../core/enums/size';
import { UI } from '../../core/enums/ui';
import { DotComponent } from '../dot/dot.component';

@Component({
  selector: 'sn-avatar',
  templateUrl: './avatar.encapsulated.html'
})
export class AvatarComponent {

  @HostBinding('attr.av')
  readonly host = '';

  ui = UI;

  @HostBinding('attr.data-sn-size')
  _size: Size = Size.normal;

  @HostBinding('attr.data-sn-shape')
  _shape: Shape = Shape.circle;

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }

  @Input()
  set shape(shape: Shape) {
    this._shape = shape || Shape.circle;
  }

  @Input()
  icon: string = UI.icons.user;

  @Input()
  name: string;

  @Input()
  surname: string;

  @Input()
  image: string;

  @ContentChild(DotComponent)
  dot: DotComponent;
}
