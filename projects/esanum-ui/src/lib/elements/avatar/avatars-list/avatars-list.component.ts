import { Component, ContentChildren, HostBinding, Input, QueryList } from '@angular/core';
import { Size } from '../../../core/enums/size';
import { UI } from '../../../core/enums/ui';
import { AvatarComponent } from '../avatar.component';

@Component({
  selector: 'sn-avatars-list',
  templateUrl: './avatars-list.encapsulated.html'
})
export class AvatarsListComponent {

  @HostBinding('attr.at')
  readonly host = '';

  ui = UI;

  @HostBinding('attr.data-sn-size')
  _size: Size = Size.normal;

  @Input()
  max = 5;

  @ContentChildren(AvatarComponent)
  avatars: QueryList<AvatarComponent>;

  @HostBinding('attr.data-sn-capacity')
  get capacity() {
    return Math.min(this.avatars.length, this.max);
  }

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }

  get size() {
    return this._size;
  }

}
