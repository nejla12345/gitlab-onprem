import { Component, ContentChildren, HostBinding, Input, QueryList } from '@angular/core';
import { Size } from '../../../core/enums/size';
import { UI } from '../../../core/enums/ui';
import { AvatarComponent } from '../avatar.component';

const MAX_CAPACITY = 4;

@Component({
  selector: 'sn-avatars-group',
  templateUrl: './avatars-group.encapsulated.html'
})
export class AvatarsGroupComponent {

  @HostBinding('attr.ag')
  readonly host = '';

  @HostBinding('attr.data-sn-size')
  _size: Size = Size.normal;

  _total = 0;

  ui = UI;
  max = MAX_CAPACITY;

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }

  get size() {
    return this._size;
  }

  @Input()
  set total(total: number) {
    this._total = total || 0;
  }

  get total() {
    return this._total;
  }

  @ContentChildren(AvatarComponent)
  avatars: QueryList<AvatarComponent>;

  @HostBinding('attr.data-sn-capacity')
  get capacity() {
    return Math.min(this.avatars.length, MAX_CAPACITY);
  }

  get overflow() {
    return Math.max(this.total - MAX_CAPACITY, 0);
  }
}
