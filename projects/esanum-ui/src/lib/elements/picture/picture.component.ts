import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  HostBinding,
  Input,
  TemplateRef
} from '@angular/core';
import { Fit } from '../../core/enums/fit';
import { Position } from '../../core/enums/position';
import { UI } from '../../core/enums/ui';

@Component({
  selector: 'sn-picture',
  templateUrl: './picture.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PictureComponent {

  @HostBinding('attr.pi')
  readonly host = '';

  ui = UI;

  _src: string;
  _icon = UI.icons.image;
  private _pictureCopyrightTemplate: TemplateRef<any>;

  @HostBinding('attr.data-sn-has-src')
  get hasSrc() {
    return !!this._src;
  }

  @HostBinding('attr.data-sn-fit')
  _fit: Fit = Fit.width;

  @HostBinding('attr.data-sn-position')
  _position: Position = Position.center;

  @Input()
  set icon(icon: string) {
    this._icon = icon || UI.icons.image;
  }

  get icon() {
    return this._icon;
  }

  @Input()
  set src(src: string) {
    this._src = src || null;
  }

  get src() {
    return this._src;
  }

  @HostBinding('attr.title')
  @Input()
  title: string;

  @HostBinding('attr.alt')
  @Input()
  alt: string;

  @HostBinding('style.width')
  @Input()
  width: string;

  @HostBinding('style.height')
  @Input()
  height: string;

  @Input()
  set fit(fit: Fit) {
    this._fit = fit || Fit.width;
  }

  @Input()
  set position(position: Position) {
    this._position = position || Position.center;
  }

  @Input()
  loading: boolean;

  @Input()
  attributes: { [key: string]: string };

  @ContentChild('pictureCopyrightTemplate')
  set pictureCopyrightTemplate(templateRef: TemplateRef<any>) {
    this._pictureCopyrightTemplate = templateRef;
    this.cd.detectChanges();
  }

  get pictureCopyrightTemplate() {
    return this._pictureCopyrightTemplate;
  }

  constructor(private cd: ChangeDetectorRef) {
  }
}
