import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SpinnerModule } from '../../layout/spinner/spinner.module';
import { BindingModule } from '../../core/directives/binding.module';
import { StackModule } from '../../layout/stack/stack.module';
import { IconModule } from '../icon/icon.module';
import { PictureComponent } from './picture.component';

@NgModule({
  imports: [
    CommonModule,
    IconModule,
    StackModule,
    BindingModule,
    SpinnerModule
  ],
  exports: [
    PictureComponent
  ],
  entryComponents: [
    PictureComponent
  ],
  declarations: [
    PictureComponent
  ]
})
export class PictureModule {
}
