import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  Output
} from '@angular/core';
import { Color } from '../../core/enums/color';
import { Feature } from '../../core/enums/feature';
import { Position } from '../../core/enums/position';
import { UI } from '../../core/enums/ui';

@Component({
  selector: 'sn-badge',
  templateUrl: './badge.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BadgeComponent {

  ui = UI;

  @HostBinding('attr.bd')
  readonly host = '';

  private _position: Position = Position.inline;
  private _color: string = Color.primary;
  private _overflow = null;
  private _value: number;
  private _text: string;

  @Output()
  updated = new EventEmitter<any>();

  @Input()
  set text (text: string) {
    this._text = text;
    this.updated.emit();
  }

  get text() {
    return this._text;
  }

  @Input()
  set value (value: number) {
    this._value = value;
    this.updated.emit();
  }

  get value() {
    return this._value;
  }

  @Input()
  set overflow(overflow: number) {
    this._overflow = overflow || null;
  }

  get overflow() {
    return this._overflow;
  }

  @HostBinding('attr.data-sn-has-overflow')
  get hasOverflow() {
    return !!this.overflow ? this.value > this.overflow : null;
  }

  @HostBinding('attr.data-sn-features')
  @Input()
  features: Feature[] = [Feature.overflow];

  @HostBinding('attr.data-sn-color')
  @Input()
  set color(color: string) {
    this._color = color || Color.primary;
    this.updated.emit();
  }

  get color() {
    return this._color;
  }

  @HostBinding('attr.data-sn-position')
  @Input()
  set position(position: Position) {
    this._position = position || Position.inline;
  }

  get position() {
    return this._position;
  }
}
