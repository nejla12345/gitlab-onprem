import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  HostBinding,
  Input
} from '@angular/core';
import { Subscription } from 'rxjs';
import { Color } from '../../core/enums/color';
import { Feature } from '../../core/enums/feature';
import { Outline } from '../../core/enums/outline';
import { Size } from '../../core/enums/size';
import { UI } from '../../core/enums/ui';
import { DotComponent } from '../dot/dot.component';

@Component({
  selector: 'sn-label',
  templateUrl: './label.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LabelComponent {

  @HostBinding('attr.ll')
  readonly host = '';

  ui = UI;

  private _dot: DotComponent;

  @HostBinding('style.background-color')
  @HostBinding('style.border-color')
  @HostBinding('attr.data-sn-color')
  _color: string = Color.primary;

  @HostBinding('attr.data-sn-size')
  _size: Size = Size.normal;

  @HostBinding('attr.data-sn-outline')
  _outline: Outline = Outline.fill;

  private _subscriptions: {dot: Subscription} = {dot: null}

  @Input()
  label: string;

  @Input()
  icon: string;

  @Input()
  set color(color: string) {
    this._color = color || Color.primary;
  }

  get color() {
    return this._color;
  }

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }

  @Input()
  set outline(outline: Outline) {
    this._outline = outline || Outline.fill;
  }

  get outline() {
    return this._outline;
  }

  @HostBinding('attr.tabindex')
  get tabindex() {
    return this.features.includes(Feature.clickable) ? 1 : null;
  }

  @HostBinding('attr.data-sn-features')
  @Input()
  features: Feature[] = [Feature.textBrightness];

  @ContentChild(DotComponent)
  set dot(dot: DotComponent) {
    this._dot = dot;
    this.cd.detectChanges();
    this._subscriptions.dot?.unsubscribe();
    if (!!this._dot) {
      this._subscriptions.dot = this._dot.updated
        .subscribe(() => this.cd.detectChanges());
    }
  }

  get dot() {
    return this._dot;
  }

  constructor(private cd: ChangeDetectorRef) {
  }
}
