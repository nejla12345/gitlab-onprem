import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input
} from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { IconModifier } from '../../core/enums/icon-modifier';
import { Size } from '../../core/enums/size';
import { Stroke } from '../../core/enums/stroke';
import { LOGGER_PROVIDERS } from '../../core/logger/providers';
import { IconType } from './enums';

const DEFAULT_ICONSET = 'esanum-ui-icons-default';

@Component({
  selector: 'sn-icon',
  templateUrl: './icon.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [...LOGGER_PROVIDERS]
})
export class IconComponent {

  @HostBinding('attr.ic')
  readonly host = '';

  iconType = IconType;
  iconModifier = IconModifier;

  @HostBinding('attr.data-sn-size')
  _size: Size = Size.auto;

  _stroke: Stroke = Stroke.normal;

  @HostBinding('attr.data-sn-icon')
  _icon: string;

  @HostBinding('attr.data-sn-type')
  type: IconType = IconType.svg;

  iconset: string = DEFAULT_ICONSET;

  @HostBinding('attr.tags')
  tags: string[];

  @Input()
  set icon(query: string) {
    if (!query) {
      this.logger.warn('Icon query was not passed');
      return;
    }

    const [icon, type, iconset, tags] = query.split(':');
    [this._icon, this.type, this.iconset, this.tags] =
      [icon, type as IconType || IconType.svg, iconset || DEFAULT_ICONSET, !!tags ? tags.split('|') : []];
  }

  @Input()
  set stroke(stroke: Stroke) {
    this._stroke = stroke || Stroke.normal;
  }

  get stroke() {
    return this._stroke;
  }

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }

  @Input()
  color = null;

  @HostBinding('attr.data-sn-has-color')
  get hasColor() {
    return !!this.color;
  }

  @Input()
  modifier: IconModifier;

  constructor(private logger: NGXLogger) {
  }
}
