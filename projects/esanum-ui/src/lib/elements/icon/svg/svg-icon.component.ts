import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnInit,
  Renderer2
} from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { EsanumUIConfig } from '../../../config';
import { Stroke } from '../../../core/enums/stroke';
import { LOGGER_PROVIDERS } from '../../../core/logger/providers';
import { InMemoryCacheService } from '../../../core/services/in-memory-cache.service';
import { IconTag } from '../enums';

const DEFAULT_ICONSET = 'default';

@Component({
  selector: 'sn-svg-icon',
  templateUrl: './svg-icon.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [...LOGGER_PROVIDERS]
})
export class SvgIconComponent implements OnInit {

  @HostBinding('attr.si')
  readonly host = '';

  private _initialized = false;
  private svg: HTMLElement;
  private _color: string;
  private _stroke: Stroke = Stroke.normal;
  private _iconset = DEFAULT_ICONSET;
  private _icon: string;
  private _tags: string[];

  @Input()
  set iconset(iconset: string) {
    this._iconset = iconset;
    if (this._initialized) {
      this.load();
    }
  }

  get iconset() {
    return this._iconset;
  }

  @Input()
  @HostBinding('attr.data-sn-icon')
  set icon(icon: string) {
    this._icon = icon;
    if (this._initialized) {
      this.load();
    }
  }

  get icon() {
    return this._icon;
  }

  @Input()
  @HostBinding('attr.data-sn-tags')
  set tags(tags: string[]) {
    this._tags = tags;
    if (!!this.svg) {
      this.render();
    }
  }

  get tags() {
    return this._tags;
  }

  @Input()
  @HostBinding('attr.data-sn-stroke')
  set stroke(stroke: Stroke) {
    this._stroke = stroke || Stroke.normal;
  }

  get stroke() {
    return this._stroke;
  }

  @Input()
  set color(color: string) {
    this._color = color;
    if (!!this.svg) {
      this.render();
    }
  }

  get color() {
    return this._color;
  }

  @HostBinding('attr.data-sn-has-color')
  get hasColor() {
    return !!this.color;
  }

  constructor(private logger: NGXLogger,
              private config: EsanumUIConfig,
              private http: HttpClient,
              private cache: InMemoryCacheService,
              private hostRef: ElementRef,
              private renderer: Renderer2) {
  }

  ngOnInit() {
    this._initialized = true;
    this.load();
  }

  render() {
    this.svg.setAttribute('fill', 'none');
    this.svg.setAttribute('stroke', 'none');
    if (!!this.color && this.tags.length > 0) {
      if (this.tags.includes(IconTag.stroked)) {
        this.svg.setAttribute('stroke', this.color);
      }
      if (this.tags.includes(IconTag.filled)) {
        this.svg.setAttribute('fill', this.color);
      }
    }

    const el = this.hostRef.nativeElement;
    this.renderer.setProperty(el, 'innerHTML', this.svg.outerHTML);
  }

  private load() {
    if (!this.iconset || !this.icon) {
      return;
    }

    const path = `${this.config.assets}/icons/svg/${this.iconset}.xml?hash=${this.config.hash}`;
    const key = `${path}|${this.icon}`;

    let icon = this.cache.get<HTMLElement>(key);
    if (icon === undefined) {
      let iconset$ = this.cache.get<BehaviorSubject<Document>>(path);
      if (iconset$ === undefined) {
        iconset$ = new BehaviorSubject<Document>(null);
        this.cache.set(path, iconset$);

        this.http.get(path, {responseType: 'text'}).subscribe(response =>
          iconset$.next(new DOMParser().parseFromString(response, 'application/xml')));
      }
      iconset$.pipe(filter(iconset => !!iconset))
        .subscribe(iconset => {
          icon = iconset.querySelector(`[id=${this.icon}]`);
          if (!icon) {
            this.logger.warn(`icon [${this.icon}] not found`);
            return;
          }

          const encapsulate = (el: Element) => {
            el.setAttribute('_si', '');
            for (let i = 0; i < el.children.length; i++) {
              encapsulate(el.children[i]);
            }
          };

          encapsulate(icon);
          icon.setAttribute('width', '100%');
          icon.setAttribute('height', '100%');
          this.svg = icon;
          this.render();

          this.cache.set(key, icon);
        });
    } else {
      this.svg = icon;
      this.render();
    }
  }

}
