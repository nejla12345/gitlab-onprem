import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';
import { Scheme } from '../../core/enums/scheme';
import { UI } from '../../core/enums/ui';

@Component({
  selector: 'sn-message',
  templateUrl: './message.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MessageComponent {

  ui = UI;

  @HostBinding('attr.ms')
  readonly host = '';

  @HostBinding('attr.data-sn-scheme')
  _scheme = Scheme.primary;

  @Input()
  icon: string;

  @Input()
  set scheme(scheme: Scheme) {
    this._scheme = scheme || Scheme.primary;
  }
}
