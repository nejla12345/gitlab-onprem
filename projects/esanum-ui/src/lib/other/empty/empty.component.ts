import { Component, ContentChild, HostBinding, Input, TemplateRef } from '@angular/core';
import { UI } from '../../core/enums/ui';

@Component({
  selector: 'sn-empty',
  templateUrl: './empty.encapsulated.html'
})
export class EmptyComponent {

  ui = UI;

  @HostBinding('attr.em')
  readonly host = '';

  @Input()
  message: string;

  @Input()
  description: string;

  @ContentChild('emptyImageTemplate')
  imageTemplate: TemplateRef<any>;

  @ContentChild('emptyDescriptionTemplate')
  descriptionTemplate: TemplateRef<any>;
}
