import { Component, EventEmitter, HostBinding, Input, Output, TemplateRef } from '@angular/core';
import { UI } from '../../core/enums/ui';
import { I18N_PROVIDERS } from '../../core/i18n/providers';

@Component({
  selector: 'sn-confirm',
  templateUrl: './confirm.encapsulated.html',
  providers: [...I18N_PROVIDERS]
})
export class ConfirmComponent {

  @HostBinding('attr.cm')
  readonly host = '';

  ui = UI;

  @Input()
  message: string;

  @Input()
  template: TemplateRef<any>;

  @Output()
  ok = new EventEmitter();

  @Output()
  cancel = new EventEmitter();
}
