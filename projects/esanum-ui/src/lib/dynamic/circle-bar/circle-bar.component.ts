import { Component, ContentChild, ContentChildren, HostBinding, QueryList, TemplateRef } from '@angular/core';
import { UI } from '../../core/enums/ui';
import { BarIndicatorGroupComponent } from './indicator-group/indicator-group.component';

@Component({
  selector: 'sn-circle-bar',
  templateUrl: './circle-bar.encapsulated.html'
})
export class CircleBarComponent {

  ui = UI;

  @HostBinding('attr.cb')
  readonly host = '';

  @ContentChildren(BarIndicatorGroupComponent)
  groups: QueryList<BarIndicatorGroupComponent>;

  @ContentChild('circleBarContentTemplate')
  circleBarContentTemplate: TemplateRef<any>;

}
