import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'sn-bar-indicator',
  templateUrl: './indicator.encapsulated.html'
})
export class BarIndicatorComponent {

  @HostBinding('attr.id')
  readonly host = '';

  @Input()
  value: number;

  @Input()
  title: string;

  @Input()
  color: string;

  @Input()
  width: string;
}
