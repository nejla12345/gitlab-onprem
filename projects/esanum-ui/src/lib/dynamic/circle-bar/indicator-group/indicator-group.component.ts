import { Component, ContentChildren, HostBinding, Input, QueryList } from '@angular/core';
import { BarIndicatorComponent } from '../indicator/indicator.component';

@Component({
  selector: 'sn-bar-indicator-group',
  templateUrl: './indicator-group.encapsulated.html'
})
export class BarIndicatorGroupComponent {

  @HostBinding('attr.ig')
  readonly host = '';

  @ContentChildren(BarIndicatorComponent)
  indicators: QueryList<BarIndicatorComponent>;

  @HostBinding('attr.data-sn-index')
  @Input()
  index: number;

  @HostBinding('attr.data-sn-groups')
  @Input() groups: number;

}
