import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  ContentChildren,
  HostBinding,
  Input,
  QueryList,
  TemplateRef
} from '@angular/core';
import { Color } from '../../core/enums/color';
import { UI } from '../../core/enums/ui';
import { ProgressLineDirective } from './line/progress-line.directive';

@Component({
  selector: 'sn-progress-bar',
  templateUrl: './progress-bar.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProgressBarComponent implements AfterViewInit {

  ui = UI;

  private _value = 0;

  @HostBinding('attr.pb')
  readonly host = '';

  @ContentChild('progressBarLegendTemplate')
  progressBarLegendTemplate: TemplateRef<any>;

  @Input()
  set value(value: number) {
    this._value = value || 0;
  }

  get value() {
    return this._value;
  }

  @Input()
  color: string = Color.purple;

  @ContentChildren(ProgressLineDirective)
  lines: QueryList<ProgressLineDirective>;

  constructor(private cd: ChangeDetectorRef) {
  }

  ngAfterViewInit() {
    this.lines.changes.subscribe(() => this.cd.detectChanges());
  }

}
