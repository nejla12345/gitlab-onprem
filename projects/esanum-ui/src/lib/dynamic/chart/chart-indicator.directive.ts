import { ContentChild, Directive, Input, TemplateRef } from '@angular/core';

@Directive({
  selector: 'sn-chart-indicator'
})
export class ChartIndicatorDirective {

  @Input()
  label: string;

  @Input()
  value: number;

  @Input()
  title: string;

  @Input()
  color: string;

  @Input()
  data: any;

  @ContentChild('chartIndicatorTitleTemplate')
  titleTemplate: TemplateRef<any>;
}
