import { Component, ContentChildren, EventEmitter, forwardRef, HostBinding, HostListener, Input, Output, QueryList } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NGXLogger } from 'ngx-logger';
import { State } from '../../core/enums/state';
import { UI } from '../../core/enums/ui';
import { LOGGER_PROVIDERS } from '../../core/logger/providers';
import { isEqual } from '../../core/utils/equal';
import { ChartIndicatorDirective } from './chart-indicator.directive';

@Component({
  selector: 'sn-chart',
  templateUrl: './chart.encapsulated.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ChartComponent),
      multi: true
    },
    ...LOGGER_PROVIDERS
  ]
})
export class ChartComponent implements ControlValueAccessor {

  @HostBinding('attr.ct')
  readonly host = '';

  ui = UI;

  private _selected: number;
  private _widthMark = 100;

  @Input()
  keyField: string;

  @Input()
  title: string;

  @Input()
  metric: string;

  @Input()
  state: State;

  @ContentChildren(ChartIndicatorDirective)
  indicators: QueryList<ChartIndicatorDirective>;

  @Input()
  heightIndicator = 55;

  @Input()
  widthPolygon = 50;

  onChange: (value: any) => void = () => this.logger.debug('value accessor is not registered');
  onTouched: () => void = () => this.logger.debug('value accessor is not registered');
  registerOnChange = fn => this.onChange = fn;
  registerOnTouched = fn => this.onTouched = fn;
  @HostListener('blur') onBlur = () => this.onTouched();

  @Output('selected')
  updated = new EventEmitter<any>();

  @Input()
  set widthMark(width: number) {
    this._widthMark = Math.min(width, 60);
  }

  get widthMark() {
    return this._widthMark;
  }

  set selected(value: any) {
    let isSame: boolean;
    if (!!this.keyField && !!this._selected && !!value) {
      isSame = this._selected[this.keyField] === value[this.keyField];
    } else {
      isSame = isEqual(this._selected, value);
    }

    this._selected = !isSame ? value : null;
    this.onChange(!!this._selected
      ? (!!this.keyField ? this._selected[this.keyField] : this._selected)
      : null);
    this.updated.emit(this._selected);
  }

  get selected() {
    return this._selected;
  }

  get heightSvg() {
    return this.heightIndicator + (this.heightIndicator * this.indicators.length);
  }

  constructor(private logger: NGXLogger) {
  }

  writeValue(value: any): void {
    this._selected = value;
  }

  trackByFn(index, indicator) {
    return indicator.data.id || index;
  }
}
