import { animate, state, style, transition, trigger } from '@angular/animations';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  ContentChildren,
  HostBinding,
  Input,
  QueryList,
  TemplateRef
} from '@angular/core';
import { merge, Subscription } from 'rxjs';
import { Context } from '../../core/enums/context';
import { Feature } from '../../core/enums/feature';
import { Outline } from '../../core/enums/outline';
import { Position } from '../../core/enums/position';
import { UI } from '../../core/enums/ui';
import { UrlMatching } from '../../core/enums/url';
import { BadgeComponent } from '../../elements/badge/badge.component';
import { LinkTarget } from './link.enums';

interface Icon {
  icon: string;
  position: Position;
}

type LinkSource = string | (string | { [key: string]: string | number })[];

@Component({
  selector: 'sn-link',
  templateUrl: './link.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('rotate', [
      state('opened', style({transform: 'rotate(-180deg)'})),
      state('closed', style({transform: 'rotate(0)'})),
      transition('opened <=> closed', [animate('.4s ease')])
    ])
  ]
})
export class LinkComponent {

  @HostBinding('attr.li')
  readonly host = '';

  ui = UI;
  icon: Icon;

  private _source: LinkSource;
  private _target: LinkTarget = LinkTarget.self;
  private _matching: UrlMatching = UrlMatching.fullMatch;

  private _subscriptions: {badges: Subscription} = {badges: null};

  external = false;

  @HostBinding('attr.data-sn-context')
  _context: Context = Context.text;

  @HostBinding('attr.data-sn-outline')
  _outline = Outline.transparent;

  @HostBinding('attr.data-sn-with-title')
  get withTitle() {
    return !!this.title;
  }

  @HostBinding('attr.data-sn-has-badge')
  get hasBadge() {
    return !!this.badges.length;
  }

  @Input()
  collapsed: boolean;

  @Input()
  opened: boolean;

  @Input()
  active = false;

  @HostBinding('attr.data-sn-disabled')
  @Input()
  disabled = false;

  @Input()
  set outline(outline: Outline) {
    this._outline = outline || Outline.transparent;
  }

  @Input('icon')
  set __icon__(icon: string | Icon) {
    this.icon = (typeof (icon) === 'string'
      ? {icon: icon, position: Position.left} : icon) as Icon;
  }

  @HostBinding('attr.data-sn-position')
  get position() {
    return !!this.icon ? this.icon.position : null;
  }

  @Input()
  title: string;

  @Input()
  queryParams: { [k: string]: any };

  @Input()
  set source(source: LinkSource) {
    this._source = source;
    if (!!source) {
      this.external = !Array.isArray(source);
      this.orphan = false;
    } else {
      this.external = false;
      this.orphan = true;
    }
  }

  get source() {
    return this._source;
  }

  @HostBinding('attr.data-sn-orphan')
  orphan = false;

  @Input()
  set target(target: LinkTarget) {
    this._target = target || LinkTarget.self;
  }

  get target() {
    return this._target;
  }

  @Input()
  fragment: string;

  @Input()
  set matching(matching: UrlMatching) {
    this._matching = matching || UrlMatching.fullMatch;
  }

  get matching() {
    return this._matching;
  }

  @HostBinding('attr.data-sn-features')
  @Input()
  features: Feature[] = [];

  @Input()
  set context(context: Context) {
    this._context = context || Context.text;
  }

  @Input()
  attributes: { [key: string]: string };

  @ContentChildren(BadgeComponent)
  badges: QueryList<BadgeComponent>;

  @ContentChild('linkContentTemplate')
  contentTemplate: TemplateRef<any>;

  constructor(private cd: ChangeDetectorRef) {
  }

  ngAfterViewInit() {
    this.listenBadges();
    this.badges.changes
      .subscribe(() => {
        this.cd.detectChanges();
        this.listenBadges();
      });
  }

  private listenBadges() {
    this._subscriptions.badges?.unsubscribe();
    this._subscriptions.badges = merge(...this.badges.map(badge => badge.updated))
      .subscribe(() => this.cd.detectChanges());
  }
}
