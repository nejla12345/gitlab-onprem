import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[fakeLink]'
})

export class FakeLinkDirective {

  @HostListener('click', ['$event'])
  onClick(event: MouseEvent) {
    event.preventDefault();
  }

}
