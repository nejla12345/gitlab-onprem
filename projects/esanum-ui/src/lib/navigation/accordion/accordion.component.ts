import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, ContentChildren, EventEmitter, HostBinding, Input, Output, QueryList } from '@angular/core';
import { Behaviour } from '../../core/enums/behaviour';
import { Outline } from '../../core/enums/outline';
import { UI } from '../../core/enums/ui';
import { AccordionSectionDirective } from './section/accordion-section.directive';

enum AnimationState {
  default = 'default',
  opened = 'opened',
  closed = 'closed'
}

@Component({
  selector: 'sn-accordion',
  templateUrl: './accordion.encapsulated.html',
  animations: [
    trigger('rotate', [
        state('open', style({transform: 'rotate(-180deg)'})),
        state('close', style({transform: 'rotate(0deg)'})),
        transition('open <=> close', [animate('.3s ease-in-out')])
      ]
    ),
    trigger('collapse', [
        transition(`* => ${AnimationState.default}`, []),
        transition(':enter', [style({height: 0}), animate('.3s', style({height: '*'}))]),
        transition(':leave', [style({height: '*'}), animate('.3s', style({height: 0}))])
      ]
    )
  ]
})
export class AccordionComponent {

  ui = UI;

  @HostBinding('attr.an')
  readonly host = '';

  @HostBinding('attr.data-sn-outline')
  _outline: Outline = Outline.ghost;

  _behaviour: Behaviour = Behaviour.single;

  animate = AnimationState.default;

  @ContentChildren(AccordionSectionDirective)
  sections: QueryList<AccordionSectionDirective>;

  @Input()
  active: number[] = [];

  @Input()
  set outline(outline: Outline) {
    this._outline = outline || Outline.ghost;
  }

  get outline() {
    return this._outline;
  }

  @Input()
  set behaviour(behaviour: Behaviour) {
    this._behaviour = behaviour || Behaviour.single;
  }

  get behaviour() {
    return this._behaviour;
  }

  @Output()
  changed = new EventEmitter<number[]>();

  setActive(index: number, event: Event) {
    if (this.active.includes(index)) {
      this.active.splice(this.active.indexOf(index), 1);
    } else {
      if (this.behaviour === Behaviour.single) {
        this.active.splice(0, this.active.length, index);
      } else {
        this.active.push(index);
      }
    }
    this.changed.emit(this.active);
    event.preventDefault();
  }

}
