import { ContentChild, Directive, Input, TemplateRef } from '@angular/core';
import { State } from '../../../core/enums/state';
import { UI } from '../../../core/enums/ui';

@Directive({
  selector: 'sn-accordion-section'
})
export class AccordionSectionDirective {

  ui = UI;

  @Input()
  title: string;

  @Input()
  icon: string;

  @Input()
  state: State;

  @ContentChild('accordionContentTemplate')
  accordionContentTemplate: TemplateRef<any>;

  @ContentChild('accordionTitleTemplate')
  accordionTitleTemplate: TemplateRef<any>;

}
