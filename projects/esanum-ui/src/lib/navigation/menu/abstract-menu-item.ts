import { ContentChildren, EventEmitter, HostBinding, Input, Output, QueryList } from '@angular/core';
import { UI } from '../../core/enums/ui';
import { UrlMatching } from '../../core/enums/url';
import { BadgeComponent } from '../../elements/badge/badge.component';
import { LinkTarget } from '../link/link.enums';

export abstract class AbstractMenuItem {

  ui = UI;

  _matching: UrlMatching = UrlMatching.fullMatch;

  @HostBinding('attr.opened')
  opened = false;

  @Input()
  loading = false;

  @Input()
  icon: string;

  @Input()
  disabled = false;

  @Input()
  title: string;

  @Input()
  link: string | (string | { [key: string]: string | number })[];

  @Input()
  target: string = LinkTarget.self;

  @Input()
  matching: UrlMatching = UrlMatching.fullMatch;

  @Input()
  active = false;

  @Input()
  queryParams: { [k: string]: any };

  @Input()
  fragment: string;

  @Output()
  click = new EventEmitter<any>();

  @Input()
  attributes: { [key: string]: string };

  @ContentChildren(BadgeComponent)
  badges: QueryList<BadgeComponent>;

}
