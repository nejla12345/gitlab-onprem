import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, ContentChildren, EventEmitter, HostBinding, Input, Output, QueryList } from '@angular/core';
import { FlexWrap } from '../../core/enums/flex';
import { Gutter } from '../../core/enums/gutter';
import { Orientation } from '../../core/enums/orientation';
import { Placement } from '../../core/enums/placement';
import { MenuStyle } from '../../core/enums/style';
import { Triggers } from '../../core/enums/triggers';
import { UI } from '../../core/enums/ui';
import { PopoverInstance } from '../../overlays/popover/popover.service';
import { MenuItemDirective } from './menu-item.directive';

@Component({
  selector: 'sn-menu',
  templateUrl: './menu.encapsulated.html',
  animations: [
    trigger('collapse', [
      state('void', style({height: 0, opacity: '0'})),
      state('*', style({height: '*', opacity: '1'})),
      transition('void <=> *', [animate('.3s ease')])
    ])
  ]
})
export class MenuComponent {

  @HostBinding('attr.mu')
  readonly host = '';

  ui = UI;

  private _gutter: Gutter = Gutter.none;
  private _spacing: Gutter = Gutter.none;
  private _placement: Placement = Placement.absolute;

  reference: { popover: PopoverInstance } = {popover: null};

  @HostBinding('attr.data-sn-style')
  _style: MenuStyle = MenuStyle.default;

  @HostBinding('attr.data-sn-orientation')
  _orientation: Orientation = Orientation.horizontal;

  @HostBinding('attr.data-sn-wrap')
  _wrap: FlexWrap = FlexWrap.wrap;

  _trigger: Triggers = Triggers.click;

  @HostBinding('attr.data-sn-collapsed')
  @Input()
  collapsed = false;

  @Input()
  set style(style: MenuStyle) {
    this._style = style || MenuStyle.default;
  }

  get style() {
    return this._style;
  }

  @Input()
  set orientation(orientation: Orientation) {
    this._orientation = orientation || Orientation.horizontal;
  }

  get orientation() {
    return this._orientation;
  }

  @Input()
  set wrap(wrap: FlexWrap) {
    this._wrap = wrap || FlexWrap.wrap;
  }

  get wrap() {
    return this._wrap;
  }

  @Input()
  set placement(placement: Placement) {
    this._placement = placement || Placement.absolute;
  }

  get placement() {
    return this._placement;
  }

  @Input()
  set gutter(gutter: Gutter) {
    this._gutter = gutter || Gutter.none;
  }

  get gutter() {
    return this._gutter;
  }

  @Input()
  set spacing(spacing: Gutter) {
    this._spacing = spacing || Gutter.none;
  }

  get spacing() {
    return this._spacing;
  }

  @Input()
  set trigger(trigger: Triggers) {
    this._trigger = trigger || Triggers.click;
  }

  get trigger() {
    return this._trigger;
  }

  @Input()
  context: string;

  @Output()
  selected = new EventEmitter<MenuItemDirective>();

  @ContentChildren(MenuItemDirective)
  items: QueryList<MenuItemDirective>;

  toggle(item: MenuItemDirective) {
    if (!!item.submenu) {
      this.items.forEach(i => i.opened = i === item ? !item.opened : false);
    }
  }
}
