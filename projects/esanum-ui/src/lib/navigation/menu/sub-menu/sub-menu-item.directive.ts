import { Directive } from '@angular/core';
import { AbstractMenuItem } from '../abstract-menu-item';

@Directive({
  selector: 'sn-sub-menu-item'
})
export class SubMenuItemDirective extends AbstractMenuItem {
}
