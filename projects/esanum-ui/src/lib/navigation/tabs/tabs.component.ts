import { animate, keyframes, state, style, transition, trigger } from '@angular/animations';
import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  Output,
  QueryList,
  ViewChildren
} from '@angular/core';
import { merge, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { Outline } from '../../core/enums/outline';
import { Feature } from '../../core/enums/feature';
import { UI } from '../../core/enums/ui';
import { TabDirective } from './tab.directive';

const CHECK_INTERVAL = 300;
const CHANGES_DELAY = 100;

// @ts-ignore
@Component({
  selector: 'sn-tabs',
  templateUrl: './tabs.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('flash', [
        state('void', style({opacity: 0})),
        state('*', style({opacity: 1, width: '150%', height: '150%'})),
        transition('void => *', [animate('.4s ease-in-out')]),
        transition('* => void', [animate('.3s ease-in-out', keyframes([style({opacity: '0'})]))])
      ]
    ),
    trigger('move', [
      state('*', style({transform: 'translateX({{distance}}px)'}), {params: {distance: '0'}}),
      transition('* <=> *', [animate('.4s ease-in-out')])
    ])
  ]
})
export class TabsComponent implements AfterContentInit {

  ui = UI;
  private _active = 0;
  distance = 0;
  activeTabWidth = 0;

  private _subscriptions: {tabs: Subscription} = {tabs: null};

  @HostBinding('attr.data-sn-outline')
  _outline: Outline = Outline.ghost;

  @Input()
  set active(active: number) {
    this._active = active;
    this.distance = this.links?.toArray()[this.active].nativeElement.offsetLeft;
    this.cd.detectChanges();
  }

  get active() {
    return this._active;
  }

  @Input()
  set outline(outline: Outline) {
    this._outline = outline || Outline.ghost;
  }

  get outline() {
    return this._outline;
  }

  @Output()
  changed = new EventEmitter<number>();

  @HostBinding('attr.tb')
  readonly host = '';

  @HostBinding('attr.data-sn-features')
  @Input()
  features: Feature[] = [];

  @ContentChildren(TabDirective)
  tabs: QueryList<TabDirective>;

  @ViewChildren('links')
  links: QueryList<ElementRef>;

  constructor(private cd: ChangeDetectorRef) {
  }

  ngAfterContentInit() {
    this.listenTabs();
    this.tabs.changes
      .pipe(debounceTime(CHANGES_DELAY))
      .subscribe(() => {
        this.cd.detectChanges();
        this.setWidth();
        this.listenTabs();
      });
  }

  private listenTabs() {
    this._subscriptions.tabs?.unsubscribe();
    this._subscriptions.tabs = merge(...this.tabs.map(tab => tab.updated))
      .subscribe(() => {
        this.cd.detectChanges();
        this.setWidth();
      });
  }

  setWidth() {
    if (this.active > this.links.length - 1) {
      this.active = this.links.length - 1
    }
    const tab = this.links.toArray()[this.active];
    if (!!tab) {
      let check: () => void;
      check = () => {
        const {nativeElement: e} = tab;
        if (!!e.offsetParent) {
          this.activeTabWidth = e.offsetWidth;
          this.distance = e.offsetLeft;
          this.cd.detectChanges();
        } else {
          setTimeout(() => check(), CHECK_INTERVAL);
        }
      };
      check();
    }
  }

  flash(index: number) {
    if (!!this.tabs) {
      const tab = this.tabs.toArray()[index];
      tab.state.flash = true;
      this.cd.detectChanges();
      setTimeout(() => {
        tab.state.flash = false;
        this.cd.detectChanges();
      }, 700);
    }
  }
}
