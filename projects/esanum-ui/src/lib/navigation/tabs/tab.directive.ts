import {
  AfterContentInit,
  ContentChild,
  ContentChildren,
  Directive,
  EventEmitter,
  Input,
  Output,
  QueryList,
  TemplateRef
} from '@angular/core';
import { merge, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { BadgeComponent } from '../../elements/badge/badge.component';

const CHANGES_DELAY = 100;

@Directive({
  selector: 'sn-tab'
})
export class TabDirective implements AfterContentInit {

  state = {flash: false};

  private _subscriptions: {badges: Subscription} = {badges: null};

  private _icon: string;
  private _title: string;
  private _titleTemplate: TemplateRef<any>;

  @Input()
  set icon(icon: string) {
    this._icon = icon;
    this.updated.emit();
  }

  get icon() {
    return this._icon;
  }

  @Input()
  set title(title: string) {
    this._title = title;
    this.updated.emit();
  }

  get title() {
    return this._title;
  }

  @Input()
  set titleTemplate(titleTemplate: TemplateRef<any>) {
    this._titleTemplate = titleTemplate;
    this.updated.emit();
  }

  get titleTemplate() {
    return this._titleTemplate;
  }

  @Output()
  updated = new EventEmitter<any>();

  @ContentChildren(BadgeComponent)
  badges: QueryList<BadgeComponent>;

  @ContentChild('tabContentTemplate')
  tabContentTemplate: TemplateRef<any>;

  ngAfterContentInit() {
    this.listenBadges();
    this.badges.changes
      .pipe(debounceTime(CHANGES_DELAY))
      .subscribe(() => {
        this.updated.emit();
        this.listenBadges();
      });
  }

  private listenBadges() {
    this._subscriptions.badges?.unsubscribe();
    this._subscriptions.badges = merge(...this.badges.map(badge => badge.updated))
      .subscribe(() => this.updated.emit());
  }
}
