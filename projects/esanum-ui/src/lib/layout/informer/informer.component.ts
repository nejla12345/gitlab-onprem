import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ContentChildren,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  OnDestroy,
  Output,
  QueryList,
  Renderer2,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { Context } from '../../core/enums/context';
import { Gutter } from '../../core/enums/gutter';
import { Placement } from '../../core/enums/placement';
import { UI } from '../../core/enums/ui';
import { I18N_PROVIDERS } from '../../core/i18n/providers';
import { ButtonComponent } from '../../forms/button/public_api';

@Component({
  selector: 'sn-informer-message',
  template: ``,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InformerMessageComponent {

  @Input()
  message: string;

}

@Component({
  selector: 'sn-informer',
  templateUrl: './informer.encapsulated.html',
  providers: [...I18N_PROVIDERS],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InformerComponent implements AfterViewInit, OnDestroy {

  @HostBinding('attr.if')
  readonly host = '';

  ui = UI;

  private _backdrop: ElementRef<HTMLElement>;
  private _icon = UI.icons.information;

  @HostBinding('attr.data-sn-outer')
  _outer: Gutter;

  @HostBinding('attr.data-sn-placement')
  _placement: Placement = Placement.fixed;

  @Input()
  set placement(placement: Placement) {
    this._placement = placement || Placement.fixed;
  }

  @Input()
  set outer(outer: Gutter) {
    this._outer = outer;
  }

  @HostBinding('attr.data-sn-context')
  @Input()
  context: Context;

  @Input()
  set icon(icon: string) {
    this._icon = icon || UI.icons.information;
  }

  get icon() {
    return this._icon;
  }

  @Input()
  set backdrop(backdrop: ElementRef<HTMLElement>) {
    if (!!backdrop) {
      this._backdrop = backdrop;
      this.render.setStyle(backdrop.nativeElement, 'filter', 'blur(5px)');
    }
  }

  get backdrop() {
    return this._backdrop;
  }

  @ContentChildren(InformerMessageComponent, {descendants: true})
  messages: QueryList<InformerMessageComponent>;

  @ContentChild('informerContentTemplate')
  contentTemplate: TemplateRef<any>;

  @ViewChild('okRef')
  okRef: ButtonComponent;

  @Output()
  ok = new EventEmitter();

  constructor(private render: Renderer2) {
  }

  ngAfterViewInit() {
    if (!!this.okRef) {
      this.okRef.focus();
    }
  }

  ngOnDestroy() {
    if (!!this.backdrop) {
      this.render.removeStyle(this.backdrop.nativeElement, 'filter');
    }
  }
}
