import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';
import { Size } from '../../core/enums/size';
import { UI } from '../../core/enums/ui';
import { SkeletonType } from './enums';

@Component({
  selector: 'sn-skeleton',
  templateUrl: './skeleton.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SkeletonComponent {

  ui = UI;

  private _type = SkeletonType.text;

  @HostBinding('attr.sk')
  readonly host = '';

  @HostBinding('attr.data-sn-size')
  _size = Size.normal;

  @HostBinding('attr.data-sn-type')
  @Input()
  set type(type: SkeletonType) {
    this._type = type || SkeletonType.text;
  }

  get type() {
    return this._type;
  }

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }

  @Input()
  width: string;

  @Input()
  height: string;

  @Input()
  lines = 1;

  @HostBinding('attr.data-sn-animated')
  @Input()
  animated = true;
}
