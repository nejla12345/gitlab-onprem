import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';
import { FlexAlign, FlexJustify } from '../../../core/enums/flex';
import { Gutter } from '../../../core/enums/gutter';

@Component({
  selector: 'sn-row',
  templateUrl: './row.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RowComponent {

  @HostBinding('attr.rw')
  readonly host = '';

  @HostBinding('attr.data-sn-align')
  _align: FlexAlign = FlexAlign.start;

  @HostBinding('attr.data-sn-gutter')
  _gutter: Gutter = Gutter.small;

  @HostBinding('attr.data-sn-spacing')
  @HostBinding('attr.data-sn-top-shift')
  _spacing: Gutter = Gutter.normal;

  @HostBinding('attr.data-sn-justify')
  _justify: FlexJustify = FlexJustify.start;

  @Input()
  set align(align: FlexAlign) {
    this._align = align || FlexAlign.start;
  }

  @Input()
  set justify(justify: FlexJustify) {
    this._justify = justify || FlexJustify.start;
  }

  @Input()
  set spacing(spacing: Gutter) {
    this._spacing = spacing || Gutter.normal;
  }

  @Input()
  set gutter(gutter: Gutter) {
    this._gutter = gutter || Gutter.small;
  }
}
