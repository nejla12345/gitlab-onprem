import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';

enum Overrides {
  tablet,
  desktop,
  wide
}

@Component({
  selector: 'sn-col',
  templateUrl: './col.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ColComponent {

  @HostBinding('attr.cl')
  readonly host = '';

  private _mobile = 12;
  private _tablet = 6;
  private _desktop = 1;
  private _wide = 1;

  overrides: Overrides[] = [];

  @HostBinding('attr.data-sn-mobile')
  get forMobile() {
    return this._mobile;
  }

  @HostBinding('attr.data-sn-tablet')
  get forTablet() {
    return this.overrides.includes(Overrides.tablet) ? this._tablet : this.forMobile;
  }

  @HostBinding('attr.data-sn-desktop')
  get forDesktop() {
    return this.overrides.includes(Overrides.desktop) ? this._desktop : this.forTablet;
  }

  @HostBinding('attr.data-sn-wide')
  get forWide() {
    return this.overrides.includes(Overrides.wide) ? this._wide : this.forDesktop;
  }

  @Input()
  set mobile(mobile: number) {
    this._mobile = mobile;
  }

  @Input()
  set tablet(tablet: number) {
    this._tablet = tablet;
    this.overrides.push(Overrides.tablet);
  }

  @Input()
  set desktop(desktop: number) {
    this._desktop = desktop;
    this.overrides.push(Overrides.desktop);
  }

  @Input()
  set wide(wide: number) {
    this._wide = wide;
    this.overrides.push(Overrides.wide);
  }

  @HostBinding('attr.data-sn-span')
  @Input()
  span = null;

}
