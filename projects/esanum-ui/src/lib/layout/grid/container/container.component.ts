import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';
import { Width } from '../../../core/enums/width';

@Component({
  selector: 'sn-container',
  templateUrl: './container.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContainerComponent {

  @HostBinding('attr.cn')
  readonly host = '';

  @HostBinding('attr.data-sn-width')
  _width = Width.default;

  @Input()
  set width(width: Width) {
    this._width = width || Width.default;
  }

}
