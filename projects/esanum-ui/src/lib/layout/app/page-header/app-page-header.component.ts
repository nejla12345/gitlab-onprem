import { Component, ContentChild, HostBinding, Input, TemplateRef } from '@angular/core';
import { UI } from '../../../core/enums/ui';

@Component({
  selector: 'sn-app-page-header',
  templateUrl: './app-page-header.encapsulated.html'
})
export class AppPageHeaderComponent {

  @HostBinding('attr.ph')
  readonly host = '';

  ui = UI;

  @Input()
  icon: string;

  @Input()
  title: string;

  @Input()
  teaser: string;

  @ContentChild('headerActionsTemplate')
  headerActionsTemplate: TemplateRef<any>;
}
