import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'sn-app-body',
  templateUrl: './app-body.encapsulated.html'
})
export class AppBodyComponent {

  @HostBinding('attr.ab')
  readonly host = '';
}
