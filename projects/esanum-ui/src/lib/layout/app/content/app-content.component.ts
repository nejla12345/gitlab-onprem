import { Component, ContentChild, ElementRef, HostBinding, Input } from '@angular/core';
import { Breakpoint } from '../../../core/enums/breakpoint';
import { BreadcrumbsComponent } from '../../../navigation/breadcrumbs/breadcrumbs.component';
import { BreakpointService } from '../../responsive/breakpoint.service';
import { DeviceService } from '../../responsive/device.service';
import { AppAsideComponent } from '../aside/app-aside.component';
import { AppFooterComponent } from '../footer/app-footer.component';

@Component({
  selector: 'sn-app-content',
  templateUrl: './app-content.encapsulated.html'
})
export class AppContentComponent {

  @HostBinding('attr.ac')
  readonly host = '';

  @HostBinding('attr.data-sn-with-aside')
  get withAside() {
    if (this.breakpoint.current === Breakpoint.mobile) {
      return !!this.aside;
    } else if (!!this.aside) {
      return this.aside.collapsed ? 'collapsed' : 'full';
    }
    return null;
  }

  @HostBinding('attr.data-sn-with-footer')
  get withFooter() {
    return !!this.footer;
  }

  @HostBinding('attr.data-sn-with-breadcrumbs')
  get withBreadcrumbs() {
    return !!this.breadcrumbs;
  }

  @HostBinding('attr.data-sn-windows')
  get windows() {
    return this.device.platform.windows;
  }

  @ContentChild(AppFooterComponent, {read: ElementRef, static: true})
  footer: AppFooterComponent;

  @ContentChild(BreadcrumbsComponent)
  breadcrumbs: BreadcrumbsComponent;

  @Input()
  aside: AppAsideComponent;

  constructor(private device: DeviceService,
              private breakpoint: BreakpointService) {
  }
}
