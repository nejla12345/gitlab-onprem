import { ChangeDetectorRef, Component, ContentChild, HostBinding, Input, TemplateRef } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { UI } from '../../../../core/enums/ui';
import { LOGGER_PROVIDERS } from '../../../../core/logger/providers';
import { PopoverInstance } from '../../../../overlays/popover/popover.service';

@Component({
  selector: 'sn-app-header-userbar',
  templateUrl: './app-header-userbar.encapsulated.html',
  providers: [...LOGGER_PROVIDERS]
})
export class AppHeaderUserbarComponent {

  @HostBinding('attr.hu')
  readonly host = '';

  ui = UI;

  reference: { popover: PopoverInstance } = {popover: null};

  @ContentChild('userbarAvatarTemplate')
  userbarAvatarTemplate: TemplateRef<any>;

  @ContentChild('userbarMenuTemplate')
  userbarMenuTemplate: TemplateRef<any>;

  @Input()
  context: { header: { hide: Function } };

  constructor(private logger: NGXLogger,
              public cd: ChangeDetectorRef) {
  }

  hide() {
    this.logger.debug('hide userbar dropdown');
    if (!!this.reference.popover) {
      this.reference.popover.hide();
      this.reference.popover = null;
    }
  }

}
