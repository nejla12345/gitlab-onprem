import { ChangeDetectorRef, Component, ContentChild, HostBinding, TemplateRef } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { UI } from '../../../core/enums/ui';
import { LOGGER_PROVIDERS } from '../../../core/logger/providers';
import { MenuComponent } from '../../../navigation/menu/menu.component';
import { PopoverInstance } from '../../../overlays/popover/popover.service';

@Component({
  selector: 'sn-app-header',
  templateUrl: './app-header.encapsulated.html',
  providers: [...LOGGER_PROVIDERS]
})
export class AppHeaderComponent {

  @HostBinding('attr.ah')
  readonly host = '';

  ui = UI;

  reference: { popover: PopoverInstance } = {popover: null};

  @ContentChild('headerLogoTemplate')
  headerLogoTemplate: TemplateRef<any>;

  @ContentChild('headerContentTemplate')
  contentTemplate: TemplateRef<any>;

  @ContentChild('headerTopMenu')
  menu: MenuComponent;

  @ContentChild('headerUserbarTemplate')
  headerUserbarTemplate: TemplateRef<any>;

  @ContentChild('headerActionsTemplate')
  headerActionsTemplate: TemplateRef<any>;

  constructor(private logger: NGXLogger,
              public cd: ChangeDetectorRef) {
  }

  hide() {
    this.logger.debug('hide header dropdown');
    if (!!this.reference.popover) {
      this.reference.popover.hide();
      this.reference.popover = null;
    }
  }
}
