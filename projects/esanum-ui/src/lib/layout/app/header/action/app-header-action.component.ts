import { Component, ContentChild, HostBinding, TemplateRef } from '@angular/core';
import { UI } from '../../../../core/enums/ui';

@Component({
  selector: 'sn-app-header-action',
  templateUrl: './app-header-action.encapsulated.html'
})
export class AppHeaderActionComponent {

  @HostBinding('attr.ha')
  readonly host = '';

  ui = UI;

  @ContentChild('actionLabelTemplate')
  actionLabelTemplate: TemplateRef<any>;

  @ContentChild('actionContentTemplate')
  actionContentTemplate: TemplateRef<any>;
}
