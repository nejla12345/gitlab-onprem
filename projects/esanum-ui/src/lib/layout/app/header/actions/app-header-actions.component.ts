import { Component, ContentChildren, HostBinding, Input, QueryList } from '@angular/core';
import { Gutter } from '../../../../core/enums/gutter';
import { UI } from '../../../../core/enums/ui';
import { AppHeaderActionComponent } from '../action/app-header-action.component';

@Component({
  selector: 'sn-app-header-actions',
  templateUrl: './app-header-actions.encapsulated.html'
})
export class AppHeaderActionsComponent {

  private _gutter: Gutter = Gutter.tiny;

  ui = UI;

  @HostBinding('attr.hs')
  readonly host = '';

  @Input()
  set gutter(gutter: Gutter) {
    this._gutter = gutter || Gutter.tiny;
  }

  get gutter() {
    return this._gutter;
  }

  @ContentChildren(AppHeaderActionComponent)
  actions: QueryList<AppHeaderActionComponent>;
}
