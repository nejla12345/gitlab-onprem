import { Directive, OnDestroy, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRouteSnapshot, NavigationEnd, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

type PageMeta = {
  title?: string,
  description?: string,
  image?: string
}

@Directive({
  selector: 'sn-app-page-meta'
})
export class AppPageMetaDirective implements OnInit, OnDestroy {

  private destroy$ = new Subject<any>();

  constructor(private router: Router,
              private titleService: Title,
              private metaService: Meta) {
  }

  ngOnInit() {
    this.build();
    this.router.events.pipe(
      takeUntil(this.destroy$),
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => this.build());
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private build() {
    let meta: string | PageMeta | Function;
    let snapshot: ActivatedRouteSnapshot;

    let current = this.router.routerState.snapshot.root;
    while (!!current) {
      if (!!current.data?.meta) {
        [meta, snapshot] = [current.data.meta, current];
      }
      current = current.firstChild;
    }
    this.setMetaTags(this.updateMeta(meta, snapshot));
  }

  private updateMeta(meta: string | PageMeta | Function,
                     snapshot: ActivatedRouteSnapshot): PageMeta {
    switch (typeof meta) {
      case 'undefined': {
        return {};
      }
      case 'string': {
        return {title: meta};
      }
      case 'object': {
        return meta || {};
      }
      case 'function': {
        return meta(snapshot.data, snapshot);
      }
      default:
        throw new Error(`wrong meta type: ${typeof meta}`);
    }
  }

  private setMetaTags(meta: PageMeta) {
    if (!!meta.title) {
      const title = meta.title
        .replace(/<br(\/)*>/, ' ')
        .replace(/(<([^>]+)>|&\w+;)/ig, '');

      this.titleService.setTitle(title);
      this.setMetaProperty('og:title', title);
      this.setMetaName('twitter:title', title);
    }

    if (!!meta.description) {
      this.setMetaName('description', meta.description);
      this.setMetaProperty('og:description', meta.description);
      this.setMetaName('twitter:description', meta.description);
    }

    if (!!meta.image) {
      this.setMetaName('image', meta.image);
    }
  }

  private setMetaName(name: string, content: string) {
    if (!this.metaService.getTag(`name = "${name}"`)) {
      this.metaService.addTag({name, content: content});
    } else {
      this.metaService.updateTag({name, content: content});
    }
  }

  private setMetaProperty(property: string, content: string) {
    if (!this.metaService.getTag(`property = "${property}"`)) {
      this.metaService.addTag({property, content: content});
    } else {
      this.metaService.updateTag({property, content: content});
    }
  }
}
