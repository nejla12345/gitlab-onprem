import { Component, ContentChild, HostBinding, Input, OnInit, TemplateRef } from '@angular/core';
import { filter } from 'rxjs/operators';
import { Breakpoint } from '../../../core/enums/breakpoint';
import { UI } from '../../../core/enums/ui';
import { I18N_PROVIDERS } from '../../../core/i18n/providers';
import { MenuComponent } from '../../../navigation/menu/menu.component';
import { BreakpointService } from '../../responsive/breakpoint.service';
import { DeviceService } from '../../responsive/device.service';

const ASIDE_STATE = 'aside_collapsed';

@Component({
  selector: 'sn-app-aside',
  templateUrl: './app-aside.encapsulated.html',
  providers: [...I18N_PROVIDERS]
})
export class AppAsideComponent implements OnInit {

  @HostBinding('attr.aa')
  readonly host = '';

  private _collapsed: boolean;
  ui = UI;

  @Input()
  set collapsed(collapsed: boolean) {
    this._collapsed = (this.breakpoint.current === Breakpoint.mobile || this.breakpoint.current === Breakpoint.tablet)
      ? false : (collapsed || false);
    localStorage.setItem(ASIDE_STATE, JSON.stringify(collapsed));
  }

  get collapsed() {
    return this._collapsed;
  }

  @ContentChild('asideHeaderTemplate', {static: false})
  headerTemplate: TemplateRef<any>;

  @ContentChild('asideFooterTemplate', {static: false})
  footerTemplate: TemplateRef<any>;

  @ContentChild(MenuComponent, {static: false})
  menu: MenuComponent;

  @HostBinding('attr.data-sn-opened')
  @Input()
  opened = false;

  constructor(private breakpoint: BreakpointService,
              public device: DeviceService) {
  }

  ngOnInit() {
    this.breakpoint.current$
      .pipe(filter(current => current === Breakpoint.mobile || current === Breakpoint.tablet))
      .subscribe(() => this.collapsed = false);
    this.collapsed = JSON.parse(localStorage.getItem(ASIDE_STATE));
  }

  toggle() {
    this.opened = !this.opened;
  }

}
