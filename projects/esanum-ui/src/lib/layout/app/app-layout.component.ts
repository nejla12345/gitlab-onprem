import { Component, ContentChild, HostBinding, Input } from '@angular/core';
import { Placement } from '../../core/enums/placement';
import { AppHeaderComponent } from './header/app-header.component';

@Component({
  selector: 'sn-app-layout',
  templateUrl: './app-layout.encapsulated.html'
})
export class AppLayoutComponent {

  @HostBinding('attr.al')
  readonly host = '';

  @ContentChild(AppHeaderComponent)
  header: AppHeaderComponent;

  @HostBinding('attr.data-sn-with-header')
  get withHeader() {
    return !!this.header;
  }

  @HostBinding('attr.data-sn-position')
  @Input()
  position: Placement = Placement.default;
}
