import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, ContentChild, HostBinding, Input, TemplateRef } from '@angular/core';
import { Orientation } from '../../core/enums/orientation';
import { UI } from '../../core/enums/ui';

@Component({
  selector: 'sn-collapsible',
  templateUrl: './collapsible.encapsulated.html',
  animations: [
    trigger('rotate', [
        state('open', style({transform: 'rotate(-180deg)'})),
        state('close', style({transform: 'rotate(0deg)'})),
        transition('open <=> close', [animate('.3s ease-in-out')])
      ]
    )
  ]
})
export class CollapsibleComponent {

  @HostBinding('attr.co')
  readonly host = '';

  ui = UI;

  @HostBinding('attr.data-sn-orientation')
  _orientation: Orientation = Orientation.horizontal;

  @Input()
  set orientation(type: Orientation) {
    this._orientation = type || Orientation.horizontal;
  }

  get orientation() {
    return this._orientation;
  }

  @HostBinding('attr.data-sn-opened')
  @Input()
  opened = false;

  @Input()
  icon: string;

  @Input()
  title: string;

  @ContentChild('collapsibleTitleTemplate')
  titleTemplate: TemplateRef<any>;

  @ContentChild('collapsibleContentTemplate')
  contentTemplate: TemplateRef<any>;
}
