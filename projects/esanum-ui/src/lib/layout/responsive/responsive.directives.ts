import { ChangeDetectorRef, Directive, EmbeddedViewRef, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { Breakpoint } from '../../core/enums/breakpoint';
import { BreakpointService } from './breakpoint.service';

export abstract class BreakpointDirective implements OnInit, OnDestroy {

  private destroyed = false;
  private view: EmbeddedViewRef<any>;
  protected _target = [];

  protected constructor(private breakpoint: BreakpointService,
                        private templateRef: TemplateRef<any>,
                        private viewContainerRef: ViewContainerRef,
                        private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.matched(this.breakpoint.current);
    this.breakpoint.current$
      .pipe(takeWhile(() => !this.destroyed))
      .subscribe(b => this.matched(b));
  }

  ngOnDestroy() {
    this.destroyed = true;
  }

  private matched(breakpoint: Breakpoint) {
    if (this._target.includes(breakpoint)) {
      if (!this.view) {
        this.view = this.viewContainerRef.createEmbeddedView(this.templateRef);
      }
    } else {
      if (!!this.view) {
        this.viewContainerRef.clear();
        this.view = null;
      }
    }
    this.cd.markForCheck();
  }

}

@Directive({
  selector: '[snFor]'
})
export class ForDirective extends BreakpointDirective {

  @Input('snFor')
  set target(target: Breakpoint) {
    this._target = [target];
  }

  constructor(breakpoint: BreakpointService,
              templateRef: TemplateRef<any>,
              viewContainerRef: ViewContainerRef,
              cd: ChangeDetectorRef) {
    super(breakpoint, templateRef, viewContainerRef, cd);
  }

}

const min = {
  [Breakpoint.mobile]: [Breakpoint.mobile, Breakpoint.tablet, Breakpoint.desktop, Breakpoint.wide],
  [Breakpoint.tablet]: [Breakpoint.tablet, Breakpoint.desktop, Breakpoint.wide],
  [Breakpoint.desktop]: [Breakpoint.desktop, Breakpoint.wide],
  [Breakpoint.wide]: [Breakpoint.wide]
};

@Directive({
  selector: '[snMinFor]'
})
export class ForMinDirective extends BreakpointDirective {

  @Input('snMinFor')
  set target(target: Breakpoint) {
    this._target = min[target];
  }

  constructor(breakpoint: BreakpointService,
              templateRef: TemplateRef<any>,
              viewContainerRef: ViewContainerRef,
              cd: ChangeDetectorRef) {
    super(breakpoint, templateRef, viewContainerRef, cd);
  }

}

const max = {
  [Breakpoint.mobile]: [Breakpoint.mobile],
  [Breakpoint.tablet]: [Breakpoint.mobile, Breakpoint.tablet],
  [Breakpoint.desktop]: [Breakpoint.mobile, Breakpoint.tablet, Breakpoint.desktop],
  [Breakpoint.wide]: [Breakpoint.mobile, Breakpoint.tablet, Breakpoint.desktop, Breakpoint.wide]
};

@Directive({
  selector: '[snMaxFor]'
})
export class ForMaxDirective extends BreakpointDirective {

  @Input('snMaxFor')
  set target(target: Breakpoint) {
    this._target = max[target];
  }

  constructor(breakpoint: BreakpointService,
              templateRef: TemplateRef<any>,
              viewContainerRef: ViewContainerRef,
              cd: ChangeDetectorRef) {
    super(breakpoint, templateRef, viewContainerRef, cd);
  }

}
