import { ChangeDetectorRef, Pipe, PipeTransform } from '@angular/core';
import { Subscription } from 'rxjs';
import { Breakpoint } from '../../core/enums/breakpoint';
import { BreakpointService } from './breakpoint.service';

@Pipe({name: 'breakpoint', pure: false})
export class BreakpointPipe implements PipeTransform {

  private value: boolean;
  private subscription: Subscription;

  constructor(private breakpoint: BreakpointService,
              private cd: ChangeDetectorRef) {
  }

  ngOnDestroy(): void {
    if (!!this.subscription) {
      this.subscription.unsubscribe();
      this.value = null;
      this.subscription = null;
    }
  }

  transform(breakpoint: Breakpoint): boolean {
    this.subscription = this.breakpoint.current$.subscribe(current => {
      this.value = current === breakpoint;
      this.cd.markForCheck();
    });

    return this.value;
  }
}
