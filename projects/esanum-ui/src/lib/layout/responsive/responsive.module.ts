import { NgModule } from '@angular/core';
import { BreakpointPipe } from './breakpoint.pipe';
import { BreakpointService } from './breakpoint.service';
import { ForAndroidPlatformDirective, ForDesktopDirective, ForIOSPlatformDirective, ForMobileDirective } from './device.directives';
import { DeviceService } from './device.service';
import { ForDirective, ForMaxDirective, ForMinDirective } from './responsive.directives';
import { ViewportDirective, ViewportRuleDirective } from './viewport.directive';

@NgModule({
  declarations: [
    ForDirective,
    ForMinDirective,
    ForMaxDirective,
    ForMobileDirective,
    ForIOSPlatformDirective,
    ForAndroidPlatformDirective,
    ForDesktopDirective,
    ViewportRuleDirective,
    ViewportDirective,
    BreakpointPipe
  ],
  exports: [
    ForDirective,
    ForMinDirective,
    ForMaxDirective,
    ForMobileDirective,
    ForIOSPlatformDirective,
    ForAndroidPlatformDirective,
    ForDesktopDirective,
    ViewportRuleDirective,
    ViewportDirective,
    BreakpointPipe
  ],
  providers: [
    BreakpointService,
    DeviceService
  ]
})
export class ResponsiveModule {

}
