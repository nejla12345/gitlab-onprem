import { Injectable, NgZone } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Breakpoint } from '../../core/enums/breakpoint';

@Injectable()
export class BreakpointService {

  current$ = new BehaviorSubject<Breakpoint>(null);

  private queries = {
    [Breakpoint.mobile]: window.matchMedia(`(max-width: 767px)`),
    [Breakpoint.tablet]: window.matchMedia(`(min-width: 768px) and (max-width: 992px)`),
    [Breakpoint.desktop]: window.matchMedia(`(min-width: 993px) and (max-width: 1200px)`),
    [Breakpoint.wide]: window.matchMedia(`(min-width: 1201px)`)
  };

  get current() {
    return this.current$.getValue();
  }

  set current(breakpoint: Breakpoint) {
    if (breakpoint !== this.current) {
      this.current$.next(breakpoint);
    }
  }

  constructor(zone: NgZone) {
    for (const i of Object.keys(this.queries)) {
      const breakpoint = i as Breakpoint;
      const query = this.queries[i];
      const checker = q => {
        if (q.matches) {
          zone.run(() => this.current = breakpoint);
        }
      };
      checker(query);
      query.addListener(q => checker(q));
    }
  }
}
