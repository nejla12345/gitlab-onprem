import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  EventEmitter,
  HostBinding,
  Input,
  Output,
  TemplateRef
} from '@angular/core';
import { Feature } from '../../core/enums/feature';
import { Gutter } from '../../core/enums/gutter';
import { Height } from '../../core/enums/height';
import { Orientation } from '../../core/enums/orientation';
import { Position } from '../../core/enums/position';
import { State } from '../../core/enums/state';
import { UI } from '../../core/enums/ui';
import { Width } from '../../core/enums/width';
import { PopoverComponent } from '../../overlays/popover/popover.component';

class Picture {
  url: string;
  template: TemplateRef<any>;
  position: Position = Position.left;
  width = 70;
  height = 70;

  constructor(defs: any = null) {
    Object.assign(this, defs);
  }
}

@Component({
  selector: 'sn-card',
  templateUrl: './card.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardComponent {

  ui = UI;

  @HostBinding('attr.cd')
  readonly host = '';

  picture: Picture;
  popover: PopoverComponent;
  focused = false;

  @HostBinding('attr.data-sn-orientation')
  _orientation: Orientation = Orientation.horizontal;

  @HostBinding('attr.data-sn-height')
  _height: Height = Height.default;

  private _spacing: Gutter = Gutter.normal;
  private _dragTemplate: TemplateRef<any>;
  private _headerTemplate: TemplateRef<any>;
  private _titleTemplate: TemplateRef<any>;
  private _footerTemplate: TemplateRef<any>;
  private _cardActionsTemplate: TemplateRef<any>;

  @HostBinding('attr.data-sn-has-color')
  get hasColor() {
    return !!this.color;
  }

  @HostBinding('attr.data-sn-has-icon')
  get hasAction() {
    return !!this.icon || !!this.cardActionsTemplate;
  }

  @HostBinding('attr.data-sn-padding')
  _padding = Gutter.normal;

  @Input()
  title: string;

  @Input()
  set height(height: Height) {
    this._height = height || Height.default;
  }

  @Input('picture')
  set __picture__(picture: string | Picture) {
    if (!!picture) {
      this.picture = typeof (picture) === 'string'
        ? new Picture({url: picture, template: null, position: Position.left, width: 70, height: 70})
        : new Picture(picture);
    } else {
      this.picture = null;
    }
  }

  @Input()
  set orientation(type: Orientation) {
    this._orientation = type || Orientation.horizontal;
  }

  get orientation() {
    return this._orientation;
  }

  @Input()
  set spacing(spacing: Gutter) {
    this._spacing = spacing || Gutter.normal;
  }

  get spacing() {
    return this._spacing;
  }

  @ContentChild('cardDragTemplate')
  set dragTemplate(template: TemplateRef<any>) {
    this._dragTemplate = template;
    this.cd.detectChanges();
  }

  get dragTemplate() {
    return this._dragTemplate;
  }

  @ContentChild('cardHeaderTemplate')
  set headerTemplate(template: TemplateRef<any>) {
    this._headerTemplate = template;
    this.cd.detectChanges();
  }

  get headerTemplate() {
    return this._headerTemplate;
  }

  @ContentChild('cardTitleTemplate')
  set titleTemplate(template: TemplateRef<any>) {
    this._titleTemplate = template;
    this.cd.detectChanges();
  }

  get titleTemplate() {
    return this._titleTemplate;
  }

  @ContentChild('cardFooterTemplate')
  set footerTemplate(template: TemplateRef<any>) {
    this._footerTemplate = template;
    this.cd.detectChanges();
  }

  get footerTemplate() {
    return this._footerTemplate;
  }

  @ContentChild('cardActionsTemplate')
  set cardActionsTemplate(template: TemplateRef<any>) {
    this._cardActionsTemplate = template;
    this.cd.detectChanges();
  }

  get cardActionsTemplate() {
    return this._cardActionsTemplate;
  }

  @Input()
  icon: string;

  @Input()
  state: State;

  @Input()
  set padding(padding: Gutter) {
    this._padding = padding || Gutter.normal;
  }

  @HostBinding('attr.data-sn-width')
  @Input()
  width: Width = Width.default;

  @HostBinding('attr.data-sn-features')
  @Input()
  features: Feature[] = [];

  @Input()
  color: string;

  @Output()
  selected = new EventEmitter<any>();

  constructor(
    private cd: ChangeDetectorRef
  ) {
  }

  hideActions() {
    this.popover.hide();
  }
}
