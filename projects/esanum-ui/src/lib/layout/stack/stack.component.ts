import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';
import { FlexAlign, FlexJustify, FlexWrap } from '../../core/enums/flex';
import { Gutter } from '../../core/enums/gutter';
import { Orientation } from '../../core/enums/orientation';
import { Width } from '../../core/enums/width';

@Component({
  selector: 'sn-stack',
  templateUrl: './stack.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StackComponent {

  @HostBinding('attr.st')
  readonly host = '';

  @HostBinding('attr.data-sn-orientation')
  _orientation = Orientation.vertical;

  @HostBinding('attr.data-sn-gutter')
  _gutter = Gutter.normal;

  @HostBinding('attr.data-sn-spacing')
  _spacing: Gutter;

  @HostBinding('attr.data-sn-padding')
  _padding: Gutter = Gutter.none;

  @HostBinding('attr.data-sn-align')
  _align: FlexAlign = FlexAlign.start;

  @HostBinding('attr.data-sn-justify')
  _justify: FlexJustify = FlexJustify.start;

  @HostBinding('attr.data-sn-wrap')
  _wrap: FlexWrap = FlexWrap.noWrap;

  @HostBinding('attr.data-sn-width')
  _width: Width = Width.default;

  @Input()
  set width(width: Width) {
    this._width = width || Width.default;
  }

  @Input()
  set orientation(orientation: Orientation) {
    this._orientation = orientation || Orientation.vertical;
  }

  @Input()
  set gutter(gutter: Gutter) {
    this._gutter = gutter || Gutter.normal;
  }

  @Input()
  set spacing(spacing: Gutter) {
    this._spacing = spacing;
  }

  @Input()
  set padding(padding: Gutter) {
    this._padding = padding || Gutter.none;
  }

  @Input()
  set align(align: FlexAlign) {
    this._align = align || FlexAlign.start;
  }

  @Input()
  set justify(justify: FlexJustify) {
    this._justify = justify || FlexJustify.start;
  }

  @Input()
  set wrap(wrap: FlexWrap) {
    this._wrap = wrap || FlexWrap.noWrap;
  }
}
