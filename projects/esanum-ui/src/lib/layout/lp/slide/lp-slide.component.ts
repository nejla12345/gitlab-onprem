import { Component, HostBinding, HostListener, Input } from '@angular/core';
import { Height } from '../../../core/enums/height';
import { LpHeaderComponent } from '../header/lp-header.component';

@Component({
  selector: 'sn-lp-slide',
  templateUrl: './lp-slide.encapsulated.html'
})
export class LpSlideComponent {

  @HostBinding('attr.ls')
  readonly host = '';

  @HostBinding('attr.data-sn-height')
  _height = Height.screen;

  @HostBinding('style.height.px')
  slideHeight: number = document.documentElement.clientHeight;

  @Input()
  set height(height: Height) {
    this._height = height || Height.screen;
  }

  get height() {
    return this._height;
  }

  @Input()
  header: LpHeaderComponent;

  @HostBinding('attr.data-sn-with-header')
  get withHeader() {
    return !!this.header;
  }

  @HostListener('window:resize')
  sizeChange() {
    this.slideHeight = document.documentElement.clientHeight;
  }

}
