import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'sn-lp-layout',
  templateUrl: './lp-layout.encapsulated.html'
})
export class LpLayoutComponent {

  @HostBinding('attr.lp')
  readonly host = '';

}
