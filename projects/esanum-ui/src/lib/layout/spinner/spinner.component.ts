import { Component, HostBinding, Input } from '@angular/core';
import { Size } from '../../core/enums/size';

@Component({
  selector: 'sn-spinner',
  templateUrl: './spinner.encapsulated.html'
})
export class SpinnerComponent {

  @HostBinding('attr.sp')
  readonly host = '';

  @HostBinding('attr.data-sn-size')
  _size: Size = Size.normal;

  @Input()
  set size(size: Size) {
    this._size = size || Size.normal;
  }
}
