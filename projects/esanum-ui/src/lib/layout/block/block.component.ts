import { animate, state, style, transition, trigger } from '@angular/animations';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChild,
  HostBinding,
  Input,
  TemplateRef
} from '@angular/core';
import { Feature } from '../../core/enums/feature';
import { Gutter } from '../../core/enums/gutter';
import { State } from '../../core/enums/state';
import { UI } from '../../core/enums/ui';
import { Width } from '../../core/enums/width';

@Component({
  selector: 'sn-block',
  templateUrl: './block.encapsulated.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('success', [
        state(
          'void',
          style({opacity: 0})
        ),
        state(
          '*',
          style({opacity: 1})
        ),
        transition(
          'void <=> *',
          [animate('.3s ease-in-out')]
        )
      ]
    )
  ]
})
export class BlockComponent {

  ui = UI;

  states = {success: false};

  private _padding = Gutter.normal;
  private _spacing = Gutter.normal;
  private _blockHelpTemplate: TemplateRef<any>;
  private _blockHeaderTemplate: TemplateRef<any>;
  private _blockFooterTemplate: TemplateRef<any>;

  @HostBinding('attr.bl')
  readonly host = '';

  @HostBinding('attr.data-sn-has-help')
  get hasHelp() {
    return !!this.blockHelpTemplate;
  }

  @HostBinding('attr.data-sn-has-header')
  get hasHeader() {
    return !!this.blockHeaderTemplate || !!this.title;
  }

  @Input()
  title: string;

  @HostBinding('attr.data-sn-padding')
  @Input()
  set padding(padding: Gutter) {
    this._padding = padding || Gutter.normal;
  }

  get padding() {
    return this._padding;
  }

  @Input()
  set spacing(spacing: Gutter) {
    this._spacing = spacing || Gutter.normal;
  }

  get spacing() {
    return this._spacing;
  }

  @HostBinding('attr.data-sn-width')
  @Input()
  width: Width = Width.default;

  @Input()
  state: State;

  @HostBinding('attr.data-sn-features')
  @Input()
  features: Feature[] = [];

  @ContentChild('blockHelpTemplate')
  set blockHelpTemplate(template: TemplateRef<any>) {
    this._blockHelpTemplate = template;
    this.cd.detectChanges();
  }

  get blockHelpTemplate() {
    return this._blockHelpTemplate;
  }

  @ContentChild('blockHeaderTemplate')
  set blockHeaderTemplate(template: TemplateRef<any>) {
    this._blockHeaderTemplate = template;
    this.cd.detectChanges();
  }

  get blockHeaderTemplate() {
    return this._blockHeaderTemplate;
  }

  @ContentChild('blockFooterTemplate')
  set blockFooterTemplate(template: TemplateRef<any>) {
    this._blockFooterTemplate = template;
    this.cd.detectChanges();
  }

  get blockFooterTemplate() {
    return this._blockFooterTemplate;
  }

  constructor(
    private cd: ChangeDetectorRef
  ) {
  }

  success() {
    this.states.success = true;
    this.cd.detectChanges();
    setTimeout(() => {
      this.states.success = false;
      this.cd.detectChanges();
    }, 1800);
  }
}
