import { ContentChild, Directive, Input, TemplateRef } from '@angular/core';
import { Color } from '../../../core/enums/color';
import { UI } from '../../../core/enums/ui';

@Directive({
  selector: 'sn-timeline-item'
})
export class TimelineItemDirective {

  ui = UI;

  @Input()
  title: string;

  @Input()
  color: string = Color.primary;

  @Input()
  icon: string;

  @Input()
  attributes: { [key: string]: string };

  @ContentChild('timelineItemContentTemplate')
  contentTemplate: TemplateRef<any>;
}
