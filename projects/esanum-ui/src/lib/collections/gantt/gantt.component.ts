import {
  Component,
  ContentChild,
  ContentChildren,
  ElementRef,
  forwardRef,
  HostBinding,
  HostListener,
  Input,
  QueryList,
  Renderer2,
  TemplateRef,
  ViewChild,
  ViewChildren
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { addMonths, addYears, subMonths, subYears } from 'date-fns';
import { NGXLogger } from 'ngx-logger';
import { combineLatest } from 'rxjs';
import { delay, filter, map } from 'rxjs/operators';
import { Breakpoint } from '../../core/enums/breakpoint';
import { UI } from '../../core/enums/ui';
import { Width } from '../../core/enums/width';
import { LOGGER_PROVIDERS } from '../../core/logger/providers';
import { today } from '../../forms/calendar/utils';
import { BreakpointService } from '../../layout/responsive/breakpoint.service';
import { GanttTypes } from './enums';
import { GanttLineDirective } from './gantt-line/gantt-line.directive';

@Component({
  selector: 'sn-gantt',
  templateUrl: './gantt.encapsulated.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => GanttComponent),
      multi: true
    },
    ...LOGGER_PROVIDERS
  ]
})
export class GanttComponent implements ControlValueAccessor {

  @HostBinding('attr.gt')
  readonly host = '';

  ui = UI;
  addMonths = addMonths;
  subMonths = subMonths;
  addYears = addYears;
  subYears = subYears;
  types = GanttTypes;

  private _current: Date = new Date();
  private _width: Width = Width.fluid;

  @Input()
  type: GanttTypes = GanttTypes.month;

  @HostBinding('attr.data-sn-width')
  @Input()
  set width(width: Width) {
    this._width = width || Width.fluid;
  }

  get width() {
    return this._width;
  }

  @Input()
  title: string;

  @Input()
  loading = false;

  @ContentChild('ganttToolsTemplate')
  toolsTemplate: TemplateRef<any>;

  @ContentChild('ganttTitleTemplate')
  titleTemplate: TemplateRef<any>;

  @ContentChildren(GanttLineDirective, {descendants: true})
  lines: QueryList<GanttLineDirective>;

  @ViewChildren('calendarDay')
  calendarDays: QueryList<ElementRef>;

  @ViewChild('currentLine')
  currentLine: ElementRef;

  today = today();
  error: Error;

  onChange: (date: Date) => void = () => this.logger.error('value accessor is not registered');
  onTouched: () => void = () => this.logger.error('value accessor is not registered');
  registerOnChange = fn => this.onChange = fn;
  registerOnTouched = fn => this.onTouched = fn;
  @HostListener('blur') onBlur = () => this.onTouched();

  get current() {
    return this._current;
  }

  set current(current: Date) {
    this._current = current;
    this.onChange(current);
  }

  constructor(private logger: NGXLogger,
              private breakpoint: BreakpointService,
              private renderer: Renderer2) {
  }

  ngAfterViewInit() {
    combineLatest([this.calendarDays.changes, this.breakpoint.current$]).pipe(
      delay(0),
      filter(([_, current]) => !!this.currentLine && current !== Breakpoint.mobile),
      map(([days]) => ({days, line: this.currentLine.nativeElement}))
    ).subscribe(({days, line}) => {
      const day = days.find(day => day.nativeElement.attributes['data-current'].value === 'true');
      if (!!day) {
        this.renderer.setStyle(line, 'display', 'block');
        this.renderer.setStyle(line, 'left',
          `${day.nativeElement.offsetLeft + (day.nativeElement.offsetWidth / 2) - 2}px`);
      }
    });
  }

  writeValue(date: Date): void {
    this._current = date;
  }
}
