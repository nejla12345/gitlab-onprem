import { ContentChild, ContentChildren, Directive, Input, QueryList, TemplateRef } from '@angular/core';
import { GanttLinePeriodDirective } from '../gantt-line-period/gantt-line-period.directive';

@Directive({
  selector: 'sn-gantt-line'
})
export class GanttLineDirective {

  @Input()
  title: string;

  @ContentChild('ganttLineTitleTemplate')
  titleTemplate: TemplateRef<any>;

  @ContentChildren(GanttLinePeriodDirective, {descendants: true})
  periods: QueryList<GanttLinePeriodDirective>;
}
