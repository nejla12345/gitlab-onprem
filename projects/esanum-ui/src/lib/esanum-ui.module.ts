import { InjectionToken, ModuleWithProviders, NgModule } from '@angular/core';
import { LOGGER_PROVIDERS } from './core/logger/providers';
import { CollectionsModule } from './collections/collections.module';
import { ESANUM_DEFAULT_CONFIG, EsanumUIConfig } from './config';
import { ArrayPipesModule } from './core/pipes/array-pipes.module';
import { ColorPipesModule } from './core/pipes/color-pipes.module';
import { TextPipesModule } from './core/pipes/text-pipes.module';
import deepMerge from './core/utils/merge';
import { DynamicModule } from './dynamic/dynamic.module';
import { ElementsModule } from './elements/elements.module';
import { FormsModule } from './forms/forms.module';
import { LayoutModule } from './layout/layout.module';
import { NavigationModule } from './navigation/navigation.module';
import { OtherModule } from './other/other.module';
import { OverlaysModule } from './overlays/overlays.module';
import { SharedModule } from './shared/shared.module';

export let CONFIG_TOKEN = new InjectionToken('EsanumUIModuleConfig');

export function configFactory(config: EsanumUIConfig) {
  return deepMerge(ESANUM_DEFAULT_CONFIG, config);
}

@NgModule({
  exports: [
    SharedModule,
    OtherModule,
    LayoutModule,
    NavigationModule,
    ElementsModule,
    FormsModule,
    CollectionsModule,
    OverlaysModule,
    DynamicModule,
    ArrayPipesModule,
    ColorPipesModule,
    TextPipesModule
  ]
})
export class EsanumUiModule {
  static forRoot(config: EsanumUIConfig = {}): ModuleWithProviders<EsanumUiModule> {
    return {
      ngModule: EsanumUiModule,
      providers: [
        {
          provide: CONFIG_TOKEN,
          useValue: config
        },
        {
          provide: EsanumUIConfig,
          useFactory: configFactory,
          deps: [CONFIG_TOKEN]
        },
        ...LOGGER_PROVIDERS
      ]
    };
  }
}
